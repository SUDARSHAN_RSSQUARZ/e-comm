package in.rssquarz.retail.enums;

public enum ProductImageType {
	DEFAULT("default", "Main image of the product"), EXTRA("extra",
			"Extra image for the product");
	private String type;
	private String desc;

	private ProductImageType(String type, String desc) {
		this.type = type;
		this.desc = desc;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getDesc() {
		return desc;
	}

	public void setDesc(String desc) {
		this.desc = desc;
	}
}
