package in.rssquarz.retail.utiliy;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;

public class SMSUtility {

	
	public boolean sendSms(String number, String message){
		
		boolean sent=false;
		
		String smsUrl = null;
		try {
			smsUrl = "http://www.kookoo.in/outbound/outbound_sms.php?phone_no="+number+"&api_key=KKc45d3bae6c1f909d9891225789e98d0a&message="+URLEncoder.encode(message, "UTF-8");
		} catch (UnsupportedEncodingException e1) {
			e1.printStackTrace();
		}
		
		StringBuffer buffer = null;
		HttpURLConnection conn = null;
		BufferedReader rd = null;
		
		try {
			URL url = new URL(smsUrl);
			conn = (HttpURLConnection) url.openConnection();
			conn.setRequestMethod("POST");
			conn.setDoOutput(true);
			conn.setDoInput(true);
			conn.setUseCaches(false);

			conn.connect();
			rd = new BufferedReader(
					new InputStreamReader(conn.getInputStream()));

			String line;
			buffer = new StringBuffer();
			while ((line = rd.readLine()) != null) {
				buffer.append(line);
			}
			conn.disconnect();
			rd.close();
			System.out.println("send SMS Response::" + buffer.toString());
		} catch (Exception e) {
			e.printStackTrace();
		}finally{
			try {
				if (rd != null)
					rd.close();
				if (conn != null)
					conn.disconnect();
				/*
				 * SmsLog log = new SmsLog(); log.setMsgId(buffer.toString());
				 * log.setSmsContent(message); log.setTimeSent(new
				 * Date(System.currentTimeMillis()));
				 * log.setDeliveryStatus("SENT");// sent
				 * log.setToNumber(number); new SMSLogDao().makeSMSLog(log);
				 */
				sent=true;
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return sent;
		
	}
}
