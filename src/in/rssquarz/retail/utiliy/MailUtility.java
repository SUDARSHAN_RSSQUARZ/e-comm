package in.rssquarz.retail.utiliy;

import java.io.File;
import java.util.Properties;

import javax.mail.Address;
import javax.mail.Message;
import javax.mail.Multipart;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

import org.springframework.stereotype.Component;

@Component
public class MailUtility {

	private static String user, host, password, from, fromName, replyTo;

	private static Properties props = new Properties();

	static {

		try {
			user = "honeybuzz1234@gmail.com";
			host = "smtp.gmail.com";
			password = "honeybuzz123";
			from = "honeybuzz1234@gmail.com";
			fromName = "Honeybuzz";
			replyTo = "honeybuzz1234@gmail.com";
			props.put("mail.smtp.user", user);
			props.put("mail.smtp.host", host);
			props.put("mail.smtp.port", 567);
			props.put("mail.smtp.auth", "true");
			props.put("mail.smtp.starttls.enable", "true");
			props.put("mail.smtp.debug", "true");
			props.put("mail.smtp.socketFactory.port", 567);

		} catch (Exception e) {
			e.printStackTrace();
		}
		props.put("mail.smtp.socketFactory.fallback", "false");
	}

	@SuppressWarnings("unused")
	public static void main(String[] args) {
		MailUtility mailSynchronous = new MailUtility();
		try {
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public boolean sendMail(final String[] to, final String[] cc,
			final String[] bcc, final String subject, final String text,
			final File[] files) throws Exception {
		try {
			// Session session = Session.getDefaultInstance(props, null);
			Session session = Session.getInstance(props,
					new javax.mail.Authenticator() {
						protected PasswordAuthentication getPasswordAuthentication() {
							return new PasswordAuthentication(user,
									password);
						}
					});

			session.setDebug(true);
			Address[] replyAdd = { new InternetAddress(replyTo) };
			MimeMessage msg = new MimeMessage(session);
			msg.setSubject(subject);
			msg.setFrom(new InternetAddress(from, fromName));
			msg.setReplyTo(replyAdd);
			for (int i = 0; i < to.length; i++) {
				msg.addRecipient(Message.RecipientType.TO, new InternetAddress(
						to[i]));
			}
			for (int i = 0; i < cc.length; i++) {
				msg.addRecipient(Message.RecipientType.CC, new InternetAddress(
						cc[i]));
			}
			for (int i = 0; i < bcc.length; i++) {
				msg.addRecipient(Message.RecipientType.BCC,
						new InternetAddress(bcc[i]));
			}

			if (files != null && files.length > 0) {
				Multipart mp = new MimeMultipart();

				MimeBodyPart messagePart = new MimeBodyPart();
				messagePart.setText(text);
				mp.addBodyPart(messagePart);

				for (int i = 0; i < files.length; i++) {
					MimeBodyPart pdfFile = new MimeBodyPart();
					pdfFile.attachFile(files[i]);
					pdfFile.setHeader("contentType",
							"application/octate-stream");
					pdfFile.setFileName(files[i].getName());
					mp.addBodyPart(pdfFile);
				}
				msg.setContent(mp);
			} else {
				msg.setText(text);
			}

			msg.saveChanges();
			System.out.println("inside pwd auth needed");
			// Transport transport = session.getTransport("smtp");
			// transport.connect(host, user, password);
			Transport.send(msg, msg.getAllRecipients());
			// transport.close();

			return true;
		} catch (Exception mex) {
			mex.printStackTrace();
			return false;
		}
	}

	public final boolean sendMail(final String emailId, final String body,
			final String subject, final File file) throws Exception {
		String[] to = { emailId };
		String[] cc = {};
		String[] bcc = {};
		File[] files = { file };
		return sendMail(to, cc, bcc, subject, body, files);
	}

	public final boolean sendMail(final String emailId, final String body,
			final String subject) throws Exception {
		String[] to = { emailId };
		String[] cc = {};
		String[] bcc = {};
		return sendMail(to, cc, bcc, subject, body, null);
	}

}
