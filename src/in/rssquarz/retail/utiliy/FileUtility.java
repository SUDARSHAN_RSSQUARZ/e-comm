package in.rssquarz.retail.utiliy;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

@Component
public class FileUtility {

	@Autowired
	private Environment environment;

	public List<String> saveImage(MultipartFile[] files, String contextPath) {
		List<String> nameList = new ArrayList<String>();
		try {
			for (int i = 0; i < files.length; i++) {
				String fileName = files[i].getOriginalFilename().trim();
				System.out.println(fileName);
				if (fileName != null && fileName.length() != 0
						&& files[i].getBytes().length != 0) {
					byte[] bytes = files[i].getBytes();
					int index = fileName.lastIndexOf('.');
					String path = environment.getRequiredProperty("imagePath");
					StringBuffer sb = new StringBuffer();
					sb.append(path);
					sb.append(fileName.substring(0, index));
					sb.append(System.currentTimeMillis());
					sb.append(fileName.substring(index));
					nameList.add(sb.toString());
					BufferedOutputStream buffStream = new BufferedOutputStream(
							new FileOutputStream(new File(contextPath
									+ sb.toString())));
					buffStream.write(bytes);
					buffStream.close();
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
		return nameList;
	}

}
