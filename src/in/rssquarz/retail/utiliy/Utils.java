package in.rssquarz.retail.utiliy;

import java.security.SecureRandom;
import java.util.Random;

public class Utils {

	public static final String DEFAULT_ENCRYPTION_KEY = "safd&%$234234%";
	public static final String LOGGED_IN_USER = "loggedInUser";
	public static final String FILE_PATH = "file.save.path";
	public static final long FLAT_PRICE = 4000000;
	
	public static String getParams(int num) {
		StringBuilder params = new StringBuilder();
		for (int i = 0; i < num; i++) {
			if (i == (num - 1)) {
				params.append("?");
			} else
				params.append("?,");

		}
		return params.toString();
	}
	public static String generateNum(int len) {
        String chars = "123456789";
        final int PW_LENGTH = len;
        Random rnd = new SecureRandom();
        StringBuilder pass = new StringBuilder();
        for (int i = 0; i < PW_LENGTH; i++)
            pass.append(chars.charAt(rnd.nextInt(chars.length())));
        return pass.toString();
    }
	
	public static String  genarateRandom(int len, String chars){
	        final int PW_LENGTH = len;
	        Random rnd = new SecureRandom();
	        StringBuilder pass = new StringBuilder();
	        for (int i = 0; i < PW_LENGTH; i++)
	            pass.append(chars.charAt(rnd.nextInt(chars.length())));
	        return pass.toString();
	}
	
	public static String getAlphaNumPass(int lenalfa,int numlen ){
		String password="";
		password=genarateRandom(lenalfa,"abcdefghijklmnpqrstvwxyz");
		password=password+genarateRandom(numlen, "123456789");
		return password;
	}
	public static String getPassword(){
		return getAlphaNumPass(4, 4);
	}
}
