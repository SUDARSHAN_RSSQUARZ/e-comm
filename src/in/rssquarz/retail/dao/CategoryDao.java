package in.rssquarz.retail.dao;

import in.rssquarz.retail.model.Category;
import in.rssquarz.retail.model.Page;
import in.rssquarz.retail.model.SubCategory;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;

public interface CategoryDao {
	void saveCategory(Category category);

	void saveSubCategory(SubCategory subCategory);

	LinkedHashMap<String, List<String>> getTopNav();

	HashMap<String, String> getCategoryList();

	Category findCategoryByName(String categoryName);

	Page getPage(String category);

	boolean checkUniquenessOfCategory(Category category);

	boolean checkUniquenessOfSubCategory(SubCategory sc);
}
