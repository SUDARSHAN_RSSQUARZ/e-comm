package in.rssquarz.retail.dao;

import in.rssquarz.retail.model.Notification;

import java.util.List;

public interface NotificationDao {

	public void saveNotification(Notification notification);

	public List<Notification> getAllNotifications();
}
