package in.rssquarz.retail.dao.impl;

import in.rssquarz.retail.dao.AbstractDao;
import in.rssquarz.retail.dao.CategoryDao;
import in.rssquarz.retail.model.Category;
import in.rssquarz.retail.model.Page;
import in.rssquarz.retail.model.SubCategory;

import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.ProjectionList;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Repository;

@Repository("categoryDao")
public class CategoryDaoImpl extends AbstractDao implements CategoryDao {

	@SuppressWarnings("unchecked")
	@Cacheable("topNav")
	public LinkedHashMap<String, List<String>> getTopNav() {
		LinkedHashMap<String, List<String>> topNav = new LinkedHashMap<String, List<String>>();
		Criteria criteria = getSession().createCriteria(Category.class);
		criteria.addOrder(Order.asc("order"));
		for (Category c : (List<Category>) criteria.list()) {
			List<SubCategory> subCat = (List<SubCategory>) c.getSubCategories();
			Collections.sort(subCat, new SubCategory());
			LinkedList<String> subCatName = new LinkedList<String>();
			for (SubCategory subName : subCat) {
				subCatName.add(subName.getDisplayText());
			}
			topNav.put(c.getDisplayText(), subCatName);
		}
		return topNav;
	}

	@Override
	public void saveCategory(Category category) {
		persist(category);
	}

	@Override
	public void saveSubCategory(SubCategory subCategory) {
		persist(subCategory);
	}

	@SuppressWarnings("unchecked")
	@Override
	public HashMap<String, String> getCategoryList() {
		ProjectionList projList = Projections.projectionList();
		projList.add(Projections.property("categoryName"));
		projList.add(Projections.property("displayText"));
		Criteria criteria = getSession().createCriteria(Category.class);
		criteria.setProjection(Projections.distinct(projList));
		List<Object> rows = criteria.list();
		HashMap<String, String> map = new HashMap<String, String>();
		for (Object r : rows) {
			Object[] row = (Object[]) r;
			map.put((String) row[0], (String) row[1]);
		}
		return map;
	}

	@Override
	public Category findCategoryByName(String categoryName) {
		Criteria criteria = getSession().createCriteria(Category.class);
		criteria.add(Restrictions.eq("categoryName", categoryName));
		return (Category) criteria.uniqueResult();
	}

	@Override
	public Page getPage(String category) {
		Criteria criteria = getSession().createCriteria(Page.class);
		criteria.add(Restrictions.eq("pageName", category));
		return (Page) criteria.uniqueResult();
	}

	@Override
	public boolean checkUniquenessOfSubCategory(SubCategory sc) {
		Criteria criteria = getSession().createCriteria(SubCategory.class);
		criteria.add(Restrictions.eq("name", sc.getName()));
		Object status = criteria.uniqueResult();
		if (status == null) {
			return true;
		} else
			return false;
	}

	@Override
	public boolean checkUniquenessOfCategory(Category category) {
		Criteria criteria = getSession().createCriteria(Category.class);
		criteria.add(Restrictions.eq("categoryName", category.getCategoryName()));
		Object status = criteria.uniqueResult();
		if (status == null) {
			return true;
		} else
			return false;
	}
}
