package in.rssquarz.retail.dao.impl;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import in.rssquarz.retail.dao.AbstractDao;
import in.rssquarz.retail.dao.ProductDao;
import in.rssquarz.retail.enums.ProductType;
import in.rssquarz.retail.model.Attribute;
import in.rssquarz.retail.model.Coupons;
import in.rssquarz.retail.model.GenericProductDetails;
import in.rssquarz.retail.model.LogisticPartner;
import in.rssquarz.retail.model.Pincodes;
import in.rssquarz.retail.model.Product;
import in.rssquarz.retail.model.ProductAttributePrice;
import in.rssquarz.retail.model.ProductAttributes;
import in.rssquarz.retail.model.ProductImage;
import in.rssquarz.retail.model.SubAttributes;

@Transactional
@Repository("productDao")
public class ProductDaoImpl extends AbstractDao implements ProductDao {

	public void saveProduct(Product product) {
		saveOrUpdate(product);
	}

	public void updateProduct(Product product) {
		getSession().update(product);
	}

	public void saveProductImage(ProductImage image) {
		saveOrUpdate(image);
	}

	@Override
	public void saveProductAttribute(ProductAttributes pa) {
		saveOrUpdate(pa);
	}

	@Override
	public void saveProductAttributePrice(ProductAttributePrice pap) {
		saveOrUpdate(pap);
	}

	@Override
	public void addQuantity(ProductAttributePrice pap) {
		/*
		 * System.out.println("id:" + pap.getId()); System.out.println("id:" +
		 * pap.getQuantity()); String sql =
		 * "update ProductAttributePrice set quantity = :quantity where id = :id"
		 * ; Query query = getSession().createQuery(sql);
		 * query.setParameter("quantity", pap.getQuantity());
		 * query.setParameter("id", pap.getId()); query.executeUpdate();
		 */
		saveOrUpdate(pap);

	}

	@SuppressWarnings("unchecked")
	public List<Product> findAllProducts() {
		Criteria criteria = getSession().createCriteria(Product.class);
		return (List<Product>) criteria.list();
	}

	public void deleteProductBySsn(String ssn) {
		Query query = getSession().createSQLQuery("delete from Employee where ssn = :ssn");
		query.setString("ssn", ssn);
		query.executeUpdate();
	}

	public Product findBySsn(String ssn) {
		Criteria criteria = getSession().createCriteria(Product.class);
		criteria.add(Restrictions.eq("ssn", ssn));
		return (Product) criteria.uniqueResult();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<ProductAttributePrice> getAllOutOfStockProducts() {
		Criteria criteria = getSession().createCriteria(ProductAttributePrice.class);
		criteria.add(Restrictions.eq("quantity", 0L));
		return criteria.list();
	}

	@SuppressWarnings("unchecked")
	public List<Product> findProductsByCategory(String category) {
		Criteria criteria = getSession().createCriteria(Product.class);
		criteria.add(Restrictions.eq("category", category));
		return (List<Product>) criteria.list();
	}

	@SuppressWarnings("unchecked")
	public List<Product> findProductsBySubCategory(String category, String subCategory) {
		Criteria criteria = getSession().createCriteria(Product.class);
		criteria.add(Restrictions.eq("category", category));
		criteria.add(Restrictions.eq("subCategory", subCategory));
		return (List<Product>) criteria.list();
	}

	@SuppressWarnings("unchecked")
	public List<Product> findProductsByType() {
		Criteria criteria = getSession().createCriteria(Product.class);
		Criterion c1 = Restrictions.eq("type", ProductType.FEATURED.getType());
		// Criterion c2 = Restrictions.eq("type", ProductType.NEW.getType());
		// criteria.add(Restrictions.or(c1, c2));
		criteria.add(c1);
		return (List<Product>) criteria.list();
	}

	public Product findProductsById(Long id) {
		return getObjectById(Product.class, id);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Product> getAllProducts() {
		Criteria criteria = getSession().createCriteria(Product.class);
		return criteria.list();
	}

	@Override
	public ProductAttributes getProductPriceFromAttribute(Long attributeId) {
		return getObjectById(ProductAttributes.class, attributeId);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Product> findFeaturedProduct() {
		Criteria criteria = getSession().createCriteria(Product.class);
		Criterion c1 = Restrictions.eq("type", ProductType.FEATURED.getType());
		criteria.add(Restrictions.or(c1));
		criteria.addOrder(Order.asc("orderDisplay"));
		return (List<Product>) criteria.list();
	}

	@Override
	public Product findProductsByAttrId(Long attributeId) {
		ProductAttributes pa = getObjectById(ProductAttributes.class, attributeId);
		return pa.getProduct();
	}

	@Override
	public void savePincodes(Pincodes pincode) {
		persist(pincode);

	}

	@Override
	public void saveLogPartner(LogisticPartner logisticPartner) {
		persist(logisticPartner);
	}

	@Override
	public boolean checkPincode(String pincodenum) {
		Criteria criteria = getSession().createCriteria(Pincodes.class);
		Criterion c1 = Restrictions.eq("pincode", pincodenum);
		criteria.add(c1);
		Pincodes pincode = (Pincodes) criteria.uniqueResult();
		if (pincode == null) {
			return false;
		} else {
			return true;
		}
	}

	@Override
	public void saveCoupon(Coupons coupon) {
		persist(coupon);
	}

	@Override
	public Coupons checkCoupon(String couponcode) {
		Criteria criteria = getSession().createCriteria(Coupons.class);
		Criterion c1 = Restrictions.eq("code", couponcode);
		criteria.add(c1);
		return (Coupons) criteria.uniqueResult();
	}

	@Override
	public ProductAttributePrice findAttributeByPriceId(Long id) {
		Criteria criteria = getSession().createCriteria(ProductAttributePrice.class);
		Criterion c1 = Restrictions.eq("id", id);
		criteria.add(c1);
		return (ProductAttributePrice) criteria.uniqueResult();
	}

	@SuppressWarnings({ "unchecked" })
	@Override
	public ArrayList<ProductAttributePrice> getAttributes(Product product) {

		Criteria criteria = getSession().createCriteria(ProductAttributePrice.class);
		Criterion c1 = Restrictions.eq("product", product);
		criteria.add(c1);
		return (ArrayList<ProductAttributePrice>) criteria.list();
	}

	@Override
	public void deleteProduct(Long id, Product p) {
		// getSession().delete("id", product);
		Long pid = p.getId();

		Query query = getSession().createQuery("delete from ProductImage where product = :p");
		query.setParameter("p", p);
		query.executeUpdate();

		Query query4 = getSession().createQuery("delete from ProductAttributePrice where product = :p");
		query4.setParameter("p", p);
		query4.executeUpdate();

		Query query2 = getSession().createQuery("delete from ProductAttributes where product = :p");
		query2.setParameter("p", p);
		query2.executeUpdate();

		Query query3 = getSession().createQuery("delete from Product where id = :id");
		query3.setParameter("id", pid);
		query3.executeUpdate();
	}

	@Override
	public void deleteAttribute(Long id, Product product) {

		Query query4 = getSession().createQuery("delete from ProductAttributePrice where id = :p");
		query4.setParameter("p", id);
		query4.executeUpdate();

	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Product> findProductsByName(String name) {
		Criteria criteria = getSession().createCriteria(Product.class);
		criteria.add(Restrictions.eq("name", name));
		return (List<Product>) criteria.list();
	}

	@Override
	public void addAttributeDetails(Attribute attributes) {
		persist(attributes);

	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Attribute> getAllAttributes() {
		Criteria criteria = getSession().createCriteria(Attribute.class);
		return criteria.list();
	}

	

	@Override
	public void addSubAttributeDetails(SubAttributes subAttributes) {
		persist(subAttributes);		
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<SubAttributes> getAllSubAttributes() {
		Criteria criteria = getSession().createCriteria(SubAttributes.class);
		return criteria.list();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<SubAttributes> getSubAttributes(Long id) {
		Criteria criteria = getSession().createCriteria(SubAttributes.class);
		criteria.add(Restrictions.eq("id.id", id));
		return criteria.list();
	}

	@Override
	public Long saveGenericProduct(GenericProductDetails gp) {
		persist(gp);
		return gp.getProductId();
	}

	@Override
	public Attribute getAttributesById(Long id) {
		Criteria criteria = getSession().createCriteria(Attribute.class);
		Criterion c1 = Restrictions.eq("id", id);
		criteria.add(c1);
		return (Attribute) criteria.uniqueResult();
	}

}