package in.rssquarz.retail.dao.impl;

import in.rssquarz.retail.dao.AbstractDao;
import in.rssquarz.retail.dao.WebsiteDao;
import in.rssquarz.retail.model.Banner;

import java.util.List;

import org.hibernate.Criteria;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Transactional
@Repository("websiteDao")
public class WebsiteDaoImpl extends AbstractDao implements WebsiteDao {

	@Override
	public void addBanner(Banner banner) {
		getSession().persist(banner);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Banner> getBanners() {
		Criteria criteria = getSession().createCriteria(Banner.class);
		return (List<Banner>) criteria.list();
	}

}
