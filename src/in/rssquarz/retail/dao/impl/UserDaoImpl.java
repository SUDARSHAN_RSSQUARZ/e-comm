package in.rssquarz.retail.dao.impl;

import in.rssquarz.retail.dao.AbstractDao;
import in.rssquarz.retail.dao.UserDao;
import in.rssquarz.retail.model.UserDetails;

import org.hibernate.Criteria;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.LogicalExpression;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Transactional
@Repository("userDao")
public class UserDaoImpl extends AbstractDao implements UserDao {

	@Override
	public UserDetails userLogin(UserDetails user) {
		Criteria criteria = getSession().createCriteria(UserDetails.class);
		Criterion name = Restrictions.eq("userName", user.getUserName());
		Criterion pwd = Restrictions.eq("pwd", user.getPwd());
		LogicalExpression andExp = Restrictions.and(name, pwd);
		criteria.add(andExp);
		return (UserDetails) criteria.uniqueResult();
	}

	@Override
	public void changePwd(UserDetails user) {
		getSession().update(user);
	}

	@Override
	public UserDetails checkUser(UserDetails user) {
		Criteria criteria = getSession().createCriteria(UserDetails.class);
		Criterion name = Restrictions.eq("userName", user.getUserName());
		Criterion email = Restrictions.eq("email", user.getEmail());
		LogicalExpression andExp = Restrictions.and(name, email);
		criteria.add(andExp);
		return (UserDetails) criteria.uniqueResult();
	}

}
