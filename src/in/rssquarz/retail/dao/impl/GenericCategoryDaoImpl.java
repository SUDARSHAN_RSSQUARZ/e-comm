package in.rssquarz.retail.dao.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;
import com.mongodb.MongoClient;

import in.rssquarz.retail.dao.GenericCategoryDao;
import in.rssquarz.retail.model.GenericCategory;

@Transactional
@Repository("genericCategoryDao")
public class GenericCategoryDaoImpl implements GenericCategoryDao{

	@Override
	public void saveCategory(GenericCategory category) {
		
		try{
			MongoClient mongo = new MongoClient( "localhost" , 27017 );
			DB db = mongo.getDB("ecomm");
			DBCollection table = db.getCollection("CATEGORY");
			BasicDBObject document = new BasicDBObject();
			document.put("name", category.getName());
			document.put("parent", category.getParent());
			document.put("categoryPath", category.getCategoryPath());
			table.insert(document);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	@Override
	public Map<String, List<String>> retrieveCategoryList() {

		List<String> categoryList = new ArrayList<String>();
		Map<String, List<String>> categoryMap = new HashMap<String, List<String>>();
		try{
			MongoClient mongo = new MongoClient( "localhost" , 27017 );
			DB db = mongo.getDB("ecomm");
			DBCollection table = db.getCollection("CATEGORY");

			BasicDBObject searchQuery = new BasicDBObject();
			searchQuery.put("parent", "/");

			DBCursor cursor = table.find(searchQuery);
			while (cursor.hasNext()) {
				
				DBObject document = cursor.next();
				
				BasicDBObject searchQuery2 = new BasicDBObject();
				searchQuery2.put("categoryPath", new BasicDBObject("$regex", "/"+document.get("name").toString()+".*").append("$options", "i"));
				DBCursor cursor2 = table.find(searchQuery2);
				categoryList = new ArrayList<String>();
				while (cursor2.hasNext()) {
					DBObject document2 = cursor2.next();
					categoryList.add(document2.get("categoryPath").toString());
				}
				categoryMap.put(document.get("name").toString(), categoryList);
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		return categoryMap;
	}

}
