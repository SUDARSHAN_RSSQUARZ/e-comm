package in.rssquarz.retail.dao.impl;

import in.rssquarz.retail.dao.AbstractDao;
import in.rssquarz.retail.dao.CartDao;
import in.rssquarz.retail.model.Cart;
import in.rssquarz.retail.model.MobileUser;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.LogicalExpression;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Transactional
@Repository("cartDao")
public class CartDaoImpl extends AbstractDao implements CartDao {

	@Override
	public MobileUser findUserByToken(String token) {
		Criteria criteria = getSession().createCriteria(MobileUser.class);
		Criterion c1 = Restrictions.eq("sessionToken", token);
		criteria.add(c1);
		return (MobileUser) criteria.uniqueResult();
	}

	@Override
	public void saveItemInCart(Cart itemInCart) {
		persist(itemInCart);
	}

	@Override
	public void removeFromCart(Cart itemInCart) {
		delete(itemInCart);
	}

	@Override
	public void savemUser(MobileUser mUser) {
		persist(mUser);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<MobileUser> mUserList() {
		Criteria criteria = getSession().createCriteria(MobileUser.class);
		return (List<MobileUser>) criteria.list();
	}

	@Override
	public MobileUser findUserByDeviceId(String deviceId) {
		Criteria criteria = getSession().createCriteria(MobileUser.class);
		Criterion c1 = Restrictions.eq("deviceId", deviceId);
		criteria.add(c1);
		return (MobileUser) criteria.uniqueResult();
	}

	@Override
	public MobileUser checkEmail(String email) {
		Criteria criteria = getSession().createCriteria(MobileUser.class);
		Criterion c1 = Restrictions.eq("email", email);
		criteria.add(c1);
		return (MobileUser) criteria.uniqueResult();
	}

	@Override
	public MobileUser wUserLogin(MobileUser mUser) {
		Criteria criteria = getSession().createCriteria(MobileUser.class);
		Criterion name = Restrictions.eq("email", mUser.getEmail());
		Criterion pwd = Restrictions.eq("password", mUser.getPassword());
		LogicalExpression andExp = Restrictions.and(name, pwd);
		criteria.add(andExp);
		return (MobileUser) criteria.uniqueResult();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<MobileUser> getAllmUsers() {
		Criteria criteria = getSession().createCriteria(MobileUser.class);
		return (List<MobileUser>) criteria.list();
	}

	@Override
	public MobileUser findUserById(Long id) {
		Criteria criteria = getSession().createCriteria(MobileUser.class);
		Criterion c1 = Restrictions.eq("id", id);
		criteria.add(c1);
		return (MobileUser) criteria.uniqueResult();
	}

}
