package in.rssquarz.retail.dao.impl;

import in.rssquarz.retail.dao.AbstractDao;
import in.rssquarz.retail.dao.OrderDao;
import in.rssquarz.retail.model.Category;
import in.rssquarz.retail.model.MobileUser;
import in.rssquarz.retail.model.OrderItems;
import in.rssquarz.retail.model.Orders;
import in.rssquarz.retail.model.Pincodes;
import in.rssquarz.retail.model.Product;
import in.rssquarz.retail.model.SubCategory;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Transactional
@Repository("orderDao")
public class OrderDaoImpl extends AbstractDao implements OrderDao {

	@Override
	public String saveOrder(Orders order) {
		persist(order);
		for (OrderItems item : order.getItems())
			persist(item);
		return "" + order.getId();
	}

	
	
	
	
	@SuppressWarnings("unchecked")
	@Override
	public List<Orders> orders() {
		Criteria criteria = getSession().createCriteria(Orders.class);
		criteria.addOrder(Order.asc("orderDate"));
		return (List<Orders>) criteria.list();
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<Orders> getOrderByArea(String pincode) {
		Criteria criteria = getSession().createCriteria(Orders.class);
		criteria.add(Restrictions.eq("pin", pincode));
		return (List<Orders>) criteria.list();
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<Orders> getOrderByDate(Date orderDate) {
		Criteria criteria = getSession().createCriteria(Orders.class);
		criteria.add(Restrictions.eq("orderDate", orderDate));
		return (List<Orders>) criteria.list();
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<Orders> getUnCheckedOrders() {
		boolean status = false;
		Criteria criteria = getSession().createCriteria(Orders.class);
		criteria.add(Restrictions.eq("status", status));
		return (List<Orders>) criteria.list();
	}

	@Override
	public void checkOrder(Long id) {
		System.out.println("id : " + id);
		Query query = getSession().createSQLQuery(
				"update orders set STATUS = :status where ORDER_ID = :id");
		query.setBoolean("status", true);
		query.setLong("id", id);
		query.executeUpdate();
	}

	@Override
	public Orders getOrderById(Long id) {
		Criteria criteria = getSession().createCriteria(Orders.class);
		criteria.add(Restrictions.eq("id", id));
		return (Orders) criteria.uniqueResult();
	}

	@Override
	public List<Product> getOrderItems(Collection<OrderItems> items) {
		List<Product> productList = new ArrayList<Product>();
		Product product = null;
		Criteria criteria = getSession().createCriteria(Product.class);
		for (OrderItems item : items) {
			product = new Product();
			criteria.add(Restrictions.eq("id", item.getProductId()));
			product = (Product) criteria.uniqueResult();
			productList.add(product);
		}
		return productList;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Orders> getUserOrders(Long userId) {
		MobileUser mUser = new MobileUser();
		mUser.setId(userId);
		Criteria criteria = getSession().createCriteria(Orders.class);
		criteria.add(Restrictions.eq("mUser", mUser));
		return (List<Orders>) criteria.list();
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Override
	public List getOrdersForChart() {
		List list = null;
		List list1 = null;
		List<List> finalList = new ArrayList<List>();

		for (int j = 0; j < 7; j++) {
			double count = 0.00;
			Calendar cal = Calendar.getInstance();
			cal.add(Calendar.DATE, -j);
			Date date = cal.getTime();
			Criteria criteria1 = getSession().createCriteria(Orders.class)
					.add(Restrictions.eq("orderDate", date));
			criteria1.setProjection(Projections.property("mUser"));
			list = new ArrayList<Double>();
			list = (List<Double>) criteria1.list();
			for (int i = 0; i < list.size(); i++) {
				count = count + 1;
			}
			list1 = new ArrayList();
			list1.add(date.toString());
			list1.add(count);
			finalList.add(list1);
		}
		return finalList;
	}

	@SuppressWarnings("unchecked")
	@Override
	public  HashMap<String,String> getPinsData() {
		HashMap<String,String> hm=new HashMap<String,String>();
		Criteria criteria = getSession().createCriteria(Pincodes.class);
		
		for (Pincodes c : (List<Pincodes>) criteria.list()) {
			hm.put(c.getPincode(), c.getCity());
		}
		return hm;
	}
}
