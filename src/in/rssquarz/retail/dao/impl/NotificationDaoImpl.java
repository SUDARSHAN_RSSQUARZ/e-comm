package in.rssquarz.retail.dao.impl;

import in.rssquarz.retail.dao.AbstractDao;
import in.rssquarz.retail.dao.NotificationDao;
import in.rssquarz.retail.model.Notification;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.Order;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Transactional
@Repository("notificationDao")
public class NotificationDaoImpl extends AbstractDao implements NotificationDao {

	@Override
	public void saveNotification(Notification notification) {
		persist(notification);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Notification> getAllNotifications() {
		Criteria criteria = getSession().createCriteria(Notification.class);
		criteria.addOrder(Order.desc("id"));
		return (List<Notification>) criteria.list();
	}

}
