package in.rssquarz.retail.dao;

import in.rssquarz.retail.model.OrderItems;
import in.rssquarz.retail.model.Orders;
import in.rssquarz.retail.model.Product;

import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

public interface OrderDao {
	public String saveOrder(Orders order);

	public List<Orders> orders();

	public void checkOrder(Long id);

	public Orders getOrderById(Long id);

	public List<Product> getOrderItems(Collection<OrderItems> items);

	public List<Orders> getUserOrders(Long userId);

	public List getOrdersForChart();

	public HashMap<String, String>  getPinsData();

	public List<Orders> getOrderByArea(String pincode);

	public List<Orders> getUnCheckedOrders();

	public List<Orders> getOrderByDate(Date orderDate);
}
