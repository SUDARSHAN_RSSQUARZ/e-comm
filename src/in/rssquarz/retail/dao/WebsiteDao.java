package in.rssquarz.retail.dao;

import in.rssquarz.retail.model.Banner;

import java.util.List;

public interface WebsiteDao {

	void addBanner(Banner banner);

	List<Banner> getBanners();

}
