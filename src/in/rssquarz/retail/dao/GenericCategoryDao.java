package in.rssquarz.retail.dao;

import java.util.List;
import java.util.Map;

import in.rssquarz.retail.model.GenericCategory;


public interface GenericCategoryDao {

	public void saveCategory(GenericCategory category);

	public Map<String, List<String>> retrieveCategoryList();

}
