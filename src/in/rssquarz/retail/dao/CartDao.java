package in.rssquarz.retail.dao;

import in.rssquarz.retail.model.Cart;
import in.rssquarz.retail.model.MobileUser;

import java.util.List;

public interface CartDao {

	public MobileUser findUserByToken(String token);

	public void saveItemInCart(Cart itemInCart);

	public void removeFromCart(Cart itemInCart);

	public void savemUser(MobileUser mUser);

	public List<MobileUser> mUserList();

	public MobileUser findUserByDeviceId(String userName);

	public MobileUser checkEmail(String email);

	public MobileUser wUserLogin(MobileUser mUser);

	public List<MobileUser> getAllmUsers();

	public MobileUser findUserById(Long id);
}
