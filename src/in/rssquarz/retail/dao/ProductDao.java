package in.rssquarz.retail.dao;

import in.rssquarz.retail.model.Attribute;
import in.rssquarz.retail.model.Coupons;
import in.rssquarz.retail.model.GenericProductDetails;
import in.rssquarz.retail.model.LogisticPartner;
import in.rssquarz.retail.model.Pincodes;
import in.rssquarz.retail.model.Product;
import in.rssquarz.retail.model.ProductAttributePrice;
import in.rssquarz.retail.model.ProductAttributes;
import in.rssquarz.retail.model.ProductImage;
import in.rssquarz.retail.model.SubAttributes;

import java.util.ArrayList;
import java.util.List;

public interface ProductDao {

	void saveProduct(Product product);

	List<Product> findProductsByCategory(String category);

	List<Product> findProductsBySubCategory(String category, String subCategory);

	void deleteProductBySsn(String ssn);

	Product findBySsn(String ssn);

	void updateProduct(Product product);

	List<Product> findProductsByType();

	void saveProductImage(ProductImage image);

	Product findProductsById(Long id);

	void saveProductAttribute(ProductAttributes pa);

	List<Product> getAllProducts();

	ProductAttributes getProductPriceFromAttribute(Long attributeId);

	List<Product> findFeaturedProduct();

	Product findProductsByAttrId(Long productId);

	void deleteProduct(Long id, Product product);

	void savePincodes(Pincodes pincode);

	void saveLogPartner(LogisticPartner logisticPartner);

	void saveProductAttributePrice(ProductAttributePrice pap);

	boolean checkPincode(String pincodenum);

	void saveCoupon(Coupons coupon);

	Coupons checkCoupon(String couponcode);

	List<ProductAttributePrice> getAllOutOfStockProducts();

	ProductAttributePrice findAttributeByPriceId(Long id);

	void addQuantity(ProductAttributePrice pap);

	ArrayList<ProductAttributePrice> getAttributes(Product product);

	void deleteAttribute(Long pid, Product product);

	List<Product> findProductsByName(String category);

	void addAttributeDetails(Attribute attributes);

	List<Attribute> getAllAttributes();


	void addSubAttributeDetails(SubAttributes subAttributes);

	List<SubAttributes> getAllSubAttributes();

	List<SubAttributes> getSubAttributes(Long id);

	Long saveGenericProduct(GenericProductDetails gp);

	Attribute getAttributesById(Long id);


	/*List<Product> findProductsByCategory(Category category);*/
}