package in.rssquarz.retail.dao;

import in.rssquarz.retail.model.UserDetails;

public interface UserDao {

	public UserDetails userLogin(UserDetails user);

	public void changePwd(UserDetails user);

	public UserDetails checkUser(UserDetails user);
}
