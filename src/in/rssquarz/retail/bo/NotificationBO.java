package in.rssquarz.retail.bo;

import in.rssquarz.retail.model.MobileUser;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

import javapns.Push;
import javapns.communication.exceptions.CommunicationException;
import javapns.communication.exceptions.KeystoreException;
import javapns.notification.PushedNotification;
import javapns.notification.ResponsePacket;

import com.google.android.gcm.server.Message;
import com.google.android.gcm.server.Result;
import com.google.android.gcm.server.Sender;

public class NotificationBO {

	public boolean sendNotification(List<MobileUser> mUserList,
			String notificationText, String key, String apnsFileName,
			String apnsFilePwd) {

		boolean result = false;

		try {
			for (MobileUser mUser : mUserList) {
				if (mUser.getOsType().equalsIgnoreCase("android")) {
					// Notification using GCM service
					Sender sender = new Sender(key);
					Message message = new Message.Builder().addData("message",
							notificationText).build();
					Result r = sender.send(message, mUser.getDeviceId(), 10);
					System.out.println(r);

				} else {
					// Notification using APNS service
					InputStream in = this.getClass().getClassLoader()
							.getResourceAsStream(apnsFileName);
					List<PushedNotification> notifications = Push.alert(
							notificationText, in, apnsFilePwd, false,
							mUser.getDeviceId());
					for (PushedNotification notification : notifications) {
						if (notification.isSuccessful()) {
							System.out
									.println("Push notification sent successfully to: "
											+ notification.getDevice()
													.getToken());
						} else {
							String invalidToken = notification.getDevice()
									.getToken();
							System.err.println("Invalid Token " + invalidToken);

							System.out.println(" The problem was");
							Exception theProblem = notification.getException();
							theProblem.printStackTrace();

							ResponsePacket theErrorResponse = notification
									.getResponse();
							if (theErrorResponse != null) {
								System.out.println(theErrorResponse
										.getMessage());
							}
						}
					}
				}
			}
			result = true;
		} catch (IOException | CommunicationException | KeystoreException e) {
			result = false;
			e.printStackTrace();
		}
		return result;
	}

}
