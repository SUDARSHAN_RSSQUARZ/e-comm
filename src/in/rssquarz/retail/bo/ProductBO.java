package in.rssquarz.retail.bo;

import in.rssquarz.retail.enums.ProductImageType;
import in.rssquarz.retail.model.Product;
import in.rssquarz.retail.model.ProductAttributes;
import in.rssquarz.retail.model.ProductImage;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;

public class ProductBO {
	private Long id;

	private String name;
	private HashMap<String, List<ProductAttributes>> mapOfAttributes = new HashMap<String, List<ProductAttributes>>();
	private Collection<ProductAttributes> attributes = new ArrayList<ProductAttributes>();
	private String category;
	private String subCategory;
	private Collection<ProductImage> images = new ArrayList<ProductImage>();
	private BigDecimal defaultPrice;
	private String type;
	private String defaultImage;

	public ProductBO(Product entity) {
		id = entity.getId();
		name = entity.getName();
		category = entity.getCategory();
		subCategory = entity.getSubCategory();
		defaultPrice = entity.getDefaultPrice();
		images = entity.getImages();
		attributes = entity.getAttributes();
		// mapDefaultImage();
		mapAttributesInMap();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public BigDecimal getDefaultPrice() {
		return defaultPrice;
	}

	public void setDefaultPrice(BigDecimal defaultPrice) {
		this.defaultPrice = defaultPrice;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public String getSubCategory() {
		return subCategory;
	}

	public void setSubCategory(String subCategory) {
		this.subCategory = subCategory;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public Collection<ProductImage> getImages() {
		return images;
	}

	public void setImages(Collection<ProductImage> images) {
		this.images = images;
	}

	public String getDefaultImage() {
		return defaultImage;
	}

	public void mapDefaultImage() {
		for (ProductImage image : images) {
			if (image.getImageType().equalsIgnoreCase(
					ProductImageType.DEFAULT.getType())) {
				defaultImage = image.getProductImage();
			}
		}

	}

	public HashMap<String, List<ProductAttributes>> getMapOfAttributes() {
		return mapOfAttributes;
	}

	public void setMapOfAttributes(
			HashMap<String, List<ProductAttributes>> mapOfAttributes) {
		this.mapOfAttributes = mapOfAttributes;
	}

	public Collection<ProductAttributes> getAttributes() {
		return attributes;
	}

	public void setAttributes(Collection<ProductAttributes> attributes) {
		this.attributes = attributes;
	}

	private void mapAttributesInMap() {
		for (ProductAttributes pa : attributes) {
			String type = pa.getType();
			if (mapOfAttributes.containsKey(type)) {
				mapOfAttributes.get(type).add(pa);
			} else {
				List<ProductAttributes> list = new ArrayList<ProductAttributes>();
				list.add(pa);
				mapOfAttributes.put(type, list);
			}

		}
	}

}
