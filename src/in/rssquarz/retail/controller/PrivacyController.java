package in.rssquarz.retail.controller;

import in.rssquarz.retail.bo.Request;
import in.rssquarz.retail.utiliy.MailUtility;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class PrivacyController extends BaseController {

	@Autowired
	private MailUtility mailUtility;

	@RequestMapping(value = "/privacyPolicy", method = RequestMethod.GET)
	private ModelAndView privacy() {
		ModelAndView mnv = new ModelAndView("privacyPolicy");
		init(mnv);
		return mnv;
	}

	@RequestMapping(value = "/requestCallBack", method = RequestMethod.POST)
	private String requestCallBack(@RequestBody Request request) {

		try {
			System.out.println(request.getSessionToken());
			mailUtility.sendMail("honeybuzz1234@gmail.com",
					"Call Back Request " + request.getSessionToken(),
					"Call Back Request");

			return "success";
		} catch (Exception e) {
			e.printStackTrace();
			return "fail";
		}

	}
}
