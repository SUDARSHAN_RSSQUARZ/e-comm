package in.rssquarz.retail.controller;

import in.rssquarz.retail.model.Page;
import in.rssquarz.retail.service.CategoryService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.servlet.ModelAndView;

@Controller
public abstract class BaseController {

	@Autowired
	private CategoryService categoryService;

	public void init(ModelAndView modelAndView) {
		modelAndView.addObject("topNav", categoryService.getTopNav());
	}

	public Page getPage(String category) {
		return categoryService.getPage(category);
	}

}
