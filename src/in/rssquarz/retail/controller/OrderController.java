package in.rssquarz.retail.controller;

import in.rssquarz.retail.dao.OrderDao;
import in.rssquarz.retail.model.Orders;
import in.rssquarz.retail.model.Product;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class OrderController {

	@Autowired
	private OrderDao orderDao;

	@RequestMapping(value = "/orders", method = RequestMethod.GET)
	public ModelAndView orders(ModelAndView model) {
		try {
			model.addObject("getPins", orderDao.getPinsData());
			model.addObject("orderList", orderDao.orders());
			model.setViewName("orders");
		} catch (Exception e) {
			e.printStackTrace();
		}
		return model;
	}
	@RequestMapping(value = "/getUnCheckedOrders", method = RequestMethod.POST)
	public ModelAndView getUnCheckedOrders(ModelAndView model) {
		try {
			model.addObject("getPins", orderDao.getPinsData());
			model.addObject("orderList", orderDao.getUnCheckedOrders());
			model.setViewName("orders");
		} catch (Exception e) {
			e.printStackTrace();
		}
		return model;
	}
	
	@RequestMapping(value = "/ordesByArea", method = { RequestMethod.GET, RequestMethod.POST })
	private ModelAndView getOrdersByArea(@RequestParam("pincode") String pincode) {
		List<Orders> orderList = orderDao.getOrderByArea(pincode);
		ModelAndView mnv = new ModelAndView();
		mnv.addObject("getPins", orderDao.getPinsData());
		mnv.addObject("orderList", orderList);
		mnv.setViewName("orders");
		return mnv;
	}
	
	@RequestMapping(value = "/ordersByDate", method = { RequestMethod.GET, RequestMethod.POST })
	private ModelAndView getOrdersByDate(@RequestParam("orderDate") String orderDate) throws Exception {
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		Date orderDate1 = formatter.parse(orderDate);
		List<Orders> orderList = orderDao.getOrderByDate(orderDate1);
		ModelAndView mnv = new ModelAndView();
		mnv.addObject("getPins", orderDao.getPinsData());
		mnv.addObject("orderList", orderList);
		mnv.setViewName("orders");
		return mnv;
	}
	
	

	
	@RequestMapping(value = "/checkorder", method = RequestMethod.GET)
	public ModelAndView checkOrder(@RequestParam Long id) {

		orderDao.checkOrder(id);
		return new ModelAndView("redirect:orders");
	}
	
	@SuppressWarnings("rawtypes")
	@RequestMapping(value = "/getOrders", method = RequestMethod.GET)
	public @ResponseBody List  getOrdersForGraph(ModelAndView model) {
		List orderList = new ArrayList();
		try {
			
			orderList = orderDao.getOrdersForChart();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return orderList;
	}

	@RequestMapping(value = "/order/{id}", method = RequestMethod.GET)
	public ModelAndView getOrderById(@PathVariable Long id, ModelAndView model) {

		Orders order = orderDao.getOrderById(id);
		List<Product> productList = orderDao.getOrderItems(order.getItems());
		model.addObject("order", order);
		model.addObject("productList", productList);
		model.setViewName("viewOrder");
		return model;
	}
}
