package in.rssquarz.retail.controller;

import in.rssquarz.retail.bo.ErrorCode;
import in.rssquarz.retail.bo.Request;
import in.rssquarz.retail.bo.Response;
import in.rssquarz.retail.dao.CartDao;
import in.rssquarz.retail.model.MobileUser;
import in.rssquarz.retail.model.OrderItems;
import in.rssquarz.retail.model.Orders;
import in.rssquarz.retail.model.Product;
import in.rssquarz.retail.model.ProductAttributes;
import in.rssquarz.retail.service.CartService;
import in.rssquarz.retail.service.CheckoutService;
import in.rssquarz.retail.service.ProductService;
import in.rssquarz.retail.utiliy.MailUtility;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.codehaus.jackson.map.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class RestProductController {
	@Autowired
	private ProductService proServiceImpl;

	@Autowired
	private CartService cartService;

	@Autowired
	private CheckoutService checkoutService;

	@Autowired
	private MailUtility mailUtility;

	@Autowired
	private CartDao cartDao;

	@RequestMapping(value = "/getPrice", method = RequestMethod.GET)
	private BigDecimal getPrice(@RequestParam("attributeId") long attributeId) {
		ProductAttributes a = proServiceImpl
				.getProductPriceFromAttribute(attributeId);
		System.out.println(a);
		if (a != null)
			return a.getPrice();
		else
			return new BigDecimal(0);
	}

	@RequestMapping(value = "/mProduct/{category}", method = RequestMethod.GET)
	private Response findProductsByCategory(@PathVariable String category) {
		Response response = new Response();
		try {
			List<Product> productsByCategory = proServiceImpl
					.productsByCategory(category);
			response.setData(productsByCategory);
			response.setCode("200");
			response.setMessage("Success");
		} catch (Exception e) {
			ErrorCode error = new ErrorCode();
			response.setCode("500");
			error.setErrorCode("500");
			error.setErrorMessage(e.getMessage());
			response.setErrorCode(error);
		}
		return response;
	}

	@RequestMapping(value = "/mProduct/featured", method = RequestMethod.GET)
	private Response findFeaturedProducts() {
		Response response = new Response();
		try {
			List<Product> featuredProduct = proServiceImpl
					.findFeaturedProduct();
			response.setData(featuredProduct);
			response.setCode("200");
			response.setMessage("Success");
		} catch (Exception e) {
			ErrorCode error = new ErrorCode();
			response.setCode("500");
			error.setErrorCode("500");
			error.setErrorMessage(e.getMessage());
			response.setErrorCode(error);
		}
		return response;
	}

	@RequestMapping(value = "/mAddToBasket", method = RequestMethod.POST)
	private Response addToBasket(@RequestParam Long id,
			@RequestBody Request request) {
		Response result = new Response();
		try {
			String token = request.getSessionToken();
			System.out.println("toke " + token);
			if (cartService.addToCart(id, token)) {
				result.setCode("200");
				result.setMessage("Product Added to basket");
			} else {
				result.setCode("300");
				result.setMessage("User not found. Get session token again");
			}

		} catch (Exception e) {
			ErrorCode error = new ErrorCode();
			error.setErrorCode("500");
			result.setCode("500");
			error.setErrorMessage(e.getMessage());
			result.setErrorCode(error);
		}
		return result;
	}

	@RequestMapping(value = "/mRemove", method = RequestMethod.POST)
	public Response removeItemFromBasket(@RequestParam("itemId") Long itemId,
			@RequestBody Request request) {
		System.out.println("RestProductController /mRemove=" + itemId);
		Response response = new Response();
		try {
			String token = request.getSessionToken();
			String productName = cartService.removeFromCart(itemId, token);
			if (productName != null) {
				response.setCode("200");
				response.setMessage("product " + productName
						+ " removed from the basket");
				response.setData(productName);
			} else {
				response.setCode("300");
				response.setMessage("No products found in basket");
			}
		} catch (Exception ex) {
			ErrorCode error = new ErrorCode();
			error.setErrorCode("500");
			response.setCode("500");
			error.setErrorMessage(ex.getMessage());
			response.setErrorCode(error);
		}
		return response;
	}

	@RequestMapping(value = "/mViewBasket", method = RequestMethod.POST)
	private Response viewBasket(@RequestBody Request request) {
		Response response = new Response();
		try {
			String token = request.getSessionToken();
			List<Product> productList = cartService.getCart(token);
			if (productList == null || productList.size() == 0) {
				response.setCode("300");
				response.setMessage("Cart empty");
				response.setData(null);
			} else {
				response.setData(productList);
				response.setCode("200");
				response.setMessage("Products in Cart");
			}
		} catch (Exception ex) {
			ErrorCode error = new ErrorCode();
			error.setErrorCode("500");
			response.setCode("500");
			error.setErrorMessage(ex.getMessage());
			response.setErrorCode(error);
		}
		return response;
	}

	@RequestMapping(value = "/mCheckout", method = RequestMethod.POST)
	public Response checkout(@RequestBody Request request) {
		System.out.println("CheckoutController /checkout=");
		Response result = new Response();
		String token = request.getSessionToken();
		Orders order = null;
		try {
			List<Product> productList = cartService.getCart(token);
			if (productList == null || productList.size() == 0) {
				result.setMessage("Cart is empty. Please add products");
				result.setCode("300");
				result.setData(null);
			} else {
				// Orders order = new Orders();
				// Arrays.asList(request.getData().as(Orders.class));
				ObjectMapper mapper = new ObjectMapper();
				order = mapper.convertValue(request.getData(), Orders.class);
				List<OrderItems> items = new ArrayList<OrderItems>();
				StringBuffer sb = new StringBuffer();
				for (Product p : productList) {
					sb.append(p.getName());
					OrderItems item = new OrderItems();
					List<ProductAttributes> pa = (List<ProductAttributes>) p
							.getAttributes();
					sb.append(pa.get(0).getValue() + "\n");
					item.setOrder(order);
					item.setProductId(pa.get(0).getId());
					items.add(item);
				}
				order.setItems(items);
				MobileUser mUser = cartDao.findUserByToken(token);
				order.setmUser(mUser);
				String orderId = checkoutService.saveOrder(order);
				cartService.emptyCart(token);
				result.setCode("200");
				result.setMessage("Order has been placed successfully, your order no is "
						+ orderId);
				result.setData(orderId);
				try {
					StringBuffer sb1 = new StringBuffer();
					sb1.append(order.toString());
					sb1.append("\n");
					System.out.println(sb1.append(sb.toString()).toString());
					mailUtility.sendMail("honeybuzz1234@gmail.com",
							sb1.toString(), "New Order ");
					mailUtility.sendMail(order.getEmail(), sb1.toString(),
							"Honey Buzz Order Confirmation");

				} catch (Exception e) {
					System.out.println("Could not send email for "
							+ order.getId());
				}
			}
		} catch (Exception ex) {
			ErrorCode error = new ErrorCode();
			error.setErrorCode("500");
			result.setCode("500");
			error.setErrorMessage(ex.getMessage());
			result.setErrorCode(error);
		}
		return result;
	}

	@RequestMapping(value = "/mProduct", method = RequestMethod.GET)
	private Response getProductById(Long id) {
		Response response = new Response();
		try {
			Product productById = proServiceImpl.findProductById(id);
			response.setData(productById);
			if (productById == null) {
				response.setCode("400");
				response.setMessage("InValid product");
			} else {
				response.setCode("200");
				response.setMessage("Product Information");
			}
		} catch (Exception e) {
			ErrorCode error = new ErrorCode();
			error.setErrorCode("500");
			response.setCode("500");
			error.setErrorMessage(e.getMessage());
		}
		return response;
	}

	@RequestMapping(value = "/mUser", method = RequestMethod.POST)
	private Response addNewUser(@RequestBody MobileUser mUser) {
		Response response = new Response();
		try {
			String token = cartService.addUser(mUser);
			if (token == null || token.length() == 0) {
				response.setCode("300");
				response.setMessage("Rerty");
				response.setData(null);
			} else {
				response.setCode("200");
				response.setMessage("User Added successfully");
				response.setData(token);
			}

		} catch (Exception ex) {
			ErrorCode error = new ErrorCode();
			error.setErrorCode("500");
			response.setCode("500");
			error.setErrorMessage(ex.getMessage());
			response.setErrorCode(error);
		}
		return response;
	}

}
