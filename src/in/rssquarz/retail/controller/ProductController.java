package in.rssquarz.retail.controller;

import java.io.IOException;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import com.mongodb.BasicDBObject;
import com.mongodb.BasicDBObjectBuilder;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;
import com.mongodb.MongoClient;
import com.mongodb.ServerAddress;

import in.rssquarz.retail.bo.ProductBO;
import in.rssquarz.retail.dao.GenericCategoryDao;
import in.rssquarz.retail.dao.ProductDao;
import in.rssquarz.retail.dao.WebsiteDao;
import in.rssquarz.retail.model.Attribute;
import in.rssquarz.retail.model.Banner;
import in.rssquarz.retail.model.Category;
import in.rssquarz.retail.model.Coupons;
import in.rssquarz.retail.model.GenericAttributeDetails;
import in.rssquarz.retail.model.GenericProductDetails;
import in.rssquarz.retail.model.LogisticPartner;
import in.rssquarz.retail.model.Product;
import in.rssquarz.retail.model.ProductAttributePrice;
import in.rssquarz.retail.model.SubAttributes;
import in.rssquarz.retail.service.CategoryService;
import in.rssquarz.retail.service.ProductService;
import in.rssquarz.retail.utiliy.FileUtility;

@Controller
public class ProductController extends BaseController {

	@Autowired
	private ProductService proServiceImpl;

	@Autowired
	private CategoryService categoryService;

	@Autowired
	private ProductDao productDao;

	@Autowired
	private FileUtility fileUtility;

	@Autowired
	private WebsiteDao websiteDao;

	@Autowired
	private GenericCategoryDao categoryDao;

	@RequestMapping(value = "/", method = RequestMethod.GET)
	private ModelAndView startUp() {
		ModelAndView mnv = new ModelAndView();
		init(mnv);
		List<Banner> bannerList = websiteDao.getBanners();
		mnv.addObject("bannerList", bannerList);
		mnv.setViewName("index");
		mnv.addObject("indexPageProduct", proServiceImpl.findProductsByType());
		return mnv;
	}

	@RequestMapping(value = "/product/{category}/{subCategory}", method = { RequestMethod.GET, RequestMethod.POST })
	private ModelAndView findProductsByCategory(@PathVariable String category, @PathVariable String subCategory) {
		List<Product> productsByCategory = proServiceImpl.findProductsBySubCategory(category, subCategory);
		ModelAndView mnv = new ModelAndView();
		init(mnv);
		mnv.addObject("productList", productsByCategory);
		mnv.setViewName("productSubCategory");
		return mnv;
	}

	@RequestMapping(value = "/productByCategory", method = { RequestMethod.GET, RequestMethod.POST })
	private ModelAndView findProductsByOnlyCategory(@RequestParam("category") String category) {
		List<Product> productsByCategory = proServiceImpl.findProductsCategory(category);
		ModelAndView mnv = new ModelAndView();
		init(mnv);
		mnv.addObject("productList", productsByCategory);
		mnv.addObject("productListForSearch", proServiceImpl.getAllProducts());
		mnv.setViewName("productList");
		return mnv;
	}

	@RequestMapping(value = "/productByName", method = { RequestMethod.GET, RequestMethod.POST })
	private ModelAndView findProductsByName(@RequestParam("name") String name) {
		List<Product> productsByCategory = proServiceImpl.findProductsByName(name);
		ModelAndView mnv = new ModelAndView();
		init(mnv);
		mnv.addObject("productList", productsByCategory);
		mnv.addObject("productListForSearch", proServiceImpl.getAllProducts());
		mnv.setViewName("productList");
		return mnv;
	}

	@RequestMapping(value = "/product/{id}", method = { RequestMethod.GET, RequestMethod.POST })
	private ModelAndView findProductsById(@PathVariable Long id, HttpServletRequest request) {
		ProductBO productById = proServiceImpl.findProductBoById(id);
		String status = request.getParameter("status");
		ModelAndView mnv = new ModelAndView();
		init(mnv);
		if (status != null && status.equalsIgnoreCase("1")) {
			mnv.addObject("status", "Product " + productById.getName() + " has been added in your basket.");
		}
		mnv.addObject("product", productById);
		mnv.setViewName("productDetails");
		return mnv;
	}

	@RequestMapping(value = "/prepareEditProduct/{id}", method = { RequestMethod.GET })
	private ModelAndView prepareEditProduct(@PathVariable Long id, ModelAndView model) {
		ObjectMapper mapper = new ObjectMapper();
		try {
			Product product = productDao.findProductsById(id);
			model.addObject("product", product);
			model.addObject("topNavString", mapper.writeValueAsString(categoryService.getTopNav()));
			model.addObject("topNav", categoryService.getTopNav());
		} catch (Exception e) {
			e.printStackTrace();
		}
		model.setViewName("editProduct");
		return model;
	}

	@RequestMapping(value = "/product", method = RequestMethod.POST)
	private ModelAndView addProduct(@ModelAttribute Product product, ModelAndView model,
			@RequestParam("file") MultipartFile[] files, HttpServletRequest request) {
		List<String> nameList = fileUtility.saveImage(files, request.getSession().getServletContext().getRealPath("/"));
		if (null == nameList) {
			return model;
		}
		proServiceImpl.saveProduct(product, nameList);
		model.addObject("productList", proServiceImpl.getAllProducts());
		model.setViewName("productList");
		return model;
	}

	@RequestMapping(value = "/updateProduct", method = RequestMethod.POST)
	private ModelAndView updateProduct(@ModelAttribute Product product, ModelAndView model,
			@RequestParam("file") MultipartFile[] files, HttpServletRequest request) {
		List<String> nameList = fileUtility.saveImage(files, request.getSession().getServletContext().getRealPath("/"));
		if (null == nameList) {
			return model;
		}
		proServiceImpl.saveProduct(product, nameList);
		model.addObject("productList", proServiceImpl.getAllProducts());
		model.setViewName("productList");
		return model;
	}

	@RequestMapping(value = "/products", method = RequestMethod.GET)
	private ModelAndView getAllProducts(ModelAndView model) throws Exception, JsonMappingException, IOException {
		ObjectMapper mapper = new ObjectMapper();
		model.addObject("topNavString", mapper.writeValueAsString(categoryService.getTopNav()));
		model.addObject("topNav", categoryService.getTopNav());
		model.addObject("productList", proServiceImpl.getAllProducts());
		model.addObject("productListForSearch", proServiceImpl.getAllProducts());
		model.setViewName("productList");
		return model;
	}

	@RequestMapping(value = "/products/{category}", method = { RequestMethod.GET, RequestMethod.POST })
	private ModelAndView prodByCat(@PathVariable String category) {
		HashMap<String, List<Product>> productsByCategory = proServiceImpl.findProductsByCategory(category);
		ModelAndView mnv = new ModelAndView();
		init(mnv);
		mnv.addObject("productList", productsByCategory);
		// This is for sub categorizing only Cakes
		if (category.equalsIgnoreCase("CAKES")) {
			mnv.addObject("showSubCat", true);
		} else {
			mnv.addObject("showSubCat", false);
		}
		mnv.addObject("page", getPage(category));
		mnv.setViewName("productSubCategory");
		return mnv;
	}

	@RequestMapping(value = "/deleteProduct", method = { RequestMethod.GET })
	private ModelAndView deleteProduct(@RequestParam Long id) {
		Product product = productDao.findProductsById(id);
		productDao.deleteProduct(id, product);
		return new ModelAndView("redirect:products");
	}

	@RequestMapping(value = "/deleteAttribute/{pid}/{id}", method = { RequestMethod.GET })
	private ModelAndView deleteProduct(@PathVariable Long pid, @PathVariable Long id, ModelAndView model) {

		Product product = productDao.findProductsById(id);
		productDao.deleteAttribute(pid, product);
		ModelAndView mnv = new ModelAndView("dashboard", "command", new Category());
		model.setViewName("dashboard");
		mnv.addObject("status", "Attribute Deleted");
		return mnv;
	}

	@RequestMapping(value = "/prepareAddQuantity/{pid}/{id}", method = { RequestMethod.GET })
	private ModelAndView prepareAddQuantity(@PathVariable Long pid, @PathVariable Long id, ModelAndView model) {

		ProductAttributePrice pap = productDao.findAttributeByPriceId(id);
		model.addObject("pap", pap);
		model.setViewName("editQuantity");
		return model;
	}

	@RequestMapping(value = "/editQuantity", method = { RequestMethod.POST })
	private ModelAndView addQuantity(@ModelAttribute ProductAttributePrice pap, ModelAndView model) {

		try {
			ProductAttributePrice pap1 = productDao.findAttributeByPriceId(pap.getId());
			Product product = productDao.findProductsById(pap1.getProduct().getId());
			for (ProductAttributePrice pap2 : product.getProductPrice()) {
				pap2.setProduct(product);
				if (pap2.getId() == pap.getId()) {
					pap2.setQuantity(pap.getQuantity());
				}
				productDao.addQuantity(pap2);
			}

			model.addObject("status", "Quantity updated....");
			return new ModelAndView("redirect:products");
		} catch (Exception e) {
			e.printStackTrace();
			model.addObject("errorstatus", "Error in updating quantity....");
			return new ModelAndView("redirect:products");
		}
	}

	@RequestMapping(value = "/viewProductDetails/{id}", method = { RequestMethod.GET })
	private ModelAndView viewProduct(@PathVariable Long id, ModelAndView model) {
		ObjectMapper mapper = new ObjectMapper();
		try {
			ArrayList<ProductAttributePrice> attributesList = new ArrayList<ProductAttributePrice>();
			Product product = productDao.findProductsById(id);
			model.addObject("product", product);
			attributesList = productDao.getAttributes(product);
			model.addObject("attributesList", attributesList);
			model.addObject("topNavString", mapper.writeValueAsString(categoryService.getTopNav()));
			model.addObject("topNav", categoryService.getTopNav());
		} catch (Exception e) {
			e.printStackTrace();
		}

		model.setViewName("viewProduct");
		return model;
	}

	@RequestMapping(value = "/preparepincodes", method = { RequestMethod.GET })
	private ModelAndView preparePincodes(ModelAndView model) {
		model.setViewName("importPincodes");
		return model;
	}

	@RequestMapping(value = "/importpincodes", method = RequestMethod.POST)
	private ModelAndView importPincodes(@ModelAttribute LogisticPartner logisticPartner, ModelAndView model,
			@RequestParam("file") MultipartFile file) {
		try {
			proServiceImpl.savePincodes(logisticPartner, file);
			model.addObject("status", "Pincodes imported successfully");
			model.setViewName("importPincodes");
			return model;
		} catch (Exception e) {
			e.printStackTrace();
			model.addObject("errorstatus", "Something went wrong");
			return model;
		}
	}

	@RequestMapping(value = "/checkpincode/{pincodenum}", method = RequestMethod.GET)
	private @ResponseBody String checkPincode(@PathVariable String pincodenum) {
		String msg = null;
		try {
			boolean status = productDao.checkPincode(pincodenum);
			if (status) {
				msg = "success";
			} else {
				msg = "error";
			}

		} catch (Exception e) {
			e.printStackTrace();

		}
		return msg;
	}

	@RequestMapping(value = "/coupons", method = { RequestMethod.GET })
	private ModelAndView prepareCoupons(ModelAndView model) {
		return new ModelAndView("coupons", "command", new Coupons());
	}

	@RequestMapping(value = "/coupons", method = { RequestMethod.POST })
	private ModelAndView addCoupons(@ModelAttribute Coupons coupon, ModelAndView model) {

		try {
			model = new ModelAndView("coupons", "command", new Coupons());
			productDao.saveCoupon(coupon);
			model.addObject("status", "Coupon saved successfully");
			return model;
		} catch (Exception e) {
			e.printStackTrace();
			model.addObject("errorstatus", "Could not save coupon");
			return model;
		}
	}

	@RequestMapping(value = "/checkcoupon/{couponcode}", method = RequestMethod.GET)
	private @ResponseBody Coupons checkCoupon(@PathVariable String couponcode) {
		Coupons coupon = null;
		try {
			coupon = productDao.checkCoupon(couponcode);
		} catch (Exception e) {
			e.printStackTrace();

		}
		return coupon;
	}

	@RequestMapping(value = "/outOfStockProducts", method = RequestMethod.POST)
	private ModelAndView getOutOfStockProducts(ModelAndView model) throws Exception, Exception, IOException {
		ObjectMapper mapper = new ObjectMapper();
		model.addObject("topNavString", mapper.writeValueAsString(categoryService.getTopNav()));
		model.addObject("topNav", categoryService.getTopNav());
		model.addObject("productList", proServiceImpl.getAllOutOfStockProducts());
		model.setViewName("outOfStockProducts");
		return model;
	}

	/* ****** Generic Product *********/

	@RequestMapping(value = "/prepareAddAttributes", method = RequestMethod.GET)
	private ModelAndView prepareAddAttributes(ModelAndView model) throws Exception, Exception, IOException {
		model.setViewName("addAttributes");
		model.addObject("attribute", productDao.getAllAttributes());
		return model;
	}

	@RequestMapping(value = "/addAttributes", method = { RequestMethod.POST })
	private ModelAndView addAttributes(@ModelAttribute Attribute attributes, ModelAndView model) {
		try {
			model.setViewName("addAttributes");
			productDao.addAttributeDetails(attributes);
			model.addObject("status", "Details saved successfully");
			return model;
		} catch (Exception e) {
			e.printStackTrace();
			model.addObject("errorstatus", "Could not save details");
			return model;
		}
	}

	@RequestMapping(value = "/addSubAttributes", method = { RequestMethod.POST })
	private ModelAndView addSubAttributes(@ModelAttribute SubAttributes subAttributes, ModelAndView model) {
		try {
			model.setViewName("addAttributes");
			model.addObject("attribute", productDao.getAllAttributes());
			productDao.addSubAttributeDetails(subAttributes);
			model.addObject("status", "Details saved successfully");
			return model;
		} catch (Exception e) {
			e.printStackTrace();
			model.addObject("errorstatus", "Could not save details");
			return model;
		}
	}

	@RequestMapping(value = "/prepareAddProductDetails", method = RequestMethod.GET)
	private ModelAndView prepareAddProductDetails(ModelAndView model) throws Exception, Exception, IOException {
		model.addObject("attributeList", productDao.getAllAttributes());
		model.setViewName("addProductAttributes");
		return model;
	}

	@RequestMapping(value = "/prepareAddProductDetails1", method = { RequestMethod.POST })
	private ModelAndView addProductDetails(@ModelAttribute Attribute productAttributes, ModelAndView model) {
		try {
			ObjectMapper mapper = new ObjectMapper();
			Map<String, List<String>> categoryMap = categoryDao.retrieveCategoryList();
			model.addObject("categoryMap", categoryMap);
			model.addObject("categoryMapString", mapper.writeValueAsString(categoryMap));

			Map<String, List<SubAttributes>> map = new HashMap<String, List<SubAttributes>>();
			for (Attribute productAttributes1 : productAttributes.getSelectedAttributes()) {
				Long id = productAttributes1.getId();
				if (productAttributes1.isCheckStatus() == true) {
					List<SubAttributes> subatts = productDao.getSubAttributes(id);
					map.put(productAttributes1.getAttributeName(), subatts);
				}
			}
			model.setViewName("addProductDetail");
			model.addObject("attributeDetails", map);
			model.addObject("subAttributeList", productDao.getAllSubAttributes());
			model.addObject("topNav", categoryService.getTopNav());
			return model;
		} catch (Exception e) {
			e.printStackTrace();
			return model;
		}
	}

	@RequestMapping(value = "/saveProductDetails", method = { RequestMethod.POST })
	private ModelAndView saveProductDetails(@ModelAttribute GenericProductDetails gp,
			@ModelAttribute GenericAttributeDetails ga, ModelAndView model) {

		try {
			ObjectMapper mapper = new ObjectMapper();
			model.setViewName("addProductAttributes");
			Map<String, List<String>> categoryMap = categoryDao.retrieveCategoryList();
			model.addObject("categoryMap", categoryMap);
			model.addObject("categoryMapString", mapper.writeValueAsString(categoryMap));

			final String DB_NAME = "Attributes";
			final String COLLECTION = "Hibernate_Demo";
			Long id = productDao.saveGenericProduct(gp);
			SubAttributes sub = null;
			GenericAttributeDetails gat = null;
			List<GenericAttributeDetails> gatList = new ArrayList<GenericAttributeDetails>();
			for (SubAttributes gp1 : ga.getSubAttributesDetails()) {
				gat = new GenericAttributeDetails();
				List<SubAttributes> subList = new ArrayList<SubAttributes>();
				String[] subattributesName = ga.getAttributename().split(",");
				String[] attributesName = gp1.getSubAttributeName().split(",");
				String[] attributesValue = gp1.getDefaultValueOfAttribute().split(",");
				for (int i = 0; i < attributesName.length; i++) {
					for (int j = 0; j < attributesValue.length; j++) {
						if (i == j) {
							sub = new SubAttributes();
							sub.setSubAttributeName(attributesName[i]);
							sub.setDefaultValueOfAttribute(attributesValue[j]);
							gat.setAttributename(subattributesName[i]);
							subList.add(sub);
						}
					}
				}

				gat.setSubAttributesDetails(subList);
				gatList.add(gat);

			}
			gp.setSubAttributesDetails(gatList);

			MongoClient mongo = new MongoClient("localhost", 27017);
			MongoOperations mongoOps = new MongoTemplate(mongo, DB_NAME);
			mongoOps.insert(gp, COLLECTION);
			model.addObject("attributeList", productDao.getAllAttributes());
			model.addObject("status", "Details saved successfully");
			return model;
		} catch (Exception e) {
			e.printStackTrace();
			model.addObject("errorstatus", "Could not save details");
			return model;
		}

	}

	@RequestMapping(value = "/showGenericProductDetails", method = RequestMethod.GET)
	private ModelAndView showGenericProductDetails(ModelAndView model) throws Exception, Exception, IOException {
		final String DB_NAME = "Attributes";
		final String COLLECTION = "Hibernate_Demo";
		MongoClient mongo = new MongoClient("localhost", 27017);
		MongoOperations mongoOps1 = new MongoTemplate(mongo, DB_NAME);
		List<GenericProductDetails> listProduct = mongoOps1.findAll(GenericProductDetails.class, COLLECTION);
		model.addObject("genericProductList", listProduct);
		model.setViewName("showGenericProductList");
		return model;
	}

	@RequestMapping(value = "/deleteGenericProduct", method = { RequestMethod.GET })
	private ModelAndView deleteGenericProduct(@RequestParam Long id) throws UnknownHostException {
		MongoClient mongo = new MongoClient(new ServerAddress("localhost", 27017));
		DB db = mongo.getDB("Attributes");
		DBCollection collection = db.getCollection("Hibernate_Demo");
		BasicDBObject query = new BasicDBObject();
		query.append("productId", id);
		collection.remove(query);
		return new ModelAndView("redirect:showGenericProductDetails");
	}

	@RequestMapping(value = "/prepareEditGenericProduct/{productId}", method = { RequestMethod.GET })
	private ModelAndView prepareEditGenericProduct(@PathVariable Long productId, ModelAndView model) {
		ObjectMapper mapper = new ObjectMapper();
		try {
			model.setViewName("addProductDetail");
			final String DB_NAME = "Attributes";
			final String COLLECTION = "Hibernate_Demo";
			MongoClient mongo = new MongoClient("localhost", 27017);
			MongoOperations mongoOps2 = new MongoTemplate(mongo, DB_NAME);
			Query query2 = new Query();
			query2.addCriteria(Criteria.where("productId").is(productId));
			List<GenericProductDetails> product = mongoOps2.find(query2, GenericProductDetails.class, COLLECTION);
			GenericProductDetails gp  = new GenericProductDetails();
			for(GenericProductDetails gpd:product)
			{
				gp.setProductId(gpd.getProductId());
				gp.setProductTitle(gpd.getProductTitle());
				gp.setCategory(gpd.getCategory());
				gp.setProductDescription(gpd.getProductDescription());
				gp.setDefaultValue(gpd.getDefaultValue());
				gp.setSubAttributesDetails(gpd.getSubAttributesDetails());
				
			}
			model.addObject("gp", gp);

			model.addObject("topNavString", mapper.writeValueAsString(categoryService.getTopNav()));
			model.addObject("topNav", categoryService.getTopNav());
			return model;
		} catch (Exception e) {
			e.printStackTrace();
		}
		model.setViewName("addProductDetail");
		return model;
	}

}
