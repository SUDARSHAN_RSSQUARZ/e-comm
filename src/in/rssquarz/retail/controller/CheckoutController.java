package in.rssquarz.retail.controller;

import in.rssquarz.retail.dao.CartDao;
import in.rssquarz.retail.model.Cart;
import in.rssquarz.retail.model.MobileUser;
import in.rssquarz.retail.model.OrderItems;
import in.rssquarz.retail.model.Orders;
import in.rssquarz.retail.model.Payu;
import in.rssquarz.retail.model.Product;
import in.rssquarz.retail.model.ProductAttributes;
import in.rssquarz.retail.service.CartService;
import in.rssquarz.retail.service.CheckoutService;
import in.rssquarz.retail.service.ProductService;
import in.rssquarz.retail.utiliy.MD5Hash;
import in.rssquarz.retail.utiliy.MailUtility;
import in.rssquarz.retail.utiliy.Utils;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class CheckoutController extends BaseController {

	public static String SUCCESS = "1";
	public static String ERROR = "-1";

	@Autowired
	private CheckoutService checkoutService;

	@Autowired
	private ProductService productService;

	@Autowired
	private MailUtility mailUtility;

	@Autowired
	private CartDao cartDao;

	@Autowired
	private CartService cartService;

	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/addToBasket", method = RequestMethod.POST)
	public ModelAndView checkout(@ModelAttribute Product product, Model model,
			HttpServletRequest request) {
		HttpSession session = request.getSession();
		Long userId = (Long) session.getAttribute("wUserId");

		if (null != userId) {
			MobileUser mUser = cartDao.findUserById(userId);
			Cart itemInCart = new Cart();
			itemInCart.setmUser(mUser);
			itemInCart.setProductId(product.getId());
			System.out.println();
			cartDao.saveItemInCart(itemInCart);
		} else {

			if (session.getAttribute("productList") == null) {
				List<Product> productList = new ArrayList<Product>();
				productList.add(product);
				session.setAttribute("productList", productList);
				System.out
						.println("product list created and added to new session");
			} else {
				List<Product> productList = (List<Product>) session
						.getAttribute("productList");
				productList.add(product);
				session.setAttribute("productList", productList);
				System.out.println("product list added to exsting session");
			}
		}
		ModelAndView mnv = new ModelAndView();
		init(mnv);
		String status = "Product " + product.getName()
				+ " has been added in your basket.";
		mnv.setViewName("redirect:/product/" + product.getId() + "?status="
				+ SUCCESS);
		return mnv;
	}

	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/remove", method = RequestMethod.POST)
	public ModelAndView removeItemFromBasket(
			@RequestParam("itemId") int itemId, ModelAndView mnv,
			HttpServletRequest request, HttpServletResponse response) {
		System.out.println("CheckoutController /remove=" + itemId);

		HttpSession session = request.getSession();
		Long userId = (Long) session.getAttribute("wUserId");
		String productName = "";
		if (session.getAttribute("productList") != null) {
			List<Product> productList = (List<Product>) session
					.getAttribute("productList");
			productName = productList.get(itemId).getName();
			Long productId = productList.get(itemId).getId();
			if (null != userId) {
				cartService.removeFromWcart(productId, userId);
			}
			productList.remove(itemId);
			session.setAttribute("productList", productList);
			System.out.println("product list added to exsting session");
		}

		init(mnv);
		mnv.addObject("status", productName);
		mnv.setViewName("redirect:/order");
		return mnv;
	}

	@RequestMapping(value = "/order", method = { RequestMethod.GET,
			RequestMethod.POST })
	public ModelAndView placeOrder(Model model, HttpServletRequest request,
			HttpServletResponse response) {
		String status = request.getParameter("status");
		MobileUser mUser = null;
		HttpSession session = request.getSession();
		Long userId = (Long) session.getAttribute("wUserId");
		if (null != userId) {
			mUser = cartDao.findUserById(userId);
			List<Product> productList = cartService.getwUserCart(mUser);
			System.out.println("ProductList:"+productList);
			session.setAttribute("productList", productList);
		}
		ModelAndView mnv = new ModelAndView();
		init(mnv);
		mnv.addObject("mUser", mUser);
		if (status != null && status.equalsIgnoreCase(SUCCESS)) {
			mnv.addObject("status", "Product " + status
					+ " has been added in your basket.");
		}

		mnv.setViewName("checkout");
		return mnv;
	}

	@RequestMapping(value = "/checkout", method = RequestMethod.POST)
	public ModelAndView checkout(Model model,
			HttpServletRequest request, HttpServletResponse response,
 @ModelAttribute Orders order) {
		System.out.println("CheckoutController /checkout=");
		HttpSession session = request.getSession();
		ModelAndView newModel = new ModelAndView();
		if (session.getAttribute("productList") == null) {
			System.out
					.println("no product in users basket, send to products list or give error");
		} else {
			@SuppressWarnings("unchecked")
			List<Product> productList = (List<Product>) session
					.getAttribute("productList");
			System.out.println(productList.size()
					+ " products found in your basket");

			List<OrderItems> items = new ArrayList<OrderItems>();
			StringBuffer sb = new StringBuffer();
			for (Product p : productList) {
				sb.append(p.getName());
				List<ProductAttributes> pa = (List<ProductAttributes>) p
						.getAttributes();
				sb.append(pa.get(0).getValue() + "\n");
				OrderItems item = new OrderItems();
				item.setOrder(order);
				item.setProductId(pa.get(0).getId());
				items.add(item);
			}
			MobileUser mUser = new MobileUser();
			if (null != session.getAttribute("wUserId")) {
				mUser.setId((Long) session.getAttribute("wUserId"));
				cartService.emptyCart(mUser);
				order.setmUser(mUser);
			}

			order.setItems(items);
			// checkoutService.saveOrder(order);
			session.removeAttribute("productList");
			// session.invalidate();
			try {
				StringBuffer sb1 = new StringBuffer();
				sb1.append(order.toString());
				sb1.append("\n");
				System.out.println(sb1.append(sb.toString()).toString());

				// mailUtility.sendMail("honeybuzz1234@gmail.com",
				// sb1.toString(),
				// "New Order ");
				// mailUtility.sendMail(order.getEmail(), sb1.toString(),
				// "Honey Buzz Order Confirmation");
				// new SMSUtility().sendSms(order.getPhNo(), sb1.toString());

				Payu payu = new Payu();
				String txnId = Utils.getAlphaNumPass(4, 4);
				String prepareHash = "gtKFFx|" + txnId + "|" + order.getPrice()
						+ "|" + "New Order|" + order.getNameUser() + "|"
						+ order.getEmail() + "|||||||||||" + "eCwWELxi";
				String hash = MD5Hash.hashCal("SHA-512", prepareHash);
				System.out.println(prepareHash);
				payu.setHash(hash);
				payu.setKey("gtKFFx");
				payu.setSalt("eCwWELxi");
				payu.setTxnId(txnId);
				newModel.addObject("payu", payu);
				newModel.addObject("order", order);
				newModel.setViewName("payuform");
			} catch (Exception e) {
				System.out.println("Could not send email for " + order.getId());
			}
		}

		/*
		 * ModelAndView mnv = new ModelAndView(); mnv.addObject("status",
		 * SUCCESS); mnv.setViewName("redirect:success");
		 */
		// return mnv;
		return newModel;
	}

	@RequestMapping(value = "/success", method = RequestMethod.GET)
	public ModelAndView checkoutSuccess(Model model,
			HttpServletRequest request, HttpServletResponse response) {
		ModelAndView mnv = new ModelAndView();
		init(mnv);
		// sString status = request.getParameter("status");
		mnv.addObject("status", "Your order has been placed successfully!");
		mnv.setViewName("success");
		return mnv;
	}

	@RequestMapping(value = "/payu", method = RequestMethod.GET)
	public ModelAndView checkoutPayu(Model model, HttpServletRequest request,
			HttpServletResponse response, @RequestBody Orders order) {
		ModelAndView mnv = new ModelAndView();
		init(mnv);
		// sString status = request.getParameter("status");
		mnv.addObject("status", "Your order has been placed successfully!");
		mnv.setViewName("payuform");
		return mnv;
	}
}