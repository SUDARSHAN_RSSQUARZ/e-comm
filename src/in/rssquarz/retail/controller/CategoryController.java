package in.rssquarz.retail.controller;

import in.rssquarz.retail.model.Category;
import in.rssquarz.retail.service.CategoryService;

import java.util.HashMap;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class CategoryController {

	@Autowired
	private CategoryService categoryService;

	public CategoryService getCategoryService() {
		return categoryService;
	}

	public void setCategoryService(CategoryService categoryService) {
		this.categoryService = categoryService;
	}

	@RequestMapping(value = "/category", method = RequestMethod.GET)
	public ModelAndView categoryForm() {
		ModelAndView mnv = new ModelAndView("categories", "command",
				new Category());
		mnv.addObject("categoryMap", categoryService.getCategoryList());
		return mnv;
	}

	@RequestMapping(value = "/saveCategory", method = RequestMethod.POST)
	public ModelAndView saveCategoryForm(@ModelAttribute Category category, ModelAndView model) {
		boolean status = false;
		HashMap<String, String> categorylist = new HashMap<String, String>();
		try {
			status = categoryService.saveCategory(category);
			if (status == true) {
				model.setViewName("categories");
				ModelAndView mnv = new ModelAndView("categories", "command", new Category());
				mnv.addObject("categoryMap", categoryService.getCategoryList());
				mnv.addObject("status", "Category added successfully....");
				return mnv;
			} else {
				model.setViewName("categories");
				ModelAndView mnv = new ModelAndView("categories", "command", new Category());
				mnv.addObject("categoryMap", categoryService.getCategoryList());
				mnv.addObject("errorstatus", "Category Name already exists");
				return mnv;
			}
		} catch (Exception e) {
			e.getStackTrace();
			// TODO
			model.setViewName("categories");
			return model;
		}

	}

	@RequestMapping(value = "/saveSubCategory", method = RequestMethod.POST)
	public ModelAndView saveSubCategoryForm(@ModelAttribute Category category, ModelAndView model) {
		boolean status = false;
		HashMap<String, String> categorylist = new HashMap<String, String>();
		try {
			status = categoryService.saveSubCategory(category);
			if (status == true) {
				model.setViewName("categories");
				ModelAndView mnv = new ModelAndView("categories", "command", new Category());
				mnv.addObject("categoryMap", categoryService.getCategoryList());
				mnv.addObject("status", "Sub-Category added successfully....");
				return mnv;
			} else {
				model.setViewName("categories");
				ModelAndView mnv = new ModelAndView("categories", "command", new Category());
				mnv.addObject("categoryMap", categoryService.getCategoryList());
				mnv.addObject("errorstatus", "Sub-Category Name already exists");
				return mnv;
			}
		} catch (Exception e) {
			model.setViewName("categories");
			ModelAndView mnv = new ModelAndView("categories", "command", new Category());
			mnv.addObject("categoryMap", categoryService.getCategoryList());
			mnv.addObject("errorstatus", "Error in adding data");
			e.getStackTrace();
			return mnv;
		}

	}
}
