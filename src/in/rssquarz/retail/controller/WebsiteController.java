package in.rssquarz.retail.controller;

import in.rssquarz.retail.dao.CartDao;
import in.rssquarz.retail.dao.OrderDao;
import in.rssquarz.retail.dao.WebsiteDao;
import in.rssquarz.retail.model.Banner;
import in.rssquarz.retail.model.MobileUser;
import in.rssquarz.retail.model.Orders;
import in.rssquarz.retail.model.Product;
import in.rssquarz.retail.service.CartService;
import in.rssquarz.retail.utiliy.FileUtility;
import in.rssquarz.retail.utiliy.MD5Hash;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class WebsiteController {

	@Autowired
	private CartDao cartDao;

	@Autowired
	private CartService cartService;

	@Autowired
	private WebsiteDao websiteDao;

	@Autowired
	private FileUtility fileUtility;

	@Autowired
	private OrderDao orderDao;

	@RequestMapping(value = "/wUserRegister", method = RequestMethod.POST)
	public ModelAndView websiteRegister(@ModelAttribute MobileUser mUser,
			ModelAndView model) {

		// User registration from website
		try {
			model.setViewName("success");
			MobileUser mu = cartDao.checkEmail(mUser.getEmail());
			if (mu != null) {
				model.addObject("errorstatus", "Email id is already registered");
			} else {
				mUser.setPassword(MD5Hash.getHash(mUser.getPassword()));
				cartDao.savemUser(mUser);
				model.addObject("status", "Registration successful");
			}

		} catch (Exception e) {
			e.printStackTrace();
			model.addObject("errorstatus", "Something went wrong");
		}
		return model;
	}

	@RequestMapping(value = "/wUserLogin", method = RequestMethod.POST)
	public ModelAndView websiteLogin(@ModelAttribute MobileUser mUser,
			HttpServletRequest request,
			ModelAndView model) {

		// User login from website
		HttpSession session = request.getSession();
		mUser.setPassword(MD5Hash.getHash(mUser.getPassword()));
		model.setViewName("success");
		try {
			MobileUser mu = cartDao.wUserLogin(mUser);
			if (mu != null) {
				session.setAttribute("wUser", mu.getName());
				session.setAttribute("wUserId", mu.getId());
				session.removeAttribute("productList");
				model.addObject("status", "Login successful");

			} else {
				model.addObject("errorstatus", "Enter valid credentials");
			}

		} catch (Exception e) {
			e.printStackTrace();
			model.addObject("errorstatus", "Something went wrong");
		}
		return model;
	}

	@RequestMapping(value = "/mUsers", method = RequestMethod.GET)
	public ModelAndView mUsers(ModelAndView model) {

		// All registered users
		try {
			model.setViewName("users");
			List<MobileUser> mUserList = cartDao.getAllmUsers();
			model.addObject("userList", mUserList);
		} catch (Exception e) {
			e.printStackTrace();
			model.addObject("errorstatus", "Something went wrong");
		}
		return model;
	}

	@RequestMapping(value = "/getusercart/{id}", method = RequestMethod.GET)
	public ModelAndView getUserCart(@PathVariable Long id, ModelAndView model) {

		// Get user cart for backend
		try {
			model.setViewName("usercart");
			List<Product> productList = cartService.getUserCart(id);
			model.addObject("productList", productList);

			List<Orders> orders = orderDao.getUserOrders(id);
			model.addObject("orderList", orders);
		} catch (Exception e) {
			e.printStackTrace();
			model.addObject("errorstatus", "Something went wrong");
		}
		return model;
	}

	@RequestMapping(value = "/bannerimages", method = RequestMethod.GET)
	public ModelAndView prepareBannerImages(ModelAndView model) {
		List<Banner> bannerList = websiteDao.getBanners();
		model.addObject("bannerList", bannerList);
		model.setViewName("bannerimages");
		return model;
	}

	@RequestMapping(value = "/banner", method = RequestMethod.POST)
	public ModelAndView addBanner(@ModelAttribute Banner banner,
			@RequestParam("file") MultipartFile[] file,
			HttpServletRequest request,
			ModelAndView model) {

		// Save website banner data
		String fileName = null;
		try {

			List<String> nameList = fileUtility.saveImage(file, request
					.getSession().getServletContext().getRealPath("/"));
			for (String fname : nameList) {
				fileName = fname;
			}
			banner.setBannerImage(fileName);
			websiteDao.addBanner(banner);
			List<Banner> bannerList = websiteDao.getBanners();
			model.addObject("bannerList", bannerList);
			model.addObject("status", "Banner images added successfully");
			model.setViewName("bannerimages");
		} catch (Exception e) {
			e.printStackTrace();
			model.addObject("errorstatus", "Error while adding banner images");
		}
		return model;
	}

	@RequestMapping(value = "/websitelogout", method = RequestMethod.GET)
	public ModelAndView websiteLogout(@ModelAttribute MobileUser mUser,
			HttpServletRequest request, ModelAndView model) {

		// User logout from website
		HttpSession session = request.getSession();
		model.setViewName("success");
		try {
			session.removeAttribute("wUser");
			session.removeAttribute("wUserId");
			session.removeAttribute("productList");
			session.invalidate();
			model.addObject("status", "You logged out successful");

		} catch (Exception e) {
			e.printStackTrace();
			model.addObject("errorstatus", "Something went wrong");
		}
		return model;
	}

	@RequestMapping(value = "/prepviousorders", method = RequestMethod.GET)
	public ModelAndView previousOrders(@ModelAttribute MobileUser mUser,
			HttpServletRequest request, ModelAndView model) {

		// User logout from website
		HttpSession session = request.getSession();
		model.setViewName("wUserOrders");
		try {
			Long userId = (Long) session.getAttribute("wUserId");

			List<Orders> orders = orderDao.getUserOrders(userId);
			model.addObject("orderList", orders);
			return model;

		} catch (Exception e) {
			e.printStackTrace();
			model.addObject("errorstatus", "Something went wrong");
			return model;
		}

	}
}
