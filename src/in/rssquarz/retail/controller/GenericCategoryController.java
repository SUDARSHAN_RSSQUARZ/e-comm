package in.rssquarz.retail.controller;

import java.util.List;
import java.util.Map;



import org.codehaus.jackson.map.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import in.rssquarz.retail.dao.GenericCategoryDao;
import in.rssquarz.retail.model.GenericCategory;

/**
 * Class to handle all category URLs.
 * 
 * @author Rssquarz
 *
 */
@Controller
public class GenericCategoryController {

	@Autowired
	private GenericCategoryDao categoryDao;
	
	/**
	 * Return existing category list and category jsp page.
	 * 
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/prepareCategory", method = RequestMethod.GET)
	private ModelAndView prepareCategory(ModelAndView model) {
		ObjectMapper mapper = new ObjectMapper();
		try{
			Map<String, List<String>> categoryMap = categoryDao.retrieveCategoryList();
			model.addObject("categoryMap",categoryMap);
			model.addObject("categoryMapString",mapper.writeValueAsString(categoryMap));
		}catch(Exception e){
			e.printStackTrace();
		}
		model.setViewName("genericCategory");
		return model;
	}
	
	/**
	 * Add category in db.
	 * 
	 * @param category
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/addCategory", method = RequestMethod.POST)
	private ModelAndView addCategory(@ModelAttribute GenericCategory category, ModelAndView model) {
		ObjectMapper mapper = new ObjectMapper();
		try{
			category.setParent("/");
			category.setCategoryPath("/"+category.getName());
			categoryDao.saveCategory(category);
			Map<String, List<String>> categoryMap = categoryDao.retrieveCategoryList();
			model.addObject("categoryMap",categoryMap);
			model.addObject("categoryMapString",mapper.writeValueAsString(categoryMap));
			model.addObject("status","Category added successfully.");
		}catch(Exception e){
			e.printStackTrace();
			model.addObject("errorstatus","Error while adding category.");
		}
		model.setViewName("genericCategory");
		return model;
	}
	
	/**
	 * Add sub-category into existing category.
	 * 
	 * @param category
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/addSubCategory", method = RequestMethod.POST)
	private ModelAndView addSubCategory(@ModelAttribute GenericCategory category, ModelAndView model) {
		ObjectMapper mapper = new ObjectMapper();
		try{
			category.setCategoryPath(category.getParent()+"/"+category.getName());
			categoryDao.saveCategory(category);
			Map<String, List<String>> categoryMap = categoryDao.retrieveCategoryList();
			model.addObject("categoryMap",categoryMap);
			model.addObject("categoryMapString",mapper.writeValueAsString(categoryMap));
			model.addObject("status","Category added successfully.");
		}catch(Exception e){
			e.printStackTrace();
			model.addObject("errorstatus","Error while adding category.");
		}
		model.setViewName("genericCategory");
		return model;
	}
}
