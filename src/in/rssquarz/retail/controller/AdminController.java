package in.rssquarz.retail.controller;

import in.rssquarz.retail.dao.OrderDao;
import in.rssquarz.retail.dao.UserDao;
import in.rssquarz.retail.model.UserDetails;
import in.rssquarz.retail.service.CategoryService;
import in.rssquarz.retail.utiliy.MD5Hash;
import in.rssquarz.retail.utiliy.MailUtility;
import in.rssquarz.retail.utiliy.Utils;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.codehaus.jackson.map.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class AdminController {

	@Autowired
	private CategoryService categoryService;

	@Autowired
	private UserDao userDao;

	@Autowired
	private MailUtility mailUtility;

	@Autowired
	private OrderDao orderDao;

	@RequestMapping(value = "/admin", method = RequestMethod.GET)
	public ModelAndView adminForm(ModelAndView model) {
		return new ModelAndView("login", "command", new UserDetails());
	}

	@RequestMapping(value = "/home", method = RequestMethod.GET)
	public ModelAndView homeForm(ModelAndView model) {
		model.setViewName("ecommDashboard");
		return model;
	}

	@RequestMapping(value = "/logout", method = RequestMethod.GET)
	public ModelAndView logOut(ModelAndView model, HttpServletRequest request) {
		HttpSession session = request.getSession();
		session.invalidate();
		return new ModelAndView("login", "command", new UserDetails());
	}

	@RequestMapping(value = "/login", method = RequestMethod.POST)
	public ModelAndView adminSubmit(@ModelAttribute UserDetails user,
			HttpServletRequest request,
			ModelAndView model) {
		// Verify pwd and sent user type
		HttpSession session = request.getSession();
		model.addObject("greeting", user);
		user.setPwd(MD5Hash.getHash(user.getPwd()));
		try {
			UserDetails u = userDao.userLogin(user);
			
			if (u != null) {
				session.setAttribute("user", u.getUserName());
				session.setAttribute("userType", u.getType());
				session.setAttribute("userId", u.getType());
				model.addObject("orderList", orderDao.orders());
				model.setViewName("ecommDashboard");
			} else {
				return new ModelAndView("login", "command", new UserDetails())
						.addObject("errorstatus",
								"Username or password is incorrect");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return model;
	}

	@RequestMapping(value = "/product", method = RequestMethod.GET)
	public ModelAndView addProductForm(ModelAndView model) {
		ObjectMapper mapper = new ObjectMapper();
		try {
			model.addObject("topNavString",
					mapper.writeValueAsString(categoryService.getTopNav()));
			model.addObject("topNav", categoryService.getTopNav());
			System.out.println(mapper.writeValueAsString(categoryService
					.getTopNav()));
		} catch (IOException e) {
			e.printStackTrace();
		}
		model.setViewName("dashboard");
		return model;
	}

	@RequestMapping(value = "/forgotPwd", method = RequestMethod.POST)
	public ModelAndView forgotPassword(@ModelAttribute UserDetails user,
			HttpServletRequest request, ModelAndView model) {
		// Verify pwd and sent user type
		try {
			UserDetails u = userDao.checkUser(user);

			if (u != null) {
				String pwd = Utils.getAlphaNumPass(4, 3);
				u.setPwd(MD5Hash.getHash(pwd));
				userDao.changePwd(u);
				String msg = "Dear " + u.getUserName() + ",\n"
						+ "Here is your new password : \n" + "Password : "
						+ pwd;
				mailUtility.sendMail(u.getEmail(), msg, "New password");
				return new ModelAndView("login", "command", new UserDetails())
						.addObject("status",
								"Please check your mail for new credentials");
			} else {
				return new ModelAndView("login", "command", new UserDetails())
						.addObject("errorstatus", "Enter valid credentials");
			}
		} catch (Exception e) {
			e.printStackTrace();
			return new ModelAndView("login", "command", new UserDetails())
					.addObject("errorstatus", "Something went wrong");
		}
	}

}
