package in.rssquarz.retail.controller;

import in.rssquarz.retail.bo.NotificationBO;
import in.rssquarz.retail.dao.CartDao;
import in.rssquarz.retail.dao.NotificationDao;
import in.rssquarz.retail.model.MobileUser;
import in.rssquarz.retail.model.Notification;
import in.rssquarz.retail.utiliy.FileUtility;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class NotificationController {

	@Autowired
	private CartDao cartDao;

	@Autowired
	private Environment environment;

	@Autowired
	private FileUtility fileUtility;

	@Autowired
	private NotificationDao notificationDao;

	@RequestMapping(value = "/notification", method = RequestMethod.GET)
	public ModelAndView prepareNotification(ModelAndView model) {

		try {
			model.addObject("mobileUserList", cartDao.mUserList());
			model.addObject("notificationList",
					notificationDao.getAllNotifications());
			model.setViewName("notification");
		} catch (Exception e) {
			e.printStackTrace();
		}
		return model;
	}

	@RequestMapping(value = "/notification", method = RequestMethod.POST)
	public ModelAndView sendNotification(
			@RequestParam(value = "deviceId", required = false) String[] ids,
			@ModelAttribute Notification notification,
			@RequestParam(value = "sendToAll", required = false) boolean sendToAll,
			@RequestParam(value = "file", required = false) MultipartFile[] files,
			HttpServletRequest request,
			ModelAndView model) {

		try {

			model.setViewName("notification");
			model.addObject("mobileUserList", cartDao.mUserList());
			model.addObject("notificationList",
					notificationDao.getAllNotifications());
			String fileName = null;
			String notificationText = notification.getNotificationText();
			MobileUser m = null;
			List<MobileUser> mUserList = new ArrayList<MobileUser>();
			String key = environment.getRequiredProperty("gcm.server.key");
			String apnsFileName = environment
					.getRequiredProperty("apns.file.name");
			String apnsFilePwd = environment
					.getRequiredProperty("apns.file.pwd");
			if (files.length != 0) {
				List<String> nameList = fileUtility.saveImage(files, request
						.getSession().getServletContext().getRealPath("/"));
				for (String fname : nameList) {
					fileName = fname;
				}
				notificationText = notificationText + ",image : " + fileName;
			}

			notification.setImage(fileName);
			notificationDao.saveNotification(notification);

			if (sendToAll) {
				mUserList = cartDao.mUserList();
				NotificationBO notificationBO = new NotificationBO();
				notificationBO.sendNotification(mUserList, notificationText,
						key, apnsFileName, apnsFilePwd);
			} else {
				for (int i = 0; i < ids.length; i++) {
					m = new MobileUser();
					m = cartDao.findUserByDeviceId(ids[i].toString());
					mUserList.add(m);
				}
				NotificationBO notificationBO = new NotificationBO();
				notificationBO.sendNotification(mUserList, notificationText,
						key, apnsFileName, apnsFilePwd);
			}
			model.addObject("status", "Notification sent successfully");
		} catch (Exception e) {
			e.printStackTrace();
			model.addObject("errorstatus", "Could not send notification");
		}
		return model;
	}

}
