package in.rssquarz.retail.service;

import in.rssquarz.retail.dao.CartDao;
import in.rssquarz.retail.dao.ProductDao;
import in.rssquarz.retail.model.Cart;
import in.rssquarz.retail.model.MobileUser;
import in.rssquarz.retail.model.Product;
import in.rssquarz.retail.model.ProductAttributes;

import java.math.BigInteger;
import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service("cartService")
@Transactional
public class CartServiceImpl implements CartService {

	@Autowired
	private CartDao cartdao;

	@Autowired
	private ProductDao productdao;

	@Override
	public boolean addToCart(Long productId, String token) {
		try {
			MobileUser mUser = cartdao.findUserByToken(token);
			if (mUser != null) {
				Cart itemInCart = new Cart();
				itemInCart.setmUser(mUser);
				itemInCart.setProductId(productId);
				cartdao.saveItemInCart(itemInCart);
				return true;
			} else {
				return false;
			}
		} catch (Exception e) {
			e.getStackTrace();
			return false;
		}
	}

	@Override
	public String removeFromCart(Long productId, String token) {
		try {
			MobileUser mUser = cartdao.findUserByToken(token);
			Product p = null;
			for (Cart item : mUser.getCart()) {
				if (productId.equals(item.getProductId())) {
					cartdao.removeFromCart(item);
					p = productdao.findProductsByAttrId(productId);
					break;
				}
			}
			return p != null ? p.getName() : null;
		} catch (Exception e) {
			e.getStackTrace();
			return null;
		}
	}

	@Override
	public List<Product> getCart(String token) {
		try {
			MobileUser mUser = cartdao.findUserByToken(token);
			List<Product> itemsInCart = new ArrayList<Product>();
			for (Cart item : mUser.getCart()) {
				/*
				 * Product p = productdao
				 * .findProductsByAttrId(item.getProductId());
				 * 
				 * formProdInCart(itemsInCart, item, p);
				 */
				Product p = productdao.findProductsById(item.getProductId());
				itemsInCart.add(p);
			}
			return itemsInCart;
		} catch (Exception e) {
			e.getStackTrace();
			return null;
		}
	}

	private void formProdInCart(List<Product> itemsInCart, Cart item, Product p) {
		List<ProductAttributes> remove = new ArrayList<ProductAttributes>();
		Product pDetach = new Product();
		pDetach.setAttributes(new ArrayList<ProductAttributes>(p
				.getAttributes()));
		pDetach.setCategory(p.getCategory());
		pDetach.setName(p.getName());
		pDetach.setSubCategory(p.getSubCategory());
		pDetach.setId(p.getId());
		pDetach.setImages(p.getImages());
		pDetach.setOrder(p.getOrder());
		for (ProductAttributes pa : pDetach.getAttributes()) {
			if (pa.getId() != item.getProductId()) {
				remove.add(pa);
			}
		}
		pDetach.getAttributes().removeAll(remove);
		itemsInCart.add(pDetach);
	}

	@Override
	public void emptyCart(String token) {
		try {
			MobileUser mUser = cartdao.findUserByToken(token);
			for (Cart item : mUser.getCart()) {
				cartdao.removeFromCart(item);
			}
		} catch (Exception e) {
			e.getStackTrace();
		}
	}

	@Override
	public String addUser(MobileUser mUser) {
		try {
			SecureRandom random = new SecureRandom();
			String token = new BigInteger(130, random).toString(32);
			mUser.setSessionToken(token);
			cartdao.savemUser(mUser);
			return token;
		} catch (Exception e) {
			return null;
		}
	}

	@Override
	public List<Product> getUserCart(Long id) {
		try {
			MobileUser mUser = cartdao.findUserById(id);
			List<Product> itemsInCart = new ArrayList<Product>();
			for (Cart item : mUser.getCart()) {
				Product p = productdao
						.findProductsByAttrId(item.getProductId());

				formProdInCart(itemsInCart, item, p);
			}
			return itemsInCart;
		} catch (Exception e) {
			e.getStackTrace();
			return null;
		}
	}

	@Override
	public List<Product> getwUserCart(MobileUser mUser) {
		try {
			List<Product> itemsInCart = new ArrayList<Product>();
			for (Cart item : mUser.getCart()) {
				Product p = productdao.findProductsById(item.getProductId());
				// formProdInCart(itemsInCart, item, p);
				itemsInCart.add(p);
			}
			return itemsInCart;
		} catch (Exception e) {
			e.getStackTrace();
			return null;
		}
	}

	@Override
	public void removeFromWcart(Long productId, Long userId) {

		try {
			MobileUser mUser = cartdao.findUserById(userId);
			for (Cart item : mUser.getCart()) {
				if (productId.equals(item.getProductId())) {
					cartdao.removeFromCart(item);
					break;
				}
			}
		} catch (Exception e) {
			e.getStackTrace();
		}
	}

	@Override
	public void emptyCart(MobileUser mUser) {
		try {
			for (Cart item : mUser.getCart()) {
				cartdao.removeFromCart(item);
			}
		} catch (Exception e) {
			e.getStackTrace();
		}

	}

}
