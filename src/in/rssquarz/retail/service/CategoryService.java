package in.rssquarz.retail.service;

import in.rssquarz.retail.model.Category;
import in.rssquarz.retail.model.Page;
import in.rssquarz.retail.model.SubCategory;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;


public interface CategoryService {

	boolean saveCategory(Category category);

	void saveSubCategory(SubCategory subCategory);

	LinkedHashMap<String, List<String>> getTopNav();

	HashMap<String, String> getCategoryList();

	Page getPage(String category);

	boolean saveSubCategory(Category category);
}
