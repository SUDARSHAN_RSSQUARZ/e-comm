
package in.rssquarz.retail.service;

import in.rssquarz.retail.bo.ProductBO;
import in.rssquarz.retail.model.Category;
import in.rssquarz.retail.model.LogisticPartner;
import in.rssquarz.retail.model.Product;
import in.rssquarz.retail.model.ProductAttributePrice;
import in.rssquarz.retail.model.ProductAttributes;
import in.rssquarz.retail.model.ProductImage;

import java.util.HashMap;
import java.util.List;

import org.springframework.web.multipart.MultipartFile;

public interface ProductService {

	void saveProduct(Product product, List<String> nameList);

	void saveProductImage(ProductImage image);

	HashMap<String, List<Product>> findProductsByCategory(String category);

	List<Product> getAllProducts();

	List<Product> findProductsBySubCategory(String category, String subCategory);

	void updateProduct(Product product);

	HashMap<String, List<Product>> findProductsByType();

	ProductBO findProductBoById(Long id);

	Product findProductById(Long id);

	ProductAttributes getProductPriceFromAttribute(Long attributeId);

	List<Product> findFeaturedProduct();

	List<Product> productsByCategory(String category);

	void savePincodes(LogisticPartner logisticPartner, MultipartFile file);

	Object getAllOutOfStockProducts();

	List<Product> findProductsCategory(String category);

	List<Product> findProductsByName(String category);

	/*List<Product> findProductsCategory(Category category);
*/
	

	// Product productByAttrId(Long attrId);
}
