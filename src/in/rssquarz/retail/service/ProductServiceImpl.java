package in.rssquarz.retail.service;

import in.rssquarz.retail.bo.ProductBO;
import in.rssquarz.retail.dao.ProductDao;
import in.rssquarz.retail.enums.ProductImageType;
import in.rssquarz.retail.enums.ProductType;
import in.rssquarz.retail.model.Category;
import in.rssquarz.retail.model.LogisticPartner;
import in.rssquarz.retail.model.Pincodes;
import in.rssquarz.retail.model.Product;
import in.rssquarz.retail.model.ProductAttributePrice;
import in.rssquarz.retail.model.ProductAttributes;
import in.rssquarz.retail.model.ProductImage;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

@Service("productService")
@Transactional
public class ProductServiceImpl implements ProductService {

	@Autowired
	private ProductDao dao;

	public void deleteProductBySsn(String ssn) {
		dao.deleteProductBySsn(ssn);
	}

	public Product findBySsn(String ssn) {
		return dao.findBySsn(ssn);
	}

	public void updateProduct(Product product) {
		dao.updateProduct(product);
	}

	public HashMap<String, List<Product>> findProductsByCategory(String category) {

		HashMap<String, List<Product>> subCatMap = new HashMap<String, List<Product>>();
		List<Product> list = dao.findProductsByCategory(category);
		List<Product> subCatList = new LinkedList<Product>();
		for (Product p : list) {
			if (subCatMap.containsKey(p.getSubCategory())) {
				subCatList = subCatMap.get(p.getSubCategory());
			} else {
				subCatList = new LinkedList<Product>();
			}
			subCatList.add(p);
			subCatMap.put(p.getSubCategory(), subCatList);
		}
		return subCatMap;
	}
	
	public List<Product> findProductsCategory(String category) {
		List<Product> list = dao.findProductsByCategory(category);
		return list;
	}
	
	public List<Product> findProductsBySubCategory(String category,
			String subCategory) {
		return dao.findProductsBySubCategory(category, subCategory);
	}

	public HashMap<String, List<Product>> findProductsByType() {
		HashMap<String, List<Product>> indexPageProducts = new HashMap<String, List<Product>>();
		List<Product> list = dao.findProductsByType();
		/*List<Product> featuredProduct = new ArrayList<Product>();
		List<Product> newProduct = new ArrayList<Product>();
		for (Product p : list) {
			if (p.getType().equalsIgnoreCase(ProductType.FEATURED.getType())) {
				featuredProduct.add(p);
			} else {
				newProduct.add(p);
			}
		}
		indexPageProducts.put(ProductType.FEATURED.getDesc(), featuredProduct);
		indexPageProducts.put(ProductType.NEW.getDesc(), newProduct);*/
		indexPageProducts.put(ProductType.FEATURED.getDesc(),list);
		return indexPageProducts;
	}

	public void saveProductImage(ProductImage image) {
		dao.saveProductImage(image);

	}

	@Override
	public ProductBO findProductBoById(Long id) {
		return new ProductBO(dao.findProductsById(id));
	}

	@Override
	public Product findProductById(Long id) {
		return dao.findProductsById(id);
	}

	@Override
	public void saveProduct(Product product, List<String> nameList) {
		dao.saveProduct(product);
		for (ProductAttributes pa : product.getAttributes()) {
			pa.setProduct(product);
			dao.saveProductAttribute(pa);
		}
		ProductImage pi = new ProductImage();
		pi.setImageType(ProductImageType.DEFAULT.getType());
		for (String name : nameList) {
			pi.setProductImage(name);
			pi.setProduct(product);
			dao.saveProductImage(pi);
			pi = new ProductImage();
		}

		for (ProductAttributePrice pap : product.getProductPrice()) {
			pap.setProduct(product);
			dao.saveProductAttributePrice(pap);
		}
		
		

	}

	@Override
	public List<Product> getAllProducts() {
		return dao.getAllProducts();
	}
	

	@Override
	public List<ProductAttributePrice> getAllOutOfStockProducts() {
		return dao.getAllOutOfStockProducts();
	}

	@Override
	public ProductAttributes getProductPriceFromAttribute(Long attributeId) {
		return dao.getProductPriceFromAttribute(attributeId);
	}

	@Override
	public List<Product> findFeaturedProduct() {
		List<Product> list = dao.findFeaturedProduct();
		return list;
	}

	@Override
	public List<Product> productsByCategory(String category) {
		return dao.findProductsByCategory(category);
	}

	@Override
	public void savePincodes(LogisticPartner logisticPartner, MultipartFile file) {

		try {
			dao.saveLogPartner(logisticPartner);
			byte[] byteArr = file.getBytes();
			InputStream fis = new ByteArrayInputStream(byteArr);

			org.apache.poi.ss.usermodel.Workbook book = WorkbookFactory
					.create(fis);
			org.apache.poi.ss.usermodel.Sheet sheet = book.getSheetAt(0);
			Iterator<org.apache.poi.ss.usermodel.Row> itr = sheet.iterator();
			int i = 0;
			// Reading rows from XL sheet
			while (itr.hasNext()) {
				Row row = itr.next();
				Cell c = null;

				try {
					if (i == 0) {
						i++;
						continue;
					}

					Pincodes pincode = new Pincodes();

					// pincode
					c = row.getCell(1);
					pincode.setPincode(getCellString(c));

					// city
					c = row.getCell(2);
					pincode.setCity(getCellString(c));

					// State
					c = row.getCell(3);
					pincode.setState(getCellString(c));

					// Delivery Days
					c = row.getCell(4);
					int days = Integer.parseInt(getCellString(c));
					pincode.setDeliveryDays(days);

					// CashOnDelivery
					c = row.getCell(5);
					if (getCellString(c).equalsIgnoreCase("true"))
						pincode.setCashOnDelivery(true);
					else
						pincode.setCashOnDelivery(false);

					pincode.setLogisticPartner(logisticPartner);
					dao.savePincodes(pincode);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
			fis.close();
		} catch (InvalidFormatException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private String getCellString(Cell cell) {
		String s = "";
		long l = 0;
		switch (cell.getCellType()) {
		case Cell.CELL_TYPE_STRING:
			s = cell.getStringCellValue();
			break;
		case Cell.CELL_TYPE_NUMERIC:
			l = (long) cell.getNumericCellValue();
			s = l + "";
			break;
		case Cell.CELL_TYPE_BOOLEAN:
			s = cell.getBooleanCellValue() + "";
			break;
		default:

		}
		System.out.println("here cell s::before ::" + s);
		return s;
	}

	@Override
	public List<Product> findProductsByName(String name) {
		List<Product> list = dao.findProductsByName(name);
		return list;
	}

	/*@Override
	public List<Product> findProductsCategory(Category category) {
		List<Product> list = dao.findProductsByCategory(category);
		return list;
	}
*/
	
}