package in.rssquarz.retail.service;

import in.rssquarz.retail.model.MobileUser;
import in.rssquarz.retail.model.Product;

import java.util.List;

public interface CartService {

	public boolean addToCart(Long productId, String token);

	public String removeFromCart(Long itemId, String token);

	public List<Product> getCart(String token);

	public List<Product> getwUserCart(MobileUser mUser);

	public void emptyCart(String token);

	public String addUser(MobileUser mUser);

	public List<Product> getUserCart(Long id);

	public void removeFromWcart(Long productId, Long userId);

	public void emptyCart(MobileUser mUser);

}
