package in.rssquarz.retail.service;

import in.rssquarz.retail.dao.CategoryDao;
import in.rssquarz.retail.model.Category;
import in.rssquarz.retail.model.Page;
import in.rssquarz.retail.model.SubCategory;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service("categoryService")
@Transactional
public class CategoryServiceImpl implements CategoryService {

	@Autowired
	private CategoryDao categoryDao;

	public LinkedHashMap<String, List<String>> getTopNav() {
		return categoryDao.getTopNav();
	}

	public boolean saveCategory(Category category) {
		Category c = null;
		boolean status = false;
		status = categoryDao.checkUniquenessOfCategory(category);
		if (status == true) {
			if (category.getOrder() == null) {
				c = categoryDao.findCategoryByName(category.getCategoryName());
			} else {
				c = new Category();
				c.setCategoryName(category.getCategoryName());
				c.setDisplayText(category.getDisplayText());
				c.setOrder(category.getOrder());
				categoryDao.saveCategory(c);
			}
			for (SubCategory sc : category.getSubCategories()) {
				sc.setCategory(c);
				saveSubCategory(sc);
			}
			return true;
		} else {
			return false;
		}

	}

	@Override
	public boolean saveSubCategory(Category category) {
		Category c = null;
		boolean status = false;
		for (SubCategory sc : category.getSubCategories()) {
			c = new Category();
			if (category.getOrder() == null) {
				c = categoryDao.findCategoryByName(category.getCategoryName());
				sc.setCategory(c);
			}
			status = categoryDao.checkUniquenessOfSubCategory(sc);
			if (status == true) {
				sc.setCategory(c);
				saveSubCategory(sc);
				status = true;
			} else
				return status;
		}
		return status;
	}


	public void saveSubCategory(SubCategory subCategory) {
		categoryDao.saveSubCategory(subCategory);
	}

	@Override
	public HashMap<String, String> getCategoryList() {
		return categoryDao.getCategoryList();
	}

	@Override
	public Page getPage(String category) {
		return categoryDao.getPage(category);
	}

}
