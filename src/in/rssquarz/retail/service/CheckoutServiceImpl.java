package in.rssquarz.retail.service;

import in.rssquarz.retail.dao.OrderDao;
import in.rssquarz.retail.model.Orders;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service("checkoutService")
@Transactional
public class CheckoutServiceImpl implements CheckoutService{

	@Autowired
	private OrderDao orderDao;
	
	@Override
	public String saveOrder(Orders order) {
		return orderDao.saveOrder(order);
	}

}
