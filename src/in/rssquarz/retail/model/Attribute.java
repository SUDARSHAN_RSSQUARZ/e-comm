package in.rssquarz.retail.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.codehaus.jackson.annotate.JsonManagedReference;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

@Entity
@Table(name = "ATTRIBUTE")
public class Attribute implements Serializable {

	private static final long serialVersionUID = 6829070714128082962L;

	@Id
	@Column(name = "ATTRIBUTE_ID")
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;

	@Column(name = "ATTRIBUTE_NAME", nullable = false, length = 20, unique = true)
	private String attributeName;
	
	@Transient
	private boolean checkStatus;
	
	@Transient
	@LazyCollection(LazyCollectionOption.FALSE)
	@JsonManagedReference
	private Collection<Attribute> selectedAttributes = new ArrayList<Attribute>();

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getAttributeName() {
		return attributeName;
	}

	public void setAttributeName(String attributeName) {
		this.attributeName = attributeName;
	}

	public boolean isCheckStatus() {
		return checkStatus;
	}

	public void setCheckStatus(boolean checkStatus) {
		this.checkStatus = checkStatus;
	}

	public Collection<Attribute> getSelectedAttributes() {
		return selectedAttributes;
	}

	public void setSelectedAttributes(Collection<Attribute> selectedAttributes) {
		this.selectedAttributes = selectedAttributes;
	}

	
}
