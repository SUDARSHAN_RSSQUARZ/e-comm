package in.rssquarz.retail.model;

import java.io.Serializable;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

/*@Entity
 @Table(name = "PRODUCT_AVAILABILITY")*/
public class ProductAvailability implements Serializable {
	private static final long serialVersionUID = 7449264635180797762L;

	/*@Id
	@Column(name = "PRODUCT_AVAIL_ID", unique = true, nullable = false)
	@GeneratedValue(strategy = GenerationType.AUTO)*/
	private Long id;

	/*@ManyToOne(targetEntity = Product.class)
	@JoinColumn(name = "PRODUCT_ID", nullable = false)*/
	private Product product;

	/*@Column(name = "QUANTITY")*/
	private Integer productQuantity = 0;

	/*@Temporal(TemporalType.DATE)
	@Column(name = "DATE_AVAILABLE")*/
	private Date productDateAvailable;

	/*@Column(name = "REGION")*/
	private String region;

	/*@Column(name = "STATUS")*/
	private boolean productStatus = true;

	/*@Column(name = "FREE_SHIPPING")*/
	private boolean productIsAlwaysFreeShipping;

	/*@Column(name = "QUANTITY_ORD_MIN")*/
	private Integer productQuantityOrderMin = 0;

	/*@Column(name = "QUANTITY_ORD_MAX")*/
	private Integer productQuantityOrderMax = 0;

	/*@OneToMany(fetch = FetchType.LAZY, mappedBy = "productAvailability", cascade = CascadeType.REMOVE)
	*/private Set<ProductPrice> prices = new HashSet<ProductPrice>();

	public ProductAvailability() {
	}

	public Integer getProductQuantity() {
		return productQuantity;
	}

	public void setProductQuantity(Integer productQuantity) {
		this.productQuantity = productQuantity;
	}

	public String getRegion() {
		return region;
	}

	public void setRegion(String region) {
		this.region = region;
	}

	public boolean getProductStatus() {
		return productStatus;
	}

	public void setProductStatus(boolean productStatus) {
		this.productStatus = productStatus;
	}

	public boolean getProductIsAlwaysFreeShipping() {
		return productIsAlwaysFreeShipping;
	}

	public void setProductIsAlwaysFreeShipping(
			boolean productIsAlwaysFreeShipping) {
		this.productIsAlwaysFreeShipping = productIsAlwaysFreeShipping;
	}

	public Integer getProductQuantityOrderMin() {
		return productQuantityOrderMin;
	}

	public void setProductQuantityOrderMin(Integer productQuantityOrderMin) {
		this.productQuantityOrderMin = productQuantityOrderMin;
	}

	public Integer getProductQuantityOrderMax() {
		return productQuantityOrderMax;
	}

	public void setProductQuantityOrderMax(Integer productQuantityOrderMax) {
		this.productQuantityOrderMax = productQuantityOrderMax;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Product getProduct() {
		return product;
	}

	public void setProduct(Product product) {
		this.product = product;
	}

	public Set<ProductPrice> getPrices() {
		return prices;
	}

	public void setPrices(Set<ProductPrice> prices) {
		this.prices = prices;
	}

}
