package in.rssquarz.retail.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "BANNER")
public class Banner implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1319963689969668718L;

	@Id
	@Column(name = "BANNER_ID")
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int id;

	@Column(name = "BANNER_TEXT", length = 50)
	private String bannerText;

	@Column(name = "BANNER_IMAGE")
	private String bannerImage;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getBannerText() {
		return bannerText;
	}

	public void setBannerText(String bannerText) {
		this.bannerText = bannerText;
	}

	public String getBannerImage() {
		return bannerImage;
	}

	public void setBannerImage(String bannerImage) {
		this.bannerImage = bannerImage;
	}

	@Override
	public String toString() {
		return "Banner [id=" + id + ", bannerText=" + bannerText
				+ ", bannerImage=" + bannerImage + "]";
	}

}
