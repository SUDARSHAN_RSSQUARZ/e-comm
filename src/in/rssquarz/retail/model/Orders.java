package in.rssquarz.retail.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.codehaus.jackson.annotate.JsonBackReference;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

@Entity
@Table(name = "ORDERS")
public class Orders implements Serializable {

	private static final long serialVersionUID = 6680777090031093803L;

	@Id
	@Column(name = "ORDER_ID")
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;

	@LazyCollection(LazyCollectionOption.FALSE)
	@OneToMany(mappedBy = "order")
	private Collection<OrderItems> items = new ArrayList<OrderItems>();

	@Column(name = "USER_NAME", length = 20)
	private String nameUser;

	@Column(name = "PHONE", length = 10, nullable = false)
	private String phNo;

	@Column(name = "EMAIL", length = 30)
	private String email;

	@Column(name = "ADDRESS", length = 50)
	private String address;

	@Column(name = "PIN", length = 10)
	private String pin;

	@Column(name = "PRICE", nullable = false)
	private BigDecimal price;
	
	@Column(name = "STATUS", columnDefinition = "boolean default false", nullable = false)
	private boolean status;

	@Temporal(TemporalType.DATE)
	@Column(name = "ORDER_DATE", nullable = false)
	private Date orderDate = new Date();

	@ManyToOne
	@JoinColumn(name = "MUSER_ID")
	@JsonBackReference
	private MobileUser mUser;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Collection<OrderItems> getItems() {
		return items;
	}

	public void setItems(Collection<OrderItems> items) {
		this.items = items;
	}

	public String getPhNo() {
		return phNo;
	}

	public void setPhNo(String phNo) {
		this.phNo = phNo;
	}

	public String getNameUser() {
		return nameUser;
	}

	public void setNameUser(String nameUser) {
		this.nameUser = nameUser;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getPin() {
		return pin;
	}

	public void setPin(String pin) {
		this.pin = pin;
	}

	public BigDecimal getPrice() {
		return price;
	}

	public void setPrice(BigDecimal price) {
		this.price = price;
	}

	public boolean isStatus() {
		return status;
	}

	public void setStatus(boolean status) {
		this.status = status;
	}

	public Date getOrderDate() {
		return orderDate;
	}

	public void setOrderDate(Date orderDate) {
		this.orderDate = orderDate;
	}

	public MobileUser getmUser() {
		return mUser;
	}

	public void setmUser(MobileUser mUser) {
		this.mUser = mUser;
	}

	@Override
	public String toString() {
		return "Order Id = " + id + "\n Name = " + nameUser
				+ "\n Phone Number = " + phNo + "\n Email = " + email
				+ "\n Address = " + address + "\n Pincode = " + pin
				+ "\n Price = " + price + "\n orderDate = " + orderDate
				+ "\n mUser = " + mUser;
	}

}
