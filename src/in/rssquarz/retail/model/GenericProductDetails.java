package in.rssquarz.retail.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name = "GENERIC_PRODUCT")
public class GenericProductDetails implements Serializable {

	private static final long serialVersionUID = 4411714214003343643L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "PRODUCT_ID")
	private Long productId;
	
	private String productTitle;
	
	private String defaultValue;
	
	private String productDescription;

	private String category;
	
	@Transient
	private Collection<GenericAttributeDetails> subAttributesDetails = new ArrayList<GenericAttributeDetails>();

	public Long getProductId() {
		return productId;
	}

	public void setProductId(Long productId) {
		this.productId = productId;
	}

	
	public String getDefaultValue() {
		return defaultValue;
	}

	public void setDefaultValue(String defaultValue) {
		this.defaultValue = defaultValue;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public String getProductTitle() {
		return productTitle;
	}

	public void setProductTitle(String productTitle) {
		this.productTitle = productTitle;
	}

	public String getProductDescription() {
		return productDescription;
	}

	public void setProductDescription(String productDescription) {
		this.productDescription = productDescription;
	}

	public Collection<GenericAttributeDetails> getSubAttributesDetails() {
		return subAttributesDetails;
	}

	public void setSubAttributesDetails(Collection<GenericAttributeDetails> subAttributesDetails) {
		this.subAttributesDetails = subAttributesDetails;
	}

	@Override
	public String toString() {
		return "GenericProductDetails [productId=" + productId + ", productTitle=" + productTitle + ", defaultValue="
				+ defaultValue + ", productDescription=" + productDescription + ", category=" + category
				+ ", subAttributesDetails=" + subAttributesDetails + "]";
	}

	
}