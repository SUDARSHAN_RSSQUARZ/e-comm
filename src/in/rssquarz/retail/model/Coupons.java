package in.rssquarz.retail.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "COUPONS")
public class Coupons implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4600772679346256762L;

	@Id
	@Column(name = "COUPON_ID")
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int id;

	@Column(name = "CODE", nullable = false, length = 20, unique = true)
	private String code;

	@Temporal(TemporalType.DATE)
	@Column(name = "FROM_DATE", nullable = false)
	private Date fromDate;

	@Temporal(TemporalType.DATE)
	@Column(name = "TO_DATE", nullable = false)
	private Date toDate;

	@Column(name = "TYPE", nullable = false, length = 20)
	private String type;

	@Column(name = "TYPE_FIGURE", nullable = false, length = 20)
	private Long typeFigure;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public Date getFromDate() {
		return fromDate;
	}

	public void setFromDate(Date fromDate) {
		this.fromDate = fromDate;
	}

	public Date getToDate() {
		return toDate;
	}

	public void setToDate(Date toDate) {
		this.toDate = toDate;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public Long getTypeFigure() {
		return typeFigure;
	}

	public void setTypeFigure(Long typeFigure) {
		this.typeFigure = typeFigure;
	}

	@Override
	public String toString() {
		return "Coupons [id=" + id + ", code=" + code + ", fromDate="
				+ fromDate + ", toDate=" + toDate + ", type=" + type
				+ ", typeFigure=" + typeFigure + "]";
	}

}
