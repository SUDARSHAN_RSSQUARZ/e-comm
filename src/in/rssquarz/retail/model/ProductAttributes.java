package in.rssquarz.retail.model;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.codehaus.jackson.annotate.JsonBackReference;

@Entity
@Table(name = "PRODUCT_ATTRIBUTES")
public class ProductAttributes implements Serializable {

	private static final long serialVersionUID = -7491995860975129061L;
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "ATTRIBUTE_ID")
	private Long id;

	@ManyToOne(targetEntity = Product.class)
	@JoinColumn(name = "PRODUCT_ID", nullable = false)
	@JsonBackReference
	private Product product;

	@Column(name = "ATTRIBUTE_TYPE", nullable = false, length = 20)
	private String type;

	@Column(name = "VALUE", nullable = false, length = 20)
	private String value;

	@Column(name = "PRICE")
	private BigDecimal price;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Product getProduct() {
		return product;
	}

	public void setProduct(Product product) {
		this.product = product;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public BigDecimal getPrice() {
		return price;
	}

	public void setPrice(BigDecimal price) {
		this.price = price;
	}

	@Override
	public String toString() {
		return "ProductAttributes [id=" + id + ", type=" + type + ", value="
				+ value + ", price=" + price + "]";
	}

}
