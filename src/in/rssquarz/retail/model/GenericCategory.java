package in.rssquarz.retail.model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "GenericCategory")
public class GenericCategory {

	@Id
	private Long id;
	private String name;
	private String parent;
	private String categoryPath;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getParent() {
		return parent;
	}
	public void setParent(String parent) {
		this.parent = parent;
	}
	public String getCategoryPath() {
		return categoryPath;
	}
	public void setCategoryPath(String categoryPath) {
		this.categoryPath = categoryPath;
	}
	@Override
	public String toString() {
		return "GenericCategory [id=" + id + ", name=" + name + ", parent=" + parent + ", categoryPath=" + categoryPath
				+ "]";
	}
	
}
