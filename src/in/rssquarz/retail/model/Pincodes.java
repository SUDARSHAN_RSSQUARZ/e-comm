package in.rssquarz.retail.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.codehaus.jackson.annotate.JsonBackReference;

@Entity
@Table(name = "PINCODES")
public class Pincodes {


	@Id
	@Column(name = "PINCODE_ID")
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;

	@Column(name = "PINCODE", nullable = false, length = 10)
	private String pincode;

	@Column(name = "CITY", nullable = false, length = 20)
	private String city;

	@Column(name = "STATE", nullable = false, length = 20)
	private String state;

	@Column(name = "DELIVERY_DAYS", nullable = false, length = 2)
	private int deliveryDays;

	@Column(name = "COD", nullable = false, columnDefinition = "boolean default false")
	private boolean cashOnDelivery;

	@ManyToOne(targetEntity = LogisticPartner.class)
	@JoinColumn(name = "PARTNER_ID", nullable = false)
	@JsonBackReference
	private LogisticPartner logisticPartner;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getPincode() {
		return pincode;
	}

	public void setPincode(String pincode) {
		this.pincode = pincode;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public int getDeliveryDays() {
		return deliveryDays;
	}

	public void setDeliveryDays(int deliveryDays) {
		this.deliveryDays = deliveryDays;
	}

	public boolean isCashOnDelivery() {
		return cashOnDelivery;
	}

	public void setCashOnDelivery(boolean cashOnDelivery) {
		this.cashOnDelivery = cashOnDelivery;
	}

	public LogisticPartner getLogisticPartner() {
		return logisticPartner;
	}

	public void setLogisticPartner(LogisticPartner logisticPartner) {
		this.logisticPartner = logisticPartner;
	}

}
