package in.rssquarz.retail.model;

import java.io.InputStream;
import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.codehaus.jackson.annotate.JsonBackReference;

@Entity
@Table(name = "PRODUCT_IMAGE")
public class ProductImage implements Serializable {
	private static final long serialVersionUID = 247514890386076337L;

	@Id
	@Column(name = "PRODUCT_IMAGE_ID")
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;

	@Column(name = "PRODUCT_IMAGE")
	private String productImage;

	/*
	 * @Column(name = "DEFAULT_IMAGE") private boolean defaultImage = true;
	 */

	@Column(name = "IMAGE_TYPE")
	private String imageType;

	@ManyToOne(targetEntity = Product.class)
	@JoinColumn(name = "PRODUCT_ID", nullable = false)
	@JsonBackReference
	private Product product;

	@Transient
	private InputStream image = null;

	// private MultiPartFile image

	public ProductImage() {
	}

	public String getProductImage() {
		return productImage;
	}

	public void setProductImage(String productImage) {
		this.productImage = productImage;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Product getProduct() {
		return product;
	}

	public void setProduct(Product product) {
		this.product = product;
	}

	public InputStream getImage() {
		return image;
	}

	public void setImage(InputStream image) {
		this.image = image;
	}

	public String getImageType() {
		return imageType;
	}

	public void setImageType(String imageType) {
		this.imageType = imageType;
	}
}
