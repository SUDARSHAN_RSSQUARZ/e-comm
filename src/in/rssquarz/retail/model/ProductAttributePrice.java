package in.rssquarz.retail.model;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.codehaus.jackson.annotate.JsonBackReference;

@Entity
@Table(name = "PRODUCT_ATTRIBUTES_PRICE")
public class ProductAttributePrice implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1449686230101097761L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "PRICE_ID")
	private Long id;

	@Column(name = "PRODUCT_WEIGHT", length = 20)
	private String weight;

	@Column(name = "PRODUCT_COLOR", length = 20)
	private String color;

	@Column(name = "PRODUCT_PRICE", nullable = false, length = 20)
	private BigDecimal price = new BigDecimal(0);
	
	@Column(name = "PRODUCT_QUANTITY", nullable = false, length = 20)
	private Long quantity = new Long(0);

	@ManyToOne(targetEntity = Product.class)
	@JoinColumn(name = "PRODUCT_ID", nullable = false)
	@JsonBackReference
	private Product product;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getWeight() {
		return weight;
	}

	public void setWeight(String weight) {
		this.weight = weight;
	}

	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}

	public Product getProduct() {
		return product;
	}

	public void setProduct(Product product) {
		this.product = product;
	}

	public BigDecimal getPrice() {
		return price;
	}

	public void setPrice(BigDecimal price) {
		this.price = price;
	}

	public Long getQuantity() {
		return quantity;
	}

	public void setQuantity(Long quantity) {
		this.quantity = quantity;
	}
}
