package in.rssquarz.retail.model;

import java.util.ArrayList;
import java.util.Collection;

public class GenericAttributeDetails {
	
	
	private String attributename;
	
	private Collection<SubAttributes> subAttributesDetails = new ArrayList<SubAttributes>();

	public String getAttributename() {
		return attributename;
	}

	public void setAttributename(String attributename) {
		this.attributename = attributename;
	}

	public Collection<SubAttributes> getSubAttributesDetails() {
		return subAttributesDetails;
	}

	public void setSubAttributesDetails(Collection<SubAttributes> subAttributesDetails) {
		this.subAttributesDetails = subAttributesDetails;
	}

	@Override
	public String toString() {
		return "GenericAttributeDetails [attributename=" + attributename + ", subAttributesDetails="
				+ subAttributesDetails + "]";
	}
	

}
