package in.rssquarz.retail.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "ORDER_ITEMS")
public class OrderItems {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "ITEM_ID")
	private Long id;

	@ManyToOne(targetEntity = Orders.class)
	@JoinColumn(name = "ORDER_ID", nullable = false)
	private Orders order;

	@Column(name = "PRODUCT_ID", nullable = false)
	private Long productId;

	/*
	 * @OneToOne(targetEntity = ProductAttributes.class)
	 * 
	 * @JoinColumn(name="ATTRIBUTE_ID", nullable = true) private
	 * ProductAttributes attribute;
	 */

	/*
	 * @LazyCollection(LazyCollectionOption.FALSE)
	 * 
	 * @OneToMany(mappedBy = "product") private Collection<ProductAttributes>
	 * attributes = new ArrayList<ProductAttributes>();
	 */
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Orders getOrder() {
		return order;
	}

	public void setOrder(Orders order) {
		this.order = order;
	}

	public Long getProductId() {
		return productId;
	}

	public void setProductId(Long productId) {
		this.productId = productId;
	}
}