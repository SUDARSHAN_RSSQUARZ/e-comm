package in.rssquarz.retail.model;

import in.rssquarz.retail.enums.ProductImageType;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.codehaus.jackson.annotate.JsonManagedReference;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

@Entity
@Table(name = "PRODUCT")
public class Product implements Serializable {

	private static final long serialVersionUID = 8384996568296521160L;

	@Id
	@Column(name = "PRODUCT_ID")
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;

	@Column(name = "PRODUCT_NAME", nullable = false)
	private String name;
	
	@Column(name = "CATEGORY", nullable = false)
	private String category;

	@Column(name = "SUB_CATEGORY", nullable = false)
	private String subCategory;
	
	@Column(name = "DEFAULT_PRICE")
	private BigDecimal defaultPrice;

	@Column(name = "TYPE")
	private String type;

	@Transient
	private String defaultImage;

	@Column(name = "ORDER_DISPLAY")
	private Long orderDisplay;

	/*
	 * @OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.REMOVE, mappedBy
	 * = "product") private Set<ProductAvailability> availabilities = new
	 * HashSet<ProductAvailability>();
	 * 
	 * @OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.REMOVE, mappedBy
	 * = "product") private Set<ProductIngrediants> ingrediants = new
	 * HashSet<ProductIngrediants>();
	 */
	@LazyCollection(LazyCollectionOption.FALSE)
	@OneToMany(mappedBy = "product")
	@JsonManagedReference
	private Collection<ProductAttributes> attributes = new ArrayList<ProductAttributes>();

	

	@LazyCollection(LazyCollectionOption.FALSE)
	@OneToMany(mappedBy = "product")
	@JsonManagedReference
	private Collection<ProductImage> images = new ArrayList<ProductImage>();

	@LazyCollection(LazyCollectionOption.FALSE)
	@OneToMany(mappedBy = "product")
	@JsonManagedReference
	private Collection<ProductAttributePrice> productPrice = new ArrayList<ProductAttributePrice>();
	
	@LazyCollection(LazyCollectionOption.FALSE)
	@OneToMany(mappedBy = "product")
	@JsonManagedReference
	private Collection<ProductAttributePrice> productPrice1 = new ArrayList<ProductAttributePrice>();
	

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public BigDecimal getDefaultPrice() {
		return defaultPrice;
	}

	public void setDefaultPrice(BigDecimal defaultPrice) {
		this.defaultPrice = defaultPrice;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public String getSubCategory() {
		return subCategory;
	}

	public void setSubCategory(String subCategory) {
		this.subCategory = subCategory;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public Collection<ProductAttributes> getAttributes() {
		return attributes;
	}

	public void setAttributes(Collection<ProductAttributes> attributes) {
		this.attributes = attributes;
	}

	public Collection<ProductImage> getImages() {
		return images;
	}

	public void setImages(Collection<ProductImage> images) {
		this.images = images;
	}

	public String getDefaultImage() {
		return defaultImage;
	}

	public void setDefaultImage() {
		for (ProductImage image : images) {
			if (image.getImageType().equalsIgnoreCase(
					ProductImageType.DEFAULT.getType())) {
				defaultImage = image.getProductImage();
			}
		}

	}

	public Collection<ProductAttributePrice> getProductPrice() {
		return productPrice;
	}

	public void setProductPrice(Collection<ProductAttributePrice> productPrice) {
		this.productPrice = productPrice;
	}

	@Override
	public String toString() {
		return "Product [id=" + id + ", name=" + name + ", attributes="
				+ attributes + ", category=" + category + ", subCategory="
				+ subCategory + ", images=" + images + ", defaultPrice="
				+ defaultPrice + ", type=" + type + ", defaultImage="
				+ defaultImage + "]";
	}

	public Long getOrder() {
		return orderDisplay;
	}

	public void setOrder(Long order) {
		this.orderDisplay = order;
	}

	public Collection<ProductAttributePrice> getProductPrice1() {
		return productPrice1;
	}

	public void setProductPrice1(Collection<ProductAttributePrice> productPrice1) {
		this.productPrice1 = productPrice1;
	}
	
}
