package in.rssquarz.retail.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.codehaus.jackson.annotate.JsonManagedReference;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

@Entity
@Table(name = "LOGISTIC_PARTNER")
public class LogisticPartner implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2457498056805828843L;

	@Id
	@Column(name = "PARTNER_ID")
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;

	@Column(name = "PARTNER_NAME", nullable = false, length = 30)
	private String partnerName;

	@LazyCollection(LazyCollectionOption.FALSE)
	@OneToMany(mappedBy = "logisticPartner")
	@JsonManagedReference
	private Collection<Pincodes> pincodes = new ArrayList<Pincodes>();

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getPartnerName() {
		return partnerName;
	}

	public void setPartnerName(String partnerName) {
		this.partnerName = partnerName;
	}

	public Collection<Pincodes> getPincodes() {
		return pincodes;
	}

	public void setPincodes(Collection<Pincodes> pincodes) {
		this.pincodes = pincodes;
	}

	@Override
	public String toString() {
		return "LogisticPartner [id=" + id + ", partnerName=" + partnerName
				+ ", pincodes=" + pincodes + "]";
	}

}
