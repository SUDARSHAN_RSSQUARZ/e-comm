package in.rssquarz.retail.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

@Entity
@Table(name = "CATEGORY")
public class Category implements Serializable {

	private static final long serialVersionUID = 37173569529265579L;

	@Id
	@Column(name = "CATEGORY_ID")
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;

	@Column(name = "NAME", nullable = false, length = 20, unique = true)
	private String categoryName;

	@LazyCollection(LazyCollectionOption.FALSE)
	@OneToMany(mappedBy = "category")
	private Collection<SubCategory> subCategories = new ArrayList<SubCategory>();

	@Column(name = "DISPLAY_TEXT", nullable = false, length = 20)
	private String displayText;

	@Column(name = "ORDER_DISPLAY", nullable = false, unique = true)
	private Long order;

	public Category() {
		categoryName = "";
		subCategories = new ArrayList<SubCategory>();
		displayText = "";
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getCategoryName() {
		return categoryName;
	}

	public void setCategoryName(String categoryName) {
		this.categoryName = categoryName;
	}

	public Collection<SubCategory> getSubCategories() {
		return subCategories;
	}

	public void setSubCategories(Collection<SubCategory> subCategories) {
		this.subCategories = subCategories;
	}

	public String getDisplayText() {
		return displayText;
	}

	public void setDisplayText(String displayText) {
		this.displayText = displayText;
	}

	public Long getOrder() {
		return order;
	}

	public void setOrder(Long order) {
		this.order = order;
	}

	@Override
	public String toString() {
		return "Category [id=" + id + ", categoryName=" + categoryName
				+ ", subCategories=" + subCategories.toString()
				+ ", displayText="
				+ displayText + ", order=" + order + "]";
	}
}
