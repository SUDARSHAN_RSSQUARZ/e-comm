package in.rssquarz.retail.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.codehaus.jackson.annotate.JsonBackReference;

@Entity
@Table(name = "CART")
public class Cart {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;

	@Column(name = "PRODUCT_ID", nullable = false)
	private Long productId;

	@ManyToOne(targetEntity = MobileUser.class)
	@JoinColumn(name = "USER_ID", nullable = false)
	@JsonBackReference
	private MobileUser mUser;

	public Long getProductId() {
		return productId;
	}

	public void setProductId(Long productId) {
		this.productId = productId;
	}

	public MobileUser getmUser() {
		return mUser;
	}

	public void setmUser(MobileUser mUser) {
		this.mUser = mUser;
	}

}
