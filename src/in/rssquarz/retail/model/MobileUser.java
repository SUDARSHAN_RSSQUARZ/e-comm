package in.rssquarz.retail.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.codehaus.jackson.annotate.JsonManagedReference;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

@Entity
@Table(name = "MOBILE_USER")
public class MobileUser implements Serializable {

	private static final long serialVersionUID = 4411714214003343643L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "MUSER_ID")
	private Long id;

	@Column(name = "SESSION_TOKEN", length = 20, unique = true)
	private String sessionToken;

	@Column(name = "DEVICE_ID", length = 200, unique = true)
	private String deviceId;

	@Column(name = "OS_TYPE", length = 10)
	private String osType;

	@Column(name = "USER_NAME", length = 40)
	private String name;

	@Column(name = "PHONE", length = 10)
	private String phNo;

	@Column(name = "EMAIL", length = 40)
	private String email;

	@Column(name = "ADDRESS", length = 60)
	private String address;

	@Column(name = "PWD", length = 255)
	private String password;

	@Column(name = "CITY", length = 30)
	private String city;

	@Column(name = "STATE", length = 30)
	private String state;

	@Column(name = "PINCODE", length = 8)
	private String pincode;

	@Column(name = "COUNTRY", length = 30)
	private String country;

	@LazyCollection(LazyCollectionOption.FALSE)
	@OneToMany(mappedBy = "mUser")
	@JsonManagedReference
	private Collection<Cart> cart = new ArrayList<Cart>();

	@LazyCollection(LazyCollectionOption.FALSE)
	@OneToMany(mappedBy = "mUser")
	@JsonManagedReference
	private Collection<Orders> orders = new ArrayList<Orders>();

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getSessionToken() {
		return sessionToken;
	}

	public void setSessionToken(String sessionToken) {
		this.sessionToken = sessionToken;
	}

	public String getDeviceId() {
		return deviceId;
	}

	public void setDeviceId(String deviceId) {
		this.deviceId = deviceId;
	}

	public String getOsType() {
		return osType;
	}

	public void setOsType(String osType) {
		this.osType = osType;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPhNo() {
		return phNo;
	}

	public void setPhNo(String phNo) {
		this.phNo = phNo;
	}

	public Collection<Cart> getCart() {
		return cart;
	}

	public void setCart(Collection<Cart> cart) {
		this.cart = cart;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getPincode() {
		return pincode;
	}

	public void setPincode(String pincode) {
		this.pincode = pincode;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public Collection<Orders> getOrders() {
		return orders;
	}

	public void setOrders(Collection<Orders> orders) {
		this.orders = orders;
	}

}
