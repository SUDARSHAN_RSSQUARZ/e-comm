package in.rssquarz.retail.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.codehaus.jackson.annotate.JsonBackReference;

@Entity
@Table(name = "Sub_ATTRIBUTE")
public class SubAttributes {
	
	@Id
	@Column(name = "SubATTRIBUTE_ID")
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long subAttributeId;
	
	
	@Column(name = "SubATTRIBUTE_NAME", length = 20)
	private String subAttributeName;
	
	@Column(name = "SubATTRIBUTE_Default_Value",length = 20)
	private String defaultValueOfAttribute;
	
	@ManyToOne(targetEntity = Attribute.class)
	@JoinColumn(name = "ATTRIBUTE_ID", nullable = false)
	@JsonBackReference
	private Attribute id;

	
	
	public Long getSubAttributeId() {
		return subAttributeId;
	}

	public void setSubAttributeId(Long subAttributeId) {
		this.subAttributeId = subAttributeId;
	}

	
	public String getSubAttributeName() {
		return subAttributeName;
	}

	public void setSubAttributeName(String subAttributeName) {
		this.subAttributeName = subAttributeName;
	}

	public String getDefaultValueOfAttribute() {
		return defaultValueOfAttribute;
	}

	public void setDefaultValueOfAttribute(String defaultValueOfAttribute) {
		this.defaultValueOfAttribute = defaultValueOfAttribute;
	}

	public Attribute getId() {
		return id;
	}

	public void setId(Attribute id) {
		this.id = id;
	}

	@Override
	public String toString() {
		return "SubAttributes [subAttributeId=" + subAttributeId + ", subAttributeName=" + subAttributeName
				+ ", defaultValueOfAttribute=" + defaultValueOfAttribute + ", id=" + id + "]";
	}
	
	
}
