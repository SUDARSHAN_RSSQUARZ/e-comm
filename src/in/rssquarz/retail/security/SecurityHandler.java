package in.rssquarz.retail.security;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import in.rssquarz.retail.model.UserDetails;

public class SecurityHandler implements HandlerInterceptor {

	@Override
	public void afterCompletion(HttpServletRequest arg0, HttpServletResponse arg1, Object arg2, Exception arg3)
			throws Exception {
		// TODO Auto-generated method stub

	}

	@Override
	public void postHandle(HttpServletRequest arg0, HttpServletResponse arg1, Object arg2, ModelAndView arg3)
			throws Exception {
		// TODO Auto-generated method stub

	}

	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object arg2) throws Exception {
		// log.info("Interceptor: Pre-handle");
		HttpServletRequest req = (HttpServletRequest) request;
		String file = req.getServletPath();
		if (request.getRequestURI().equals("/HoneyBuzz/admin") || request.getRequestURI().equals("/HoneyBuzz/login")
				|| request.getRequestURI().equals("/HoneyBuzz/") || file.startsWith("/resources/")) {
			return true;
		} else {
			String userData = (String) request.getSession().getAttribute("user");
			if (userData == null) {
				response.sendRedirect("/HoneyBuzz/admin");
				return false;
			} else
				return true;
		}

	}

}
