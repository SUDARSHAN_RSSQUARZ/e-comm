
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<!DOCTYPE html>
<html>
<head>
<title>Checkout</title>
<jsp:include page="websiteScript.jsp"></jsp:include>
<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
	
<!-- Custom Theme files -->
<!--theme-style-->

<!--//theme-style-->
<link rel="shortcut icon" type="image/png"
	href="${pageContext.request.contextPath}/resources/images/favicon.png" />
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="keywords" content="Honey Buzz" />
<script type="application/x-javascript">
	
	
	
	
        addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){
        window.scrollTo(0,1); }
        



</script>
<!--fonts-->
<link
	href='http://fonts.googleapis.com/css?family=Lato:100,300,400,700,900'
	rel='stylesheet' type='text/css'>
<link
	href='http://fonts.googleapis.com/css?family=Roboto:400,100,300,500,700,900'
	rel='stylesheet' type='text/css'>
<!--//fonts-->
<!-- start menu -->
<link href="${pageContext.request.contextPath}/resources/css/memenu.css"
	rel="stylesheet" type="text/css" media="all" />
<script type="text/javascript"
	src="${pageContext.request.contextPath}/resources/js/memenu.js"></script>

<script
	src="${pageContext.request.contextPath}/resources/js/simpleCart.min.js">
	
</script>
<script
	src="${pageContext.request.contextPath}/resources/js/imagezoom.js"></script>
<script src="${pageContext.request.contextPath}/resources/js/cookie.js"></script>
<!--Start of Zopim Live Chat Script-->
<script type="text/javascript">
window.$zopim||(function(d,s){var z=$zopim=function(c){z._.push(c)},$=z.s=
	d.createElement(s),e=d.getElementsByTagName(s)[0];z.set=function(o){z.set.
	_.push(o)};z._=[];z.set._=[];$.async=!0;$.setAttribute("charset","utf-8");
	$.src="//v2.zopim.com/?3a0P6Plo0iOMK5TF0Eg8CqvSBOR4WnKY";z.t=+new Date;$.
	type="text/javascript";e.parentNode.insertBefore($,e)})(document,"script");
</script>
</head>
<body>

	<!--header-->
	<jsp:include page="header.jsp"></jsp:include>

	<!-- grow -->
	<div class="grow">
		<div class="container">
			<h2>Success</h2>
		</div>
	</div>

	<!-- grow -->
	<div class="container">
		<div class="check">
			<c:if test="${not empty status}">
				<div class="alert alert-success">
					<strong>Success!</strong> ${status}
				</div>
			</c:if>
			<c:if test="${not empty errorstatus}">
				<div class="alert alert-danger">
					<strong></strong> ${errorstatus}
				</div>
			</c:if>
			<div class="clearfix"></div>
		</div>
	</div>

	<!--//content-->
	<jsp:include page="footer.jsp"></jsp:include>
	<jsp:include page="modal.jsp"></jsp:include>
</body>
</html>
