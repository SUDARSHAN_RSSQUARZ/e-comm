<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<!-- Meta, title, CSS, favicons, etc. -->
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Notification</title>
<!-- Bootstrap core CSS -->
<jsp:include page="adminCssInclude.jsp"></jsp:include>
</head>
<body class="nav-md">
	<div class="container body">
		<div class="main_container">

			<jsp:include page="navigationbar.jsp"></jsp:include>

			<!-- page content -->
			<div class="right_col" role="main">

				<c:if test="${not empty errorstatus}">
					<div class="alert alert-danger">
						<strong></strong> ${errorstatus }
					</div>
				</c:if>
				<c:if test="${not empty status}">
					<div class="alert alert-success">
						<strong></strong> ${status }
					</div>
				</c:if>
				<div class="">
					<div class="page-title">
						<div class="title_left">
							<h3>Send Notifications</h3>
						</div>
						<div class="title_right">
							<div
								class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
								<div class="input-group">
									<input type="text" class="form-control"
										placeholder="Search for..."> <span
										class="input-group-btn">
										<button class="btn btn-default" type="button">Go!</button>
									</span>
								</div>
							</div>
						</div>
					</div>
					<div class="clearfix"></div>
					<div class="row">
						<div class="col-md-12 col-sm-12 col-xs-12">
							<div class="x_panel">
								<form:form method="POST" enctype="multipart/form-data"
									action="${pageContext.request.contextPath}/notification"
									id="demo-form2" class="form-horizontal form-label-left">

									<div class="form-group">
										<label class="control-label col-md-3 col-sm-3 col-xs-12">Mobile User</label>
										<div class="col-md-6 col-sm-9 col-xs-12">
											<select name="deviceId" class="form-control" multiple>
												<c:forEach var="entry" items="${mobileUserList}">
													<option value="${entry.deviceId}">${entry.name}<option>
												</c:forEach>

											</select>
										</div>
									</div>
									<div class="form-group">
										<label class="control-label col-md-3 col-sm-3 col-xs-12"
											for="last-name">Send To All </label>
										<div class="col-md-6 col-sm-6 col-xs-12">
											<div class="checkbox">
												<label> <input type="checkbox" name="sendToAll" class="flat">
												</label>
											</div>
										</div>
									</div>
									<div class="form-group">
										<label class="control-label col-md-3 col-sm-3 col-xs-12"
											for="first-name">Notification Text <span class="required">*</span>
										</label>
										<div class="col-md-6 col-sm-6 col-xs-12">
											<input type="text" id="first-name" required="required"
												placeholder="Notification Text" name="notificationText"
												class="form-control col-md-7 col-xs-12">
										</div>
									</div>
									<div class="form-group">
										<label class="control-label col-md-3 col-sm-3 col-xs-12"
											for="last-name">Notification Image <span class="required">*</span>
										</label>
										<div class="col-md-6 col-sm-6 col-xs-12">
											<input type="file" name="file" placeholder="Notification Image"
												class="form-control col-md-7 col-xs-12">
										</div>
									</div>
									<div class="form-group">
                                        <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                                            <button type="submit" class="btn btn-success">Send</button>
                                         </div>
                                  	</div>
								</form:form>
							</div>
						</div>
					</div>
					<div class="row">

                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="x_panel">
                                <div class="x_title">
                                    <h2>Sent Notifications</h2>
                                   
                                    <div class="clearfix"></div>
                                </div>
                                <div class="x_content">
                                    <table id="example" class="table table-striped responsive-utilities jambo_table">
                                        <thead>
                                            <tr class="headings">
                                                <th>
                                                    <input type="checkbox" class="flat">
                                                </th>
												<th>Id </th>
                                                <th>Notification Text </th>
                                                <th>Image </th>
                                            </tr>
                                        </thead>

                                        <tbody>
										<c:forEach var="o" items="${notificationList}" varStatus="loop">
										<!--<c:choose>
										<c:when test="${loop.index%2 == 0}">
										even
										</c:when>
										<c:otherwise>
										odd
										</c:otherwise>
										</c:choose>-->
										
                                            <tr class="<c:choose><c:when test='${loop.index%2 == 0}'>	even</c:when><c:otherwise>odd</c:otherwise></c:choose> pointer">
                                                <td class="a-center ">
                                                    <input type="checkbox" class="flat">
                                                </td>
                                                <td class=" ">${o.id}</td>
                                                <td class=" ">${o.notificationText}</td>
                                                <td class=" "><img alt="" src="${pageContext.request.contextPath}${o.image}" 
                                                					width="60px" height="60px" class="img-responsive"></td>
                                            </tr>
											</c:forEach>
                                            </tbody>

                                    </table>
                                </div>
                            </div>
                        </div>

                        <br />
                        <br />
                        <br />

                    </div>
				</div>
				<!-- /page content -->
				<!-- footer content -->
				<!-- <footer>
                  <div class="">
                     <p class="pull-right">Gentelella Alela! a Bootstrap 3 template by <a>Kimlabs</a>. |
                        <span class="lead"> <i class="fa fa-paw"></i> Gentelella Alela!</span>
                     </p>
                  </div>
                  <div class="clearfix"></div>
               </footer> -->
				<!-- /footer content -->
			</div>
		</div>
	</div>
	<div id="custom_notifications" class="custom-notifications dsp_none">
		<ul class="list-unstyled notifications clearfix"
			data-tabbed_notifications="notif-group">
		</ul>
		<div class="clearfix"></div>
		<div id="notif-group" class="tabbed_notifications"></div>
	</div>
	<jsp:include page="adminScriptInclude.jsp"></jsp:include>

	<script>
         autosize($('.resizable_textarea'));
      </script>

	<script src="${pageContext.request.contextPath}/resources/js/custom.js"></script>
	<!-- select2 -->
	<script>
	  var cntAttribute = -1;
         $(document).ready(function () {
			 
			 loadAttributes();
             $(".select2_single").select2({
                 placeholder: "Select a state",
                 allowClear: true
             });
             $(".select2_group").select2({});
             $(".select2_multiple").select2({
                 maximumSelectionLength: 4,
                 placeholder: "With Max Selection limit 4",
                 allowClear: true
             });
         
         //$("#clrbtn").click(addAttrColor);
         $("#wtbtn").click(addAttrWeight);
         });
		 
		 function loadAttributes(){
			 //addAttrColor();
			 addAttrWeight();			 
		 };
		 
		 function addAttrColor(){
			 cntAttribute++;
         $("#cakecolor").append("<div><input name='attributes["+cntAttribute+"].type' type='hidden' value='COLOR'/><input name='attributes["+cntAttribute+"].value' type='text' required='required' placeholder='Colour' class='form-control'></div><br>");
		 };
		 
		 function addAttrWeight(){
			 cntAttribute++;
         $("#cakeweight").append("<div> <input name='attributes["+cntAttribute+"].type' type='hidden' value='WEIGHT'/><input name='attributes["+cntAttribute+"].value' type='text' required='required' placeholder='Weight' class='form-control'/><div class='col-md-2 col-sm-2 col-xs-2'><input type='text' name='attributes["+cntAttribute+"].price' required='required' placeholder='Price' class='form-control'></div></div><br>");
		 };
      </script>
	<!-- /select2 -->
	<!-- input tags -->
	<script>
         function onAddTag(tag) {
             alert("Added a tag: " + tag);
         }
         
         function onRemoveTag(tag) {
             alert("Removed a tag: " + tag);
         }
         
         function onChangeTag(input, tag) {
             alert("Changed a tag: " + tag);
         }
         
         $(function () {
             $('#tags_1').tagsInput({
                 width: 'auto'
             });
         });
      </script>

	<script>
            $(document).ready(function () {
                $('input.tableflat').iCheck({
                    checkboxClass: 'icheckbox_flat-green',
                    radioClass: 'iradio_flat-green'
                });
            });

            var asInitVals = new Array();
            $(document).ready(function () {
                var oTable = $('#example').dataTable({
                    "oLanguage": {
                        "sSearch": "Search all columns:"
                    },
                    "aoColumnDefs": [
                        {
                            'bSortable': false,
                            'aTargets': [0]
                        } //disables sorting for column one
            ],
                    'iDisplayLength': 12,
                    "sPaginationType": "full_numbers",
                    "dom": 'T<"clear">lfrtip',
                    "tableTools": {
                        "sSwfPath": "<?php echo base_url('assets2/js/Datatables/tools/swf/copy_csv_xls_pdf.swf'); ?>"
                    }
                });
                $("tfoot input").keyup(function () {
                    /* Filter on the column based on the index of this element's parent <th> */
                    oTable.fnFilter(this.value, $("tfoot th").index($(this).parent()));
                });
                $("tfoot input").each(function (i) {
                    asInitVals[i] = this.value;
                });
                $("tfoot input").focus(function () {
                    if (this.className == "search_init") {
                        this.className = "";
                        this.value = "";
                    }
                });
                $("tfoot input").blur(function (i) {
                    if (this.value == "") {
                        this.className = "search_init";
                        this.value = asInitVals[$("tfoot input").index(this)];
                    }
                });
            });
        </script>
</body>
</html>