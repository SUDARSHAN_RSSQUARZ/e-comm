<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
   <head>
      <title>Honey Buzz</title>
      <jsp:include page="websiteScript.jsp"></jsp:include>
      <!--//theme-style-->
      <link rel="shortcut icon" type="resources/image/png"
         href="resources/images/favicon.png" />
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
      <script type="application/x-javascript">
         addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } 
         
         
      </script>
      <!--fonts-->
      <link
         href='http://fonts.googleapis.com/css?family=Lato:100,300,400,700,900'
         rel='stylesheet' type='text/css'>
      <link
         href='http://fonts.googleapis.com/css?family=Roboto:400,100,300,500,700,900'
         rel='stylesheet' type='text/css'>
      <!--//fonts-->
      <!-- start menu -->
      <link href="resources/css/memenu.css" rel="stylesheet" type="text/css"
         media="all" />
      <script type="text/javascript" src="resources/js/memenu.js"></script>
      
      <script src="resources/js/simpleCart.min.js"></script>
      <script src="resources/js/responsiveslides.min.js"></script>
      <script src="resources/js/cookie.js"></script>
      <script src="resources/js/jquery.backstretch.js"></script>
      <style>
         #friends {
         position: absolute;
         }
         #loader {
         z-index: 999999;
         display: block;
         position: fixed;
         top: 0;
         left: 0;
         width: 100%;
         height: 100%;
         background: url(resources/images/honeybuzz.jpg) 50% 50% no-repeat #FFFFFF;
         background-size: 250px 100px;
         }
      </style>
      <script>
         var t = 0;
         
         function moveit() {
         	t += 0.03;
         
         	var r = 150;
         	var xcenter = 650;
         	var ycenter = 260;
         	var newLeft = Math.floor(xcenter + (r * Math.cos(t)));
         	var newTop = Math.floor(ycenter + (r * Math.sin(t)));
         	$('#friends').animate({
         		top : newTop,
         		left : newLeft,
         	}, 1, function() {
         		moveit();
         	});
         }
         
         $.backstretch([ "home6.jpg", "home2.jpg", "home3.jpg", "home4.jpg",
         		"home5.jpg", "home1.jpg" ], {
         	fade : 1500,
         	duration : 3000
         });
         $(window).load(function() {
         	setTimeout(function() {
         		$("#loader").hide();
         	}, 3000);
         	moveit();
         });
      </script>
      <!--Start of Zopim Live Chat Script-->
      <script type="text/javascript">
      window.$zopim||(function(d,s){var z=$zopim=function(c){z._.push(c)},$=z.s=
    	  d.createElement(s),e=d.getElementsByTagName(s)[0];z.set=function(o){z.set.
    	  _.push(o)};z._=[];z.set._=[];$.async=!0;$.setAttribute("charset","utf-8");
    	  $.src="//v2.zopim.com/?3a0P6Plo0iOMK5TF0Eg8CqvSBOR4WnKY";z.t=+new Date;$.
    	  type="text/javascript";e.parentNode.insertBefore($,e)})(document,"script");
      </script>
      <!--End of Zopim Live Chat Script-->
   </head>
   <body>
      <div id="loader">
         <img src="${pageContext.request.contextPath}/resources/images/bee3.png" id="friends" />
      </div>
      <!--header-->
      <jsp:include page="header.jsp"></jsp:include>
      <!-- banner-page -->
      <jsp:include page="banner.jsp"></jsp:include>
      <!--content-->
      <div class="container">
         <div class="cont">
            <c:forEach var="entry" items="${indexPageProduct}">
               <div class="rowfluid content-top">
                  <h1>PRODUCTS</h1>
                  <div class="grid-in">
                     <c:forEach var="product" items="${entry.value}">
                        <div class="col-md-3 grid-top simpleCart_shelfItem">
                           <a href="product/${product.id}"
                              class="b-link-stripe b-animate-go  thickbox">
                              <img
                                 class="img-responsive"
                                 src="${pageContext.request.contextPath}/${product.images.get(0).getProductImage()}" alt="">
                              <div class="b-wrapper">
                                 <h3 class="b-animate b-from-left    b-delay03 ">
                                    <span>${product.name}</span>
                                 </h3>
                              </div>
                           </a>
                           <p>
                              <a href="product/${product.id}">${product.name}</a>
                           </p>
                           <a href="product/${product.id}" class="item_add">
                              <p class="number item_price">
                                 <i> </i>${product.defaultPrice}
                              </p>
                           </a>
                        </div>
                     </c:forEach>
                  </div>
               </div>
               <div class="clearfix"></div>
            </c:forEach>
         </div>
         <!---->
      </div>
      <!-- footer -->
      <jsp:include page="footer.jsp"></jsp:include>
      <jsp:include page="modal.jsp"></jsp:include>
   </body>
</html>