<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Product</title>
<jsp:include page="adminCssInclude.jsp"></jsp:include>
</head>
<body class="nav-md">
	<div class="container body">
		<div class="main_container">

			<jsp:include page="navigationbar.jsp"></jsp:include>

			<!-- page content -->
			<div class="right_col" role="main">
				<div class="">
					<div class="page-title">
						<div class="title_left">
							<h3>Product Elements</h3>
						</div>
						<div class="title_right">
							<div
								class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
								<div class="input-group">
									<input type="text" class="form-control"
										placeholder="Search for..."> <span
										class="input-group-btn">
										<button class="btn btn-default" type="button">Go!</button>
									</span>
								</div>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-12 col-sm-12 col-xs-12">
							<div class="x_panel">
								<div class="x_content">
									<form name="myform" id="myform" method="POST"
										action="${pageContext.request.contextPath}/saveProductDetails">
										<div class="col-md-12 col-sm-12 col-xs-12">
											<fieldset>
												<legend>Product Title:</legend>
												<div class="form-group">
													<div class="col-md-12 col-sm-12 col-xs-12">
														<input name="productTitle" value="${gp.productTitle }" type="text" required="required"
															placeholder="Product Title"
															class="form-control col-md-7 col-xs-12">
													</div>
												</div>

												<div class="clearfix" style="height: 10px; clear: both;"></div>
											</fieldset>
										</div>
										<div class="col-md-12 col-sm-12 col-xs-12">
											<fieldset>
												<legend>Product Description:</legend>
												<div class="form-group">
													<div class="col-md-12 col-sm-12 col-xs-12">
														<textarea name="productDescription"  maxlength="50"  required="required"
															placeholder="Product Description"
															class="form-control col-md-7 col-xs-12" rows="5"></textarea>
													</div>
												</div>

												<div class="clearfix" style="height: 10px; clear: both;"></div>
											</fieldset>
										</div>
										<div class="col-md-12 col-sm-12 col-xs-12">
											<fieldset>
												<legend>Default Value:</legend>
												<div class="form-group">
													<div class="col-md-12 col-sm-12 col-xs-12">
														<input name="defaultValue" value="${gp.defaultValue }" required="required"
															placeholder="Product Default Value"
															class="form-control col-md-7 col-xs-12">
													</div>
												</div>

												<div class="clearfix" style="height: 10px; clear: both;"></div>
											</fieldset>
										</div>
										<div class="col-md-12 col-sm-12 col-xs-12">
											<fieldset>
												<legend>Category Details:</legend>
												<div class="form-group">
													<label class="control-label col-md-3 col-sm-3 col-xs-12">Category</label>
													<div class="col-md-12 col-sm-12 col-xs-12">
														<select id="categoryId" 
															class="form-control">
															<option>Choose Category</option>
															<c:forEach var="category" items="${categoryMap}">
																<option>${category.key}</option>
															</c:forEach>

														</select>
													</div>
												</div>
											</fieldset>
										</div>
										<div class="col-md-12 col-sm-12 col-xs-12">
											<fieldset>
												<legend>Sub-Category Details:</legend>
												<div class="form-group">
													<div class="form-group">
														<label class="control-label col-md-3 col-sm-3 col-xs-12">Nodes</label>
														<div class="col-md-12 col-sm-12 col-xs-12">
															<select name="category" class="form-control" id="subcatId">
															</select>
														</div>
													</div>
												</div>
											</fieldset>
										</div>
										<c:forEach items="${attributeDetails}" var="attr1"
											varStatus="status">

											<div class="col-md-6 col-sm-6 col-xs-12">
												<fieldset>
													<legend>${attr1.key}</legend>
													<div class="x_panel">
														<h3>
															<small>Edit Predefined Attributes</small>
														</h3>
														<div class="col-md-6 col-sm-6 col-xs-12">
															<div>
																<table>
																	<c:forEach var="m" items="${attr1.value}">
																		<tr id="Row1">
																			<td class="space">&nbsp;</td>
																		</tr>
																		<tr id="Row2">
																			<td><input name="attributename"
																				value="${attr1.key}" /></td>
																			<td style="display: none;"><input
																				name="subAttributesDetails[${status.index}].subAttributeId"
																				value="${m.subAttributeId}" /></td>

																			<td><input
																				name="subAttributesDetails[${status.index}].subAttributeName"
																				value="${m.subAttributeName}" /></td>
																			<td><input
																				name="subAttributesDetails[${status.index}].defaultValueOfAttribute"
																				value="${m.defaultValueOfAttribute}" /></td>

																			<td style="display: none;"><input
																				name="subAttributesDetails[${status.index}].id.id"
																				value="${m.id.id}" /></td>
																		</tr>
																	</c:forEach>
																</table>

															</div>
														</div>
													</div>
												</fieldset>
											</div>
										</c:forEach>
										<div class="form-group">
											<div class="col-lg-10 col-lg-offset-2">
												<button type="submit">Submit</button>
											</div>
										</div>
									</form>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<jsp:include page="adminScriptInclude.jsp"></jsp:include>
	<div id="custom_notifications" class="custom-notifications dsp_none">
		<ul class="list-unstyled notifications clearfix"
			data-tabbed_notifications="notif-group">
		</ul>
		<div class="clearfix"></div>
		<div id="notif-group" class="tabbed_notifications"></div>
	</div>


	<script type="text/javascript">
		$(document)
				.ready(
						function() {
							var map = ${categoryMapString};
							$('#categoryId')
									.change(
											function() {
												//Assign value to subcategory
												var cat = $('#categoryId')
														.val();
												console.log(map[cat]);
												$('#subcatId')
														.html(
																$("<option>Choose</option>"));
												$
														.each(
																map[cat],
																function(key,
																		value) {
																	$(
																			'#subcatId')
																			.append(
																					$(
																							"<option>--Select Node--</option>")
																							.attr(
																									"value",
																									value)
																							.text(
																									value));
																});

											});
						});
	</script>


</body>
</html>