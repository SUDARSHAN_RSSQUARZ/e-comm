<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="en">

<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<!-- Meta, title, CSS, favicons, etc. -->
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">

<title>Products</title>

<jsp:include page="adminCssInclude.jsp"></jsp:include>

</head>


<body class="nav-md">

	<div class="container body">


		<div class="main_container">

			<jsp:include page="navigationbar.jsp"></jsp:include>

			<!-- page content -->
			<div class="right_col" role="main">
				<div class="">
					<div class="page-title">
						<div class="title_left">
							<h3>
								Products <small> </small>
							</h3>
						</div>

						<div class="title_right"></div>
					</div>
					<div class="clearfix"></div>
					<div class="row">
						<div class="col-md-6 col-sm-12 col-xs-12">
							<div class="x_panel">
								<div class="x_title">
									<div class="clearfix"></div>
								</div>
								<div class="x_content">
									<table
										class="table table-striped responsive-utilities jambo_table">
										<tbody>
											<tr>
												<td class=" "><b>Product Name</b></td>
												<td class=" "><b>${product.name}</b></td>
											</tr>
											<tr>
												<td class=" "><b>Product Category</b></td>
												<td class=" "><b>${product.category}</b></td>
											</tr>
											<tr>
												<td class=" "><b>Product Sub-Category</b></td>
												<td class=" "><b>${product.subCategory}</b></td>
											</tr>
											<tr>
												<td class=" "><b>Default Price</b></td>
												<td class=" "><b>${product.defaultPrice}</b></td>
											</tr>
											<tr>
												<td class=" "><b>Type</b></td>
												<td class=" "><b>${product.type}</b></td>
											</tr>
										</tbody>
									</table>
								</div>
							</div>
						</div>
						<div class="col-md-6 col-sm-12 col-xs-12">
							<div class="x_panel">
								<div class="x_title">
									<div class="clearfix"></div>
								</div>
								<div class="x_content">
									<table
										class="table table-striped responsive-utilities jambo_table">
										<tbody>
											<tr>
												<td class=" "><b>Product Image</b></td>
												<td class=" "><img alt=""
													src="${pageContext.request.contextPath}${product.defaultImage}"
													width="60px" height="60px" class="img-responsive"></td>
											</tr>
										</tbody>
									</table>
								</div>
							</div>
						</div>
					</div>
					<div class="row">

						<div class="col-md-12 col-sm-12 col-xs-12">
							<div class="x_panel">
								<div class="x_title">
									<!-- <h2>Daily active users <small>Sessions</small></h2> -->

									<div class="clearfix"></div>
								</div>
								<div class="x_content">
									<table id="example"
										class="table table-striped responsive-utilities jambo_table">
										<thead>
											<tr class="headings">
												<th><input type="checkbox" class="tableflat"></th>
												<th>Price Id</th>
												<th>Product Color</th>
												<th>Product Price</th>
												<th>Product Weight</th>
												<th>Available Quantity</th>
												<th>Add Quantity</th>
												<th>Delete</th>
											</tr>
										</thead>

										<tbody>
											<c:forEach var="p" items="${attributesList}" varStatus="loop">

												<tr
													class="<c:choose><c:when test='${loop.index%2 == 0}'>	even</c:when><c:otherwise>odd</c:otherwise></c:choose> pointer">
													<td class="a-center "><input type="checkbox"
														class="tableflat"></td>
													<td class=" ">${p.id}</td>
													<td class=" ">${p.color}</td>
													<td class=" ">${p.price}</td>
													<td class=" ">${p.weight}</td>
													<td class=" ">${p.quantity}</td>
													<td class=" last"><a
														href="${pageContext.request.contextPath}/prepareAddQuantity/${p.product.id}/${p.id}">Add
															Quantity</a></td>
													<td><a
														href="${pageContext.request.contextPath}/deleteAttribute/${p.id}/${p.product.id}">Delete</a>
													</td>
												</tr>
											</c:forEach>
										</tbody>

									</table>
								</div>
							</div>
						</div>

						<br /> <br /> <br />

					</div>
				</div>

			</div>
			<!-- /page content -->
		</div>

	</div>

	<div id="custom_notifications" class="custom-notifications dsp_none">
		<ul class="list-unstyled notifications clearfix"
			data-tabbed_notifications="notif-group">
		</ul>
		<div class="clearfix"></div>
		<div id="notif-group" class="tabbed_notifications"></div>
	</div>

	<jsp:include page="adminScriptInclude.jsp"></jsp:include>
	<script>
		$(document).ready(function() {
			$('input.tableflat').iCheck({
				checkboxClass : 'icheckbox_flat-green',
				radioClass : 'iradio_flat-green'
			});
		});

		var asInitVals = new Array();
		$(document)
				.ready(
						function() {
							var oTable = $('#example')
									.dataTable(
											{
												"oLanguage" : {
													"sSearch" : "Search all columns:"
												},
												"aoColumnDefs" : [ {
													'bSortable' : false,
													'aTargets' : [ 0 ]
												} //disables sorting for column one
												],
												'iDisplayLength' : 12,
												"sPaginationType" : "full_numbers",
												"dom" : 'T<"clear">lfrtip',
												"tableTools" : {
													"sSwfPath" : "<?php echo base_url('assets2/js/Datatables/tools/swf/copy_csv_xls_pdf.swf'); ?>"
												}
											});
							$("tfoot input").keyup(
									function() {
										/* Filter on the column based on the index of this element's parent <th> */
										oTable.fnFilter(this.value, $(
												"tfoot th").index(
												$(this).parent()));
									});
							$("tfoot input").each(function(i) {
								asInitVals[i] = this.value;
							});
							$("tfoot input").focus(function() {
								if (this.className == "search_init") {
									this.className = "";
									this.value = "";
								}
							});
							$("tfoot input")
									.blur(
											function(i) {
												if (this.value == "") {
													this.className = "search_init";
													this.value = asInitVals[$(
															"tfoot input")
															.index(this)];
												}
											});
						});
	</script>
</body>

</html>