<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<!-- Meta, title, CSS, favicons, etc. -->
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Dashboard</title>
<!-- Bootstrap core CSS -->
<jsp:include page="adminCssInclude.jsp"></jsp:include>
</head>
<body class="nav-md">
	<div class="container body">
		<div class="main_container">

			<jsp:include page="navigationbar.jsp"></jsp:include>

			<!-- page content -->
			<div class="right_col" role="main">
				<div class="">
					<div class="page-title">
						<div class="title_left">
							<h3>Dashboard</h3>
						</div>
					</div>
					<c:forEach var="order" items="${orderList}" varStatus="loop">
						<c:set var="total" value="${delivered + remaining}" />
						<c:if test="${order.status==true}">
							<c:set var="delivered" value="${delivered + 1}" />
							<c:set var="totalamount" value="${totalamount + order.price}" />
						</c:if>
						<c:if test="${order.status==false}">
							<c:set var="remaining" value="${remaining + 1}" />
						</c:if>
					</c:forEach>

					<div class="clearfix"></div>
					<div class="row">
						<div class="x_title">
							<div class="col-md-4 col-sm-4 col-xs-12">
								<div class="x_panel tile fixed_height_320 overflow_hidden">
									<div class="x_title">
										<h2>Delivery Status</h2>

										<div class="clearfix"></div>
									</div>
									<div class="x_content">

										<table class="" style="width: 100%">
											<tr>

												<td>
											<tr>
												<td>
													<p>
														<i class="fa fa-square blue"></i> Total Orders
													</p>
												</td>
												<td>${total}</td>
											</tr>
											<tr>
												<td>
													<p>
														<i class="fa fa-square green"></i> Delivered
													</p>
												</td>
												<td>${delivered}</td>
											</tr>
											<tr>
												<td>
													<p>
														<i class="fa fa-square purple"></i> Remaining
													</p>
												</td>
												<td>${remaining}</td>
											</tr>
											<tr>
												<td>
													<p>
														<i class="fa fa-square purple"></i> Amount Paid
													</p>
												</td>
												<td>${totalamount}</td>
											</tr>
										</table>
									</div>
								</div>
							</div>
							<div class="col-md-8 col-sm-8 col-xs-12">
								<div class="panel panel-default">
									<div class="panel-heading">
										<h3 class="panel-title">
											<i class="fa fa-bar-chart-o fa-fw"></i> Order Chart
										</h3>
									</div>
									<div class="panel-body">
										<div id="newsChart2"></div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div id="custom_notifications" class="custom-notifications dsp_none">
		<ul class="list-unstyled notifications clearfix"
			data-tabbed_notifications="notif-group">
		</ul>
		<div class="clearfix"></div>
		<div id="notif-group" class="tabbed_notifications"></div>
	</div>


	<script>
		autosize($('.resizable_textarea'));
	</script>
	<script type="text/javascript">
		$(function() {
			'use strict';
			var countriesArray = $.map(countries, function(value, key) {
				return {
					value : value,
					data : key
				};
			});
			// Initialize autocomplete with custom appendTo:
			$('#autocomplete-custom-append').autocomplete({
				lookup : countriesArray,
				appendTo : '#autocomplete-container'
			});
		});
	</script>
	<script src="${pageContext.request.contextPath}/resources/js/custom.js"></script>
	<jsp:include page="adminScriptInclude.jsp"></jsp:include>
	<script src="http://code.highcharts.com/highcharts.js"></script>
	<script src="http://code.highcharts.com/highcharts-3d.js"></script>
	<script src="http://code.highcharts.com/modules/heatmap.js"></script>
	<script src="http://code.highcharts.com/modules/exporting.js"></script>

	<script type="text/javascript">
		$.ajax({
			url : '${pageContext.request.contextPath}/getOrders'
		}).done(function(data) {
			$('#newsChart2').highcharts({
				chart : {
					type : 'column',
					margin : 75,
					options3d : {
						enabled : true,
						alpha : 10,
						beta : 25,
						depth : 70
					}
				},
				title : {
					text : 'Order Chart'
				},
				plotOptions : {
					column : {
						depth : 25
					}
				},
				
				 xAxis: {
				        type: 'date',
				        categories: {
							data : data.orderDate,
						},
						 labels: {
					           /*  format: '{value:%Y-%m-%d}', */
					            rotation: 00,
					            align: 'centre'
					        }
				    },
				yAxis : {
					title : {
						text : 'Count'
					}

				},
				series : [ {
					name : 'Date',
					data : data,
				} ],

				plotOptions : {
					series : {
						cursor : 'pointer',
						point : {
							events : {
								click : function() {
									location.href = '#';
								},
							}
						}
					}
				}
			});
		});
	</script>


</body>
</html>