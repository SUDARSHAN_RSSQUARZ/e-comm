<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<div id="Mymodel" class="modal" role="dialog" aria-hidden="true">
		<div class="modal-dialog modal-lg">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal"
						aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
					<h4 class="modal-title" id="gridSystemModalLabel">Enter
						Details</h4>
				</div>
				<form action="${pageContext.request.contextPath}/checkout" method="post">	
					<div class="modal-body">
					
						<div class="container-fluid">
						<div class="row">
							<input type="hidden" id="pri" name="price" value="">
							<div class="col-md-6 col-xs-12">
								 <label for="InputName">Enter Name</label>
                            	<div class="input-group">
                                	<input type="text" class="form-control" name="nameUser" value="${mUser.name}"
                                	id="InputName" placeholder="Enter Name" required/>
								</div>
							</div>	
							<div class="col-md-6 col-xs-12">
								<label for="InputMobile">Mobile Number</label>
                            	<div class="input-group">
                                	 <input type="text" class="form-control" 
                                	 id="InputMobile" name="phNo" value="${mUser.phNo}" placeholder="Contact Number" required/>
								</div>
							</div>
							<div class="col-md-6 col-xs-12">	
								<label for="InputEmail">Email</label>
                            	<div class="input-group">
                                	 <input type="email" class="form-control" 
                                	 id="InputEmail" name="email" value="${mUser.email}" placeholder="Enter Email" required/>
								</div>
							</div>
							<div class="col-md-6 col-xs-12">	
								<label for="InputName">Address</label>
                            	<div class="input-group">
                                	<textarea class="form-control" name="address" 
                                	id="InputAddress" placeholder="Address" required ></textarea>
								</div>
							</div>
							<div class="col-md-6 col-xs-12">	
								<label for="InputName">Pincode</label>
                            	<div class="input-group">
                                	<input type="text" class="form-control" name="pin" 
                                	id="InputPin" placeholder="Pincode" value="${mUser.pincode}" required/>
								</div>
							</div>	
							<div class="col-md-6 col-xs-12">
								<input type="checkbox" name="vehicle" value="Accept terms and conditions">Accept terms and conditions<br>
							</div>
							</div>
						</div>
					</div>

					<div class="modal-footer">
						<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
						<button type="submit" class="btn btn-default">Order</button>
					</div>
					</form>
			</div>
			<!-- /.modal-content -->
		</div>
		<!-- /.modal-dialog -->
	</div>
	
<div id="RegisterModel" class="modal" role="dialog" aria-hidden="true">
		<div class="modal-dialog modal-lg">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal"
						aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
					<h4 class="modal-title" id="gridSystemModalLabel">Register</h4>
				</div>
			<div class="modal-body">

				<div class=" container">
					<div class=" register" style="padding:0px;">
						<form method="POST" action="${pageContext.request.contextPath}/wUserRegister">
							<div class="col-md-4 register-top-grid">
								<div>
									<span>Full Name</span> <input type="text" name="name"
										placeholder="Full Name" required>
								</div>
								<div>
									<span>Mobile</span> <input type="tel" name="phNo"
										placeholder="10 Digit mobile number" pattern="[0-9]{10}"
										required>
								</div>
								<div>
									<span>Email Address</span> <input type="email" name="email"
										placeholder="Email Id" required>
								</div>
								<div>
									<span>Password</span> <input type="password" name="password"
										placeholder="Password" required>
								</div>
								<div>
									<span>Confirm Password</span> <input type="password" name="name"
										placeholder="Retype Password" required>
								</div>
							</div>
							<div class="col-md-4 register-top-grid">	
								<div>
									<span>City</span> <input type="text" placeholder="City" name="city">
								</div>
								<div>
									<span>State</span> <input type="text" placeholder="State" name="state">
								</div>
								<div>
									<span>Pincode</span> <input type="text" placeholder="Pincode" name="pincode">
								</div>
								<div>
									<span>Country</span> <input type="text" placeholder="Country" name="country">
								</div>
								<input type="submit" value="Register">
								<!-- <a class="news-letter" href="#"> <label class="checkbox"><input
							type="checkbox" name="checkbox" checked=""><i> </i>Sign
							Up for Newsletter</label>
					</a> -->
							</div>
						</form>
					</div>
				</div>
			</div>
			<!-- /.modal-content -->
		</div>
		<!-- /.modal-dialog -->
	</div>	
</div>	
	
<div id="LoginModel" class="modal" role="dialog" aria-hidden="true">
		<div class="modal-dialog modal-lg">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal"
						aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
					<h4 class="modal-title" id="gridSystemModalLabel">Login</h4>
				</div>
			<div class="modal-body">

				<div class=" container">
					<div class=" register" style="padding:0px;">

						<form method="POST" action="${pageContext.request.contextPath}/wUserLogin">
							<div class="col-md-6 register-top-grid">
								<div>
									<span>Email Address</span> <input type="email" name="email"
										placeholder="Email Id" required>
								</div>
								<div>
									<span>Password</span> <input type="password" name="password"
										placeholder="Password" required>
								</div>
								<input type="submit" value="Login">

							</div>
							<div class="clearfix"></div>
						</form>
					</div>
				</div>
			</div>
			<!-- /.modal-content -->
		</div>
		<!-- /.modal-dialog -->
	</div>	
</div>	