<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="en">

<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<!-- Meta, title, CSS, favicons, etc. -->
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">

<title>Categories</title>
<jsp:include page="adminCssInclude.jsp"></jsp:include>
<!--[if lt IE 9]>
        <script src="../assets/js/ie8-responsive-file-warning.js"></script>
        <![endif]-->

<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
          <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->

</head>


<body class="nav-md">

	<div class="container body">


		<div class="main_container">

			<jsp:include page="navigationbar.jsp"></jsp:include>

			<!-- page content -->
			<div class="right_col" role="main">
				<div class="">

					<div class="page-title">
						<div class="title_left">
							<h3>Category</h3>
						</div>
						<div class="title_right">
							<c:if test="${not empty errorstatus}">
								<div class="alert alert-danger">
									<strong></strong> ${errorstatus }
								</div>
							</c:if>
							<c:if test="${not empty status}">
								<div class="alert alert-success">
									<strong></strong> ${status }
								</div>
							</c:if>
						</div>
					</div>
					<div class="clearfix"></div>
					<div class="row">
						<div class="col-md-6 col-sm-12 col-xs-12">
							<div class="x_panel">
								<div class="x_title">
									<h2>Add New Category</h2>

									<div class="clearfix"></div>
								</div>
								<div class="x_content">
									<br />

									<form:form method="POST"
										action="${pageContext.request.contextPath}/saveCategory"
										class="form-horizontal form-label-left">

										<div class="form-group">
											<label class="control-label col-md-3 col-sm-3 col-xs-12"
												for="categoryName">Category DB<span class="required">*</span>
											</label>
											<div class="col-md-6 col-sm-6 col-xs-12">
												<form:input id="categoryName" path="categoryName"
													type="text" class="form-control col-md-7 col-xs-12"
													required="required" placeholder="Category for DB" />

											</div>
										</div>
										<div class="form-group">
											<label class="control-label col-md-3 col-sm-3 col-xs-12"
												for="displayText">Category UI<span class="required">*</span>
											</label>
											<div class="col-md-6 col-sm-6 col-xs-12">
												<form:input id="displayText" path="displayText" type="text"
													class="form-control col-md-7 col-xs-12" required="required"
													placeholder="Category for UI" />

											</div>
										</div>
										<div class="form-group">
											<label class="control-label col-md-3 col-sm-3 col-xs-12"
												for="order">Order on UI<span class="required">*</span>
											</label>
											<div class="col-md-6 col-sm-6 col-xs-12">
												<form:input id="order" path="order" type="text"
													class="form-control col-md-7 col-xs-12" required="required"
													placeholder="Order" />

											</div>
										</div>
										<div class="form-group">
											<label class="control-label col-md-3 col-sm-3 col-xs-12"
												for="SubCategoryName">Add Sub-Category<span
												class="required">*</span>
											</label> <label>



												<div class="col-md-2 col-sm-2 col-xs-2">
													<input type="button" id="wtbtn" class="btn btn-success"
														value="+1">
												</div>
											</label>

										</div>
										<div id="cakeweight" class="form-group">
											<div class="col-md-4 col-sm-4 col-xs-4">
												<form:input id="SubCategoryName"
													path="subCategories[0].name" type="text"
													class="form-control col-md-7 col-xs-12" required="required"
													placeholder="SubCat in DB" />

											</div>
											<div class="col-md-4 col-sm-4 col-xs-4">
												<form:input path="subCategories[0].displayText" type="text"
													class="form-control col-md-7 col-xs-12" required="required"
													placeholder="SubCat on UI" />

											</div>
											<div class="col-md-4 col-sm-4 col-xs-4">
												<form:input path="subCategories[0].order" type="text"
													class="form-control col-md-7 col-xs-12" required="required"
													placeholder="SubCat Order" />
											</div>
										</div>
										<div class="ln_solid"></div>
										<div class="form-group">
											<div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
												<button class="btn btn-success" id="submitNewCategory">Submit</button>
											</div>
										</div>

									</form:form>
								</div>
							</div>

						</div>
						<div class="col-md-6 col-sm-12 col-xs-12">
							<div class="x_panel">
								<div class="x_title">
									<h2>Enter sub-category into existing category</h2>

									<div class="clearfix"></div>
								</div>
								<div class="x_content">
									<br />

									<form:form class="form-horizontal form-label-left"
										action="${pageContext.request.contextPath}/saveSubCategory"
										method="POST">
										<div class="form-group">
											<label class="control-label col-md-3 col-sm-3 col-xs-12">Categories</label>
											<div class="col-md-6 col-sm-9 col-xs-12">
												<select name="categoryName" class="form-control">
													<c:forEach var="category" items="${categoryMap}">
														<option value="${category.key}">${category.value}</option>
													</c:forEach>
												</select>
											</div>
										</div>
										<div class="form-group">
											<div class="form-group">
												<label class="control-label col-md-3 col-sm-3 col-xs-12"
													for="SubCategoryName">Add Sub-Category<span
													class="required">*</span>
												</label> <label>



													<div class="col-md-2 col-sm-2 col-xs-2">
														<input type="button" id="addSubCatOnly"
															class="btn btn-success" value="+1">
													</div>
												</label>

											</div>

										</div>
										<div id="subCatOnly" class="form-group">
											<div class="col-md-4 col-sm-4 col-xs-4">
												<form:input id="SubCategoryName"
													path="subCategories[0].name" type="text"
													class="form-control col-md-7 col-xs-12" required="required"
													placeholder="SubCat in DB" />

											</div>
											<div class="col-md-4 col-sm-4 col-xs-4">
												<form:input path="subCategories[0].displayText" type="text"
													class="form-control col-md-7 col-xs-12" required="required"
													placeholder="SubCat on UI" />

											</div>
											<div class="col-md-4 col-sm-4 col-xs-4">
												<form:input path="subCategories[0].order" type="text"
													class="form-control col-md-7 col-xs-12" required="required"
													placeholder="SubCat Order" />
											</div>
										</div>

										<div class="ln_solid"></div>
										<div class="form-group">
											<div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
												<button type="submit" class="btn btn-success">Submit</button>
											</div>
										</div>

									</form:form>
								</div>
							</div>
						</div>

						<script type="text/javascript">
							$(document).ready(
									function() {
										$('#birthday').daterangepicker(
												{
													singleDatePicker : true,
													calender_style : "picker_4"
												},
												function(start, end, label) {
													console.log(start
															.toISOString(), end
															.toISOString(),
															label);
												});
									});
						</script>
					</div>
					<!-- /page content -->

					<!-- footer content -->
					<footer>

						<div class="clearfix"></div>
					</footer>
					<!-- /footer content -->

				</div>

			</div>
		</div>
	</div>
	<div id="custom_notifications" class="custom-notifications dsp_none">
		<ul class="list-unstyled notifications clearfix"
			data-tabbed_notifications="notif-group">
		</ul>
		<div class="clearfix"></div>
		<div id="notif-group" class="tabbed_notifications"></div>
	</div>

	<jsp:include page="adminScriptInclude.jsp"></jsp:include>
	<script>
		autosize($('.resizable_textarea'));
	</script>
	<!-- Autocomplete -->

	<script type="text/javascript">
		$(function() {
			'use strict';
			var countriesArray = $.map(countries, function(value, key) {
				return {
					value : value,
					data : key
				};
			});
			// Initialize autocomplete with custom appendTo:
			$('#autocomplete-custom-append').autocomplete({
				lookup : countriesArray,
				appendTo : '#autocomplete-container'
			});
		});
	</script>
	<script src="${pageContext.request.contextPath}/resources/js/custom.js"></script>


	<!-- select2 -->
	<script>
		$(document).ready(function() {
			$(".select2_single").select2({
				placeholder : "Select a state",
				allowClear : true
			});
			$(".select2_group").select2({});
			$(".select2_multiple").select2({
				maximumSelectionLength : 4,
				placeholder : "With Max Selection limit 4",
				allowClear : true
			});
		});
	</script>
	<!-- /select2 -->
	<!-- input tags -->
	<script>
		function onAddTag(tag) {
			alert("Added a tag: " + tag);
		}

		function onRemoveTag(tag) {
			alert("Removed a tag: " + tag);
		}

		function onChangeTag(input, tag) {
			alert("Changed a tag: " + tag);
		}

		$(function() {
			$('#tags_1').tagsInput({
				width : 'auto'
			});
		});
	</script>
	<!-- /input tags -->
	<!-- form validation -->
	<script type="text/javascript">
		$(document).ready(function() {
			$.listen('parsley:field:validate', function() {
				validateFront();
			});
			$('#demo-form .btn').on('click', function() {
				$('#demo-form').parsley().validate();
				validateFront();
			});
			var validateFront = function() {
				if (true === $('#demo-form').parsley().isValid()) {
					$('.bs-callout-info').removeClass('hidden');
					$('.bs-callout-warning').addClass('hidden');
				} else {
					$('.bs-callout-info').addClass('hidden');
					$('.bs-callout-warning').removeClass('hidden');
				}
			};
		});

		$(document).ready(function() {
			$.listen('parsley:field:validate', function() {
				validateFront();
			});
			$('#demo-form2 .btn').on('click', function() {
				$('#demo-form2').parsley().validate();
				validateFront();
			});
			var validateFront = function() {
				if (true === $('#demo-form2').parsley().isValid()) {
					$('.bs-callout-info').removeClass('hidden');
					$('.bs-callout-warning').addClass('hidden');
				} else {
					$('.bs-callout-info').addClass('hidden');
					$('.bs-callout-warning').removeClass('hidden');
				}
			};
		});
		try {
			hljs.initHighlightingOnLoad();
		} catch (err) {
		}
	</script>
	<!-- /form validation -->
	<!-- editor -->
	<script type='text/javascript'>
		$(document)
				.ready(
						function() {
							var countSubCat = 0;
							var cntForSubCatOnly = 0;
							$('.xcxc').click(function() {
								$('#descr').val($('#editor').html());
							});
							$("#wtbtn")
									.click(
											function() {
												countSubCat++;
												$("#cakeweight")
														.append(
																"<div class='col-md-4 col-sm-4 col-xs-4'>													  <input name='subCategories["+countSubCat+"].name' type='text' class='form-control col-md-7 col-xs-12' required='required' placeholder='SubCat in DB'/>                                                       </div>												  <div class='col-md-4 col-sm-4 col-xs-4'>													  <input name='subCategories["+countSubCat+"].displayText' type='text' class='form-control col-md-7 col-xs-12' required='required' placeholder='SubCat on UI'/>                                                          </div>													   <div class='col-md-4 col-sm-4 col-xs-4'>													  <input name='subCategories["+countSubCat+"].order' type='text' class='form-control col-md-7 col-xs-12' required='required' placeholder='SubCat Order'/>                                        </div>");
											});
							$("#addSubCatOnly")
									.click(
											function() {
												cntForSubCatOnly++;
												$("#subCatOnly")
														.append(
																"<div class='col-md-4 col-sm-4 col-xs-4'>													  <input name='subCategories["+cntForSubCatOnly+"].name' type='text' class='form-control col-md-7 col-xs-12' required='required' placeholder='SubCat in DB'/>                                                       </div>												  <div class='col-md-4 col-sm-4 col-xs-4'>													  <input name='subCategories["+cntForSubCatOnly+"].displayText' type='text' class='form-control col-md-7 col-xs-12' required='required' placeholder='SubCat on UI'/>                                                          </div>													   <div class='col-md-4 col-sm-4 col-xs-4'>													  <input name='subCategories["+cntForSubCatOnly+"].order' type='text' class='form-control col-md-7 col-xs-12' required='required' placeholder='SubCat Order'/>                                        </div>");
											});
						});

		$(function() {
			function initToolbarBootstrapBindings() {
				var fonts = [ 'Serif', 'Sans', 'Arial', 'Arial Black',
						'Courier', 'Courier New', 'Comic Sans MS', 'Helvetica',
						'Impact', 'Lucida Grande', 'Lucida Sans', 'Tahoma',
						'Times', 'Times New Roman', 'Verdana' ], fontTarget = $(
						'[title=Font]').siblings('.dropdown-menu');
				$
						.each(
								fonts,
								function(idx, fontName) {
									fontTarget
											.append($('<li><a data-edit="fontName ' + fontName + '" style="font-family:\'' + fontName + '\'">'
													+ fontName + '</a></li>'));
								});
				$('a[title]').tooltip({
					container : 'body'
				});
				$('.dropdown-menu input').click(function() {
					return false;
				}).change(
						function() {
							$(this).parent('.dropdown-menu').siblings(
									'.dropdown-toggle').dropdown('toggle');
						}).keydown('esc', function() {
					this.value = '';
					$(this).change();
				});

				$('[data-role=magic-overlay]').each(
						function() {
							var overlay = $(this), target = $(overlay
									.data('target'));
							overlay.css('opacity', 0).css('position',
									'absolute').offset(target.offset()).width(
									target.outerWidth()).height(
									target.outerHeight());
						});
				if ("onwebkitspeechchange" in document.createElement("input")) {
					var editorOffset = $('#editor').offset();
					$('#voiceBtn').css('position', 'absolute').offset(
							{
								top : editorOffset.top,
								left : editorOffset.left
										+ $('#editor').innerWidth() - 35
							});
				} else {
					$('#voiceBtn').hide();
				}
			}
			;

			function showErrorAlert(reason, detail) {
				var msg = '';
				if (reason === 'unsupported-file-type') {
					msg = "Unsupported format " + detail;
				} else {
					console.log("error uploading file", reason, detail);
				}
				$(
						'<div class="alert"> <button type="button" class="close" data-dismiss="alert">&times;</button>'
								+ '<strong>File upload error</strong> '
								+ msg
								+ ' </div>').prependTo('#alerts');
			}
			;
			initToolbarBootstrapBindings();
			$('#editor').wysiwyg({
				fileUploadError : showErrorAlert
			});
			window.prettyPrint && prettyPrint();
		});

		/*$('#submitNewCategory').click(function(){			
		$.ajax({
		url: '${pageContext.request.contextPath}/category',
		method : 'POST'
		context: document.body
		}).done(function() {
		alert('Category Added');
		//populateCategoryList();
		}).error(function(){
		alert('Pls try again')
		});
			
		});*/

		//function populateCategoryList(){		}
	</script>
	<!-- /editor -->
</body>

</html>