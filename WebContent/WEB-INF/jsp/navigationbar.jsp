<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<div class="col-md-3 left_col">
               <div class="left_col scroll-view">
                  <div class="navbar nav_title" style="border: 0;">
                     <a href="${pageContext.request.contextPath}/home" class="site_title"><i class="fa fa-paw"></i> <span>E-Commerse</span></a>
                  </div>
                  <div class="clearfix"></div>
                  <!-- menu prile quick info -->
                  
                  <!-- /menu prile quick info -->
                  <br />
                  <!-- sidebar menu -->
                  <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
                     <div class="menu_section">
                        <h3>General</h3>
                        <ul class="nav side-menu">
                           <li><a href="${pageContext.request.contextPath}/home"><i class="fa fa-home"></i> Home </a>
                           </li>
                           <li>
                              <a><i class="fa fa-edit"></i> Category <span class="fa fa-chevron-down"></span></a>
                              <ul class="nav child_menu" style="display: none">
                                 <li><a href="${pageContext.request.contextPath}/category">Add/Edit Category</a>
                                 </li>
                              </ul>
                           </li>
                           <li>
                              <a><i class="fa fa-edit"></i> Product <span class="fa fa-chevron-down"></span></a>
                              <ul class="nav child_menu" style="display: none">
                                 <li><a href="${pageContext.request.contextPath}/product">Add Product</a>
                                 </li>
                                 <li><a href="${pageContext.request.contextPath}/products">List Products</a>
                                 </li>
                                  <%-- <li><a href="${pageContext.request.contextPath}/outOfStockProducts">List Out Of Stock Products</a>
                                 </li> --%>
                              </ul>
                           </li>
                            <li>
                              <a><i class="fa fa-edit"></i>Generic Product <span class="fa fa-chevron-down"></span></a>
                              <ul class="nav child_menu" style="display: none">
                                 <li><a href="${pageContext.request.contextPath}/prepareCategory">Add Category/Sub-Category</a>
                                 </li> 
                                 <li><a href="${pageContext.request.contextPath}/prepareAddAttributes">Add Attributes/Sub Attributes</a>
                                 </li>
                                 <li><a href="${pageContext.request.contextPath}/prepareAddProductDetails">Add New Products</a>
                                 </li>
                                 <li><a href="${pageContext.request.contextPath}/showGenericProductDetails">List All Products</a>
                                 </li>
                              </ul>
                           </li>
                           <li>
                              <a href="${pageContext.request.contextPath}/coupons"><i class="fa fa-edit"></i> Coupons </a>
                           </li>
                           <li>
                              <a href="${pageContext.request.contextPath}/orders"><i class="fa fa-edit"></i> Orders </a>
                           </li>
                           <li>
                              <a href="${pageContext.request.contextPath}/mUsers"><i class="fa fa-edit"></i> Users </a>
                           </li>
                            <li>
                              <a href="${pageContext.request.contextPath}/bannerimages"><i class="fa fa-edit"></i> Banner </a>
                           </li>
                           <li>
                              <a href="${pageContext.request.contextPath}/notification"><i class="fa fa-edit"></i> Notification </a>
                           </li>
                           <li>
                              <a href="${pageContext.request.contextPath}/preparepincodes"><i class="fa fa-edit"></i> Pincodes </a>
                           </li>
                           <!-- <li>
                              <a><i class="fa fa-table"></i> SMS <span class="fa fa-chevron-down"></span></a>
                              <ul class="nav child_menu" style="display: none">
                                 <li><a href="sms.html">Send SMS</a>
                                 </li>
                                 <li><a href="sms.html">SMS Log</a>
                                 </li>
                              </ul>
                           </li> -->
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
            <!-- top navigation -->
            <div class="top_nav">
               <div class="nav_menu">
                  <nav class="" role="navigation">
                     <div class="nav toggle">
                        <a id="menu_toggle"><i class="fa fa-bars"></i></a>
                     </div>
                     <ul class="nav navbar-nav navbar-right">
                        <li class="">
                           <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                           <%-- <img src="${pageContext.request.contextPath}/resources/images/img.jpg" alt=""> --%>
                           <c:out value="${sessionScope.user}" />
                           <span class=" fa fa-angle-down"></span>
                           </a>
                           <ul class="dropdown-menu dropdown-usermenu animated fadeInDown pull-right">
                             <!--  <li><a href="javascript:;">  Profile</a>
                              </li>
                              <li>
                                 <a href="javascript:;">
                                 <span class="badge bg-red pull-right">50%</span>
                                 <span>Settings</span>
                                 </a>
                              </li>
                              <li>
                                 <a href="javascript:;">Help</a>
                              </li> -->
                              <li><a href="${pageContext.request.contextPath}/logout"><i class="fa fa-sign-out pull-right"></i> Log Out</a>
                              </li>
                           </ul>
                        </li>
                        <%-- <li role="presentation" class="dropdown">
                           <a href="javascript:;" class="dropdown-toggle info-number" data-toggle="dropdown" aria-expanded="false">
                           <i class="fa fa-envelope-o"></i>
                           <span class="badge bg-green">1</span>
                           </a>
                           <ul id="menu1" class="dropdown-menu list-unstyled msg_list animated fadeInDown" role="menu">
                              <li>
                                 <a>
                                 <span class="image">
                                 <img src="${pageContext.request.contextPath}/resources/images/img.jpg" alt="Profile Image" />
                                 </span>
                                 <span>
                                 <span>admin</span>
                                 <span class="time">3 mins ago</span>
                                 </span>
                                 <span class="message">
                                 Ecomm dashboard portal  
                                 </span>
                                 </a>
                              </li>
                              
                              <li>
                                 <div class="text-center">
                                    <a>
                                    <strong>See All Alerts</strong>
                                    <i class="fa fa-angle-right"></i>
                                    </a>
                                 </div>
                              </li>
                           </ul>
                        </li> --%>
                     </ul>
                  </nav>
               </div>
            </div>
            <!-- /top navigation -->