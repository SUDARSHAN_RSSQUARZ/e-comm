<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<div class="banner">
	<script>
		$(function() {
			$("#slider").responsiveSlides({
				auto : true,
				nav : true,
				speed : 500,
				namespace : "callbacks",
				pager : true,
			});
		});
	</script>
	<div id="top" class="callbacks_container">
		<ul class="rslides" id="slider">
			<c:forEach var="banner" items="${bannerList}" varStatus="loop">
			<li>
				<div class="banner-text"
					style="background: transparent url('${pageContext.request.contextPath}${banner.bannerImage}') no-repeat scroll 0% 0%/cover; width: 100%; position: relative;">
					<h3 style="padding-left:30px;color:#FF0066;"> ${banner.bannerText}</h3>
				</div>
			</li>
			</c:forEach>
			<!-- <li>
				<div class="banner-text"
					style="background: transparent url('resources/images/6.jpg') no-repeat scroll 0% 0%/cover; width: 100%; position: relative;">
					<h3 style="padding-left:30px;color:#400000;">Pure ghee sweets <br>for all occasions.   </h3>
				</div>
			</li>
			<li>
				<div class="banner-text"
					style="background: transparent url('resources/images/5.jpg') no-repeat scroll 0% 0%/cover; width: 100%; position: relative;">
					<h3 style="padding-left:30px;color:#400000;">Customized chocolates <br>for your dear ones </h3>
				</div>
			</li> -->
		</ul>
	</div>
</div>
