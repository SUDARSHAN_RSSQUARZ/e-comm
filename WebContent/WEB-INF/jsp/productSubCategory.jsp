<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
   <head>
      <title>${page.title}</title>
      <jsp:include page="websiteScript.jsp"></jsp:include>
      <!--//theme-style-->
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
      
      <script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
      <!--fonts-->
      <!--//fonts-->
      <!-- start menu -->
	  <link rel="shortcut icon" type="image/png" href="${pageContext.request.contextPath}/resources/images/favicon.png"/>
	  <script type="text/javascript">
	  window.$zopim||(function(d,s){var z=$zopim=function(c){z._.push(c)},$=z.s=
		  d.createElement(s),e=d.getElementsByTagName(s)[0];z.set=function(o){z.set.
		  _.push(o)};z._=[];z.set._=[];$.async=!0;$.setAttribute("charset","utf-8");
		  $.src="//v2.zopim.com/?3a0P6Plo0iOMK5TF0Eg8CqvSBOR4WnKY";z.t=+new Date;$.
		  type="text/javascript";e.parentNode.insertBefore($,e)})(document,"script");
    </script>
   </head>
   <body>
      <!--header-->
      <jsp:include page="header.jsp"></jsp:include>
     <!-- <c:if test="${showSubCat==true}">
	 <div class="parallax-window" data-parallax="scroll" data-image-src="${pageContext.request.contextPath}/resources/images/3.jpg"></div>
	 </c:if>-->
      <div class="container">
	 
         <div class="cont">
            
               <div class="content-top">
                  <p style="font-size: 20px;text-align: justify;text-justify: inter-word;">
				  ${page.headerText}
                  </p>
				  </div>
                 
				  <div class="grid-in">
				  <c:forEach var="entry" items="${productList}">
				  
				<!--<div class="rowfluid content-top">-->
					<c:if test="${showSubCat==true}">
					
					
					<h1 style="padding-top:20px;text-align:center;">${entry.key}</h1>
					 <div class="grid-in">
					</c:if>
				
						<c:forEach var="product" items="${entry.value}">
							<div class="col-md-3 grid-top simpleCart_shelfItem">
								<a href="${pageContext.request.contextPath}/product/${product.id}"
									class="b-link-stripe b-animate-go  thickbox"> <img
									class="img-responsive"
									src="${pageContext.request.contextPath}/${product.images.get(0).getProductImage()}" alt="">
									<div class="b-wrapper">
										<h3 class="b-animate b-from-left    b-delay03 ">
											<span>${product.name}</span>
										</h3>
									</div>
								</a>
								<p style="text-align:center;">
									<a href="${pageContext.request.contextPath}/product/${product.id}">${product.name}</a>
								</p>
								<a href="${pageContext.request.contextPath}/product/${product.id}" class="item_add">
									<p class="number item_price">
										<i> </i>${product.defaultPrice}</p>
								</a>
							</div>
						</c:forEach>
						
					<c:if test="${showSubCat==true}">
					
					<div class="clearfix"> </div>
					
					 </div>
					 <hr>
					</c:if>
					
				
				

			</c:forEach>
				</div>
				  </div>
            </div>
            <!----->
       
      
      <!-- footer -->
      <jsp:include page="footer.jsp"></jsp:include>
      <jsp:include page="modal.jsp"></jsp:include>
   </body>
</html>