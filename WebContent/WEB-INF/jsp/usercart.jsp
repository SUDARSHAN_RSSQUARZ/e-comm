<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<!DOCTYPE html>
<html lang="en">
   <head>
      <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
      <!-- Meta, title, CSS, favicons, etc. -->
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <title>User Cart </title>
      <!-- Bootstrap core CSS -->
      <jsp:include page="adminCssInclude.jsp"></jsp:include>
   </head>
   <body class="nav-md">
      <div class="container body">
         <div class="main_container">
            
            <jsp:include page="navigationbar.jsp"></jsp:include>
            
            <!-- page content -->
            <div class="right_col" role="main">
               <div class="">
                  <div class="page-title">
						<c:choose>
							<c:when test="${fn:length(productList) gt 0}">
        						<div class="title_left">
                        			<h3>Cart Items</h3>
                     			</div>
							</c:when>
							<c:otherwise>
        						<div class="title_left">
                        			<h3>Cart is empty</h3>
                     			</div>
							</c:otherwise>
						</c:choose>
                     <div class="title_right">
                        <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                           <div class="input-group">
                              <input type="text" class="form-control" placeholder="Search for...">
                              <span class="input-group-btn">
                              <button class="btn btn-default" type="button">Go!</button>
                              </span>
                           </div>
                        </div>
                     </div>
                  </div>
                  <div class="clearfix"></div>
                  
                  <c:forEach var="product" items="${productList}"
										varStatus="loop">
                  <div class="row">
                     <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="x_panel">
                           <div class="x_title">
                              <h2><small>Product</small></h2>
                              <div class="clearfix"></div>
                           </div>
                           <div class="x_content">
                              <br />
										<div class="col-md-3">
											<c:forEach var="img" items="${product.images}" varStatus="ind">
												<%-- <c:if test="${ind==0}"> --%>
													<img alt="Product Image" src="${pageContext.request.contextPath}${img.productImage}" class="img-responsive">
											</c:forEach>
										</div>
										<div class="col-md-9">
										<div class="col-md-6">
											<div class="form-group">
												<label class="control-label col-md-3 col-sm-3 col-xs-12"
													for="first-name">Product Name : </label> <label
													class="control-label col-md-3 col-sm-3 col-xs-12"
													for="first-name">${product.name}</label>
											</div>
										</div>
										<div class="col-md-6">
											<div class="form-group">
												<label class="control-label col-md-3 col-sm-3 col-xs-12"
													for="first-name">Product Category : </label> <label
													class="control-label col-md-3 col-sm-3 col-xs-12"
													for="first-name">${product.category}</label>
											</div>
										</div>
										<div class="col-md-6">
											<div class="form-group">
												<label class="control-label col-md-3 col-sm-3 col-xs-12"
													for="first-name">Product Sub-Category : </label> <label
													class="control-label col-md-3 col-sm-3 col-xs-12"
													for="first-name">${product.subCategory}</label>
											</div>
										</div>
										<div class="col-md-6">
											<div class="form-group">
												<label class="control-label col-md-3 col-sm-3 col-xs-12"
													for="first-name">Product Default Price : </label> <label
													class="control-label col-md-3 col-sm-3 col-xs-12"
													for="first-name">${product.defaultPrice}</label>
											</div>
										</div>
										</div>
									<br><br>
                           </div>
                        </div>
                     </div>
                  </div>
                  </c:forEach>
                  <div class="row">
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="x_panel">
                            	<div class="x_title">
                              		<h2>Previous Orders</h2>
                              		<div class="clearfix"></div>
                           		</div>
                                <div class="x_content">
                                    <table id="example" class="table table-striped responsive-utilities jambo_table">
                                        <thead>
                                            <tr class="headings">
                                                <th>
                                                    <input type="checkbox" class="tableflat">
                                                </th>
												<th>Id </th>
                                                <th>UserName </th>
                                                <th>Email </th>
                                                <th>Phone </th>
                                                <th>Address </th>
                                                <th>Pin </th>
                                                <th>Price </th>
                                                <th>Status </th>
                                                <th class=" no-link last"><span class="nobr">Action</span>
                                                </th>
                                            </tr>
                                        </thead>

                                        <tbody>
										<c:forEach var="o" items="${orderList}" varStatus="loop">
										
                                            <tr class="<c:choose><c:when test='${loop.index%2 == 0}'>	even</c:when><c:otherwise>odd</c:otherwise></c:choose> pointer">
                                                <td class="a-center ">
                                                    <input type="checkbox" class="tableflat">
                                                </td>
                                                <td class=" ">${o.id}</td>
                                                <td class=" ">${o.nameUser}</td>
                                                <td class=" ">${o.email}</td>
                                                <td class=" ">${o.phNo}</td>
                                                <td class=" ">${o.address}</td>
                                                <td class=" ">${o.pin}</td>
                                                <td class=" ">${o.price}</td>
                                                <c:if test="${o.status==true}">
			 										<td class=" ">Checked</td> 
												</c:if>
												<c:if test="${o.status==false}">
													<td class=" "><a href="${pageContext.request.contextPath}/checkorder?id=${o.id}">Check</a></td>
												</c:if>
                                                <td class=" "><a href="${pageContext.request.contextPath}/order/${o.id}">
                                                	<input type="button" class="btn btn-success" value="View"></a></td>
                                            </tr>
											</c:forEach>
                                            </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
               </div>
               <!-- /page content -->
            </div>
         </div>
      </div>
      <div id="custom_notifications" class="custom-notifications dsp_none">
         <ul class="list-unstyled notifications clearfix" data-tabbed_notifications="notif-group">
         </ul>
         <div class="clearfix"></div>
         <div id="notif-group" class="tabbed_notifications"></div>
      </div>
      <jsp:include page="adminScriptInclude.jsp"></jsp:include>
      
      <script>
         autosize($('.resizable_textarea'));
      </script>
      <!-- Autocomplete -->
      
      <script type="text/javascript">
         $(function () {
             'use strict';
             var countriesArray = $.map(countries, function (value, key) {
                 return {
                     value: value,
                     data: key
                 };
             });
             // Initialize autocomplete with custom appendTo:
             $('#autocomplete-custom-append').autocomplete({
                 lookup: countriesArray,
                 appendTo: '#autocomplete-container'
             });
         });
      </script>
      <script src="${pageContext.request.contextPath}/resources/js/custom.js"></script>
      <!-- select2 -->
      <script>
         $(document).ready(function () {
			 
			 loadAttributes();
             $(".select2_single").select2({
                 placeholder: "Select a state",
                 allowClear: true
             });
             $(".select2_group").select2({});
             $(".select2_multiple").select2({
                 maximumSelectionLength: 4,
                 placeholder: "With Max Selection limit 4",
                 allowClear: true
             });
         
         });
		 
         
      </script>
      <!-- /select2 -->
      <!-- input tags -->
      <script>
         function onAddTag(tag) {
             alert("Added a tag: " + tag);
         }
         
         function onRemoveTag(tag) {
             alert("Removed a tag: " + tag);
         }
         
         function onChangeTag(input, tag) {
             alert("Changed a tag: " + tag);
         }
         
         $(function () {
             $('#tags_1').tagsInput({
                 width: 'auto'
             });
         });
      </script>
      <!-- /input tags -->
      <!-- editor -->
      <script type="text/javascript">
      	$('#print').click(function () {
  	    	window.print();
  		});	
         $(document).ready(function () {
             $('.xcxc').click(function () {
                 $('#descr').val($('#editor').html());
             });
         });
         
         $(function () {
             function initToolbarBootstrapBindings() {
                 var fonts = ['Serif', 'Sans', 'Arial', 'Arial Black', 'Courier',
             'Courier New', 'Comic Sans MS', 'Helvetica', 'Impact', 'Lucida Grande', 'Lucida Sans', 'Tahoma', 'Times',
             'Times New Roman', 'Verdana'],
                     fontTarget = $('[title=Font]').siblings('.dropdown-menu');
                 $.each(fonts, function (idx, fontName) {
                     fontTarget.append($('<li><a data-edit="fontName ' + fontName + '" style="font-family:\'' + fontName + '\'">' + fontName + '</a></li>'));
                 });
                 $('a[title]').tooltip({
                     container: 'body'
                 });
                 $('.dropdown-menu input').click(function () {
                         return false;
                     })
                     .change(function () {
                         $(this).parent('.dropdown-menu').siblings('.dropdown-toggle').dropdown('toggle');
                     })
                     .keydown('esc', function () {
                         this.value = '';
                         $(this).change();
                     });
         
                 $('[data-role=magic-overlay]').each(function () {
                     var overlay = $(this),
                         target = $(overlay.data('target'));
                     overlay.css('opacity', 0).css('position', 'absolute').offset(target.offset()).width(target.outerWidth()).height(target.outerHeight());
                 });
                 if ("onwebkitspeechchange" in document.createElement("input")) {
                     var editorOffset = $('#editor').offset();
                     $('#voiceBtn').css('position', 'absolute').offset({
                         top: editorOffset.top,
                         left: editorOffset.left + $('#editor').innerWidth() - 35
                     });
                 } else {
                     $('#voiceBtn').hide();
                 }
             };
         
             function showErrorAlert(reason, detail) {
                 var msg = '';
                 if (reason === 'unsupported-file-type') {
                     msg = "Unsupported format " + detail;
                 } else {
                     console.log("error uploading file", reason, detail);
                 }
                 $('<div class="alert"> <button type="button" class="close" data-dismiss="alert">&times;</button>' +
                     '<strong>File upload error</strong> ' + msg + ' </div>').prependTo('#alerts');
             };
             initToolbarBootstrapBindings();
             $('#editor').wysiwyg({
                 fileUploadError: showErrorAlert
             });
             window.prettyPrint && prettyPrint();
         });
      </script>
   </body>
</html>