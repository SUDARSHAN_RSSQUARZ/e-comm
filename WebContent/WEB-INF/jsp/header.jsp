<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<div class="social-icons">
			<div class="social" style="padding:3px;">
						<ul>
							<li><a href="#"><i class="facebok"> </i></a></li><br>
							<li><a href="#"><i class="twiter"> </i></a></li><br>
							<li><a href="#"><i class="inst"> </i></a></li><br>
							<li><a href="#"><i class="goog"> </i></a></li><br>
							<li><a href="#" id="callbackbtn"><i class="requestcall"></i></a></li>
							<div class="clearfix"></div>	
						</ul>
						
						<div class="callbackdia">
							<img src="${pageContext.request.contextPath}/resources/images/callback.png"><br><br><br>
							<input id="phoneNumber" type="text" value="+91" class="mobnumber" ><br><br>
							<button class="btn btn-default" style="background:#6C3F9A;margin-left:60px;color:#fff;" id="requestCallBack">Send</button>
						</div>
						
					</div>
		</div>
<div class="header">
	<div class="container-fluid" id="nv-br" style="z-index:100;">
		<div class="logo">
			<h1><a href="${pageContext.request.contextPath}/"><img src="${pageContext.request.contextPath}/resources/images/honeybuzz.jpg" width=170px height=40px></a></h1>
		</div>
		<div class=" h_menu4">
			<ul class="memenu skyblue">
				<c:forEach var="entry" items="${topNav}">
					<li style="float: left;position: relative;"><a class="color8" href="#">${entry.key}</a>
						<div class="mepanel">
							<div class="h_nav">
								<ul>
									<c:forEach var="subCat" items="${entry.value}">
										<li><a href="${pageContext.request.contextPath}/products/${entry.key}">${subCat}</a></li>
									</c:forEach>
								</ul>	
							</div>							
						</div>
					</li>
				</c:forEach>
				<li>
					<div class="cart box_1">
						<a href="${pageContext.request.contextPath}/order">
							<h3> <div class="total">
							</div>
							<img src="${pageContext.request.contextPath}/resources/images/cart.png" alt=""/></h3>
						</a>
						<p><a href="${pageContext.request.contextPath}/order" class="simpleCart_empty">My Cart</a></p>
						
					</div>
				</li>
				<c:if test="${sessionScope.wUser==null}">
				<li>
					<div class="cart box_1">
						<p><a href="#" class="simpleCart_empty" id="register">Register</a></p>
						<p><a href="#" class="simpleCart_empty" id="login">Login</a></p>
						
					</div>
				</li>
				</c:if>
				<c:if test="${sessionScope.wUser!=null}">
					<li style="float: left;position: relative;"><a class="color8" href="#">My Account</a>
						<div class="mepanel">
							<div class="h_nav">
								<ul>
									<li>${sessionScope.wUser}</li>
									<li><a href="${pageContext.request.contextPath}/order">My cart</a></li>
									<li><a href="${pageContext.request.contextPath}/prepviousorders">Previous Orders</a></li>
									<li><a href="${pageContext.request.contextPath}/websitelogout">Logout</a></li>
								</ul>	
							</div>							
						</div>
					</li>
				</c:if>
			</ul> 
		</div>
	</div>
	
</div>
<script>
  $(document).ready(function () {
	  
	  $('#requestCallBack').click(callBack);
	  
	  $('#register').click(function (){
		  $('#RegisterModel').modal('show');
	  });
	  
	  $('#login').click(function (){
		  $('#LoginModel').modal('show');
	  });
	  
  });
  
  function callBack(){
	  var str = $("#phoneNumber").val().trim();
	  var data0 = {sessionToken: str,data:{}};

var json = JSON.stringify(data0); 


$.ajax({
    type:"post",
    data:json,
    url:"${pageContext.request.contextPath}/requestCallBack",
    async: false,
     contentType : "application/json",
            dataType : "json"
    
})


  }

</script>