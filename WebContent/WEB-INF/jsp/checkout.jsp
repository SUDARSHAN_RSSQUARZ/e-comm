<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<!DOCTYPE html>
<html>
   <head>
      <title>Checkout</title>
      <jsp:include page="websiteScript.jsp"></jsp:include>
      <!--//theme-style-->
      <link rel="shortcut icon" type="image/png"
         href="${pageContext.request.contextPath}/resources/images/favicon.png" />
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
      <meta name="keywords" content="Honey Buzz" />
      <script type="application/x-javascript">
         addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } 
         
         
      </script>
      <!--fonts-->
      <link
         href='http://fonts.googleapis.com/css?family=Lato:100,300,400,700,900'
         rel='stylesheet' type='text/css'>
      <link
         href='http://fonts.googleapis.com/css?family=Roboto:400,100,300,500,700,900'
         rel='stylesheet' type='text/css'>
      <!--//fonts-->
      <!-- start menu -->
      <link href="${pageContext.request.contextPath}/resources/css/memenu.css"
         rel="stylesheet" type="text/css" media="all" />
      <script type="text/javascript"
         src="${pageContext.request.contextPath}/resources/js/memenu.js"></script>
      <script>
         $(document).ready(function() {
         	/*if($('#productcost').text()<240){
				$('#delcharge').text('40');
         	}*/
         	$('#placeOrder').click(placeOrder);
         	//Validations
         	$('#InputMobile').mask('9999999999');
			$('#InputMobile,#InputEmail,#InputName,#InputPin,#InputAddress').prop('required',true);
         });
         function isEmail(email) {
          var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
          return regex.test(email);
         }
      </script>
      <script
         src="${pageContext.request.contextPath}/resources/js/simpleCart.min.js"></script>
      <script
         src="${pageContext.request.contextPath}/resources/js/imagezoom.js"></script>
      <script src="${pageContext.request.contextPath}/resources/js/cookie.js"></script>
      <!--Start of Zopim Live Chat Script-->
      <script type="text/javascript">
      <!--Start of Zopim Live Chat Script-->
      window.$zopim||(function(d,s){var z=$zopim=function(c){z._.push(c)},$=z.s=
    	  d.createElement(s),e=d.getElementsByTagName(s)[0];z.set=function(o){z.set.
    	  _.push(o)};z._=[];z.set._=[];$.async=!0;$.setAttribute("charset","utf-8");
    	  $.src="//v2.zopim.com/?3a0P6Plo0iOMK5TF0Eg8CqvSBOR4WnKY";z.t=+new Date;$.
    	  type="text/javascript";e.parentNode.insertBefore($,e)})(document,"script");

         
         function placeOrder(){
         	
         	
         	var name = $("#InputName").val().trim();
         	var email = $("#InputEmail").val().trim();
         	var mobile = $("#InputMobile").val().trim();
         	var atposition=email.indexOf("@");  
         	var dotposition=email.lastIndexOf("."); 
         	
         	if (name==null || name=="" || name.length>20){  
         		alert("Name must be maximum 20 character long");
         		return false;
         	}
         	
         	if(!isEmail(email)){
         		alert("Please enter a valid e-mail address");  
         		  return false;  
         	}
         		
         		$('#Mymodel').modal('hide');
         		$.ajax({
                        url: "${pageContext.request.contextPath}/checkout",
         			headers: { 
                				'Accept': 'application/json',
                				'Content-Type': 'application/json' 
            			},
                        data: JSON.stringify({
         				email: email,
                            nameUser: name,
         				phNo: mobile,
         				address: $("#InputPin").val().trim(),
         				pin: $("#InputAddress").val().trim(),
         				price: $("#totalamount").text().trim()
                        }),
                        type: "POST",
                        dataType : "json",
                     
                    }).done(function (data) {
         			window.location.href = "${pageContext.request.contextPath}/payu";
         		}).fail(function (jqXHR, textStatus) {
         alert("Some thing went wrong. Place your order again");
         		});
         	
         	
         	
         	}
         	
         	function openModal(){
         		var amount = $('#productcost').text().trim();
         		if(amount==0){
         			alert("Please add products into your basket");
         		}else{
         			document.getElementById('pri').value=$("#totalamount").text().trim();
         			$('#Mymodel').modal('show');
         		}
         			
         	}
      </script>
   </head>
   <body>
      <!--header-->
      <jsp:include page="header.jsp"></jsp:include>
      <!-- grow -->
      <div class="grow">
         <div class="container">
            <h2>Checkout</h2>
         </div>
      </div>
      <!-- grow -->
      <div class="container">
         <div class="check">
            <c:if test="${not empty status}">
               <div class="alert alert-success">
                  <strong>Success!</strong> ${status }
               </div>
            </c:if>
            <div id="alert">
				
			</div>
            <div class="col-md-12 register-top-grid">
				<div class="col-md-8 col-sm-10">
					<input type="text" placeholder="Enter Coupon Code" id="coupon" style="height:40px;">
				</div>
				<div class="col-md-2 col-sm-2" style="padding-top:15px;">	
					<input type="submit" value="Apply" id="checkcoupon">
				</div>	
			</div>
            
            <h1>My Shopping Bag (${fn:length(sessionScope.productList)})</h1>
            <div class="col-md-9 cart-items">
               <script>
                  $(document)
                  		.ready(
                  				function(c) {
                  					$('.close1')
                  							.on(
                  									'click',
                  									function(c) {
                  										var status = $(this)
                  												.attr('id');
                  										console
                  												.log("status="
                  														+ status);
                  
                  										$(
                  												'<form action="${pageContext.request.contextPath}/remove" method="POST"/>')
                  												.append(
                  														$('<input type="hidden" name="itemId" value="' + status + '">'))
                  												.appendTo(
                  														$(document.body)) //it has to be added somewhere into the <body>
                  												.submit();
                  
                  										/* $('#'+status).fadeOut('slow', function(c) {
                  											$('.cart-header').remove();
                  										}); */
                  									});
                  				});
               </script>
               <c:set var="total" value="${0}" />
               <c:forEach var="product" items="${sessionScope.productList}"
                  varStatus="loop">
                  <c:set var="total" value="${total + product.defaultPrice}" />
                  <div class="cart-header">
                     <div class="close1" id="${loop.index}"></div>
                     <div class="cart-sec simpleCart_shelfItem">
                        <div class="cart-item cyc">
                           <img
                              src="${pageContext.request.contextPath}/${product.images[0].productImage }"
                              class="img-responsive" alt="" />
                        </div>
                        <div class="cart-item-info">
                           <h3>
                              <a href="#">${product.name}</a>
                           </h3>
                           <ul class="qty">
                              <li>
                                 <p>${product.attributes[0].value}</p>
                              </li>
                           </ul>
                           <div class="delivery">
                              <p>MRP : Rs.${product.defaultPrice}</p>
                              <span>Delivered in 2-3 bussiness days</span>
                              <div class="clearfix"></div>
                           </div>
                        </div>
                        <div class="clearfix"></div>
                     </div>
                  </div>
               </c:forEach>
            </div>
			<c:if test="${total<240}">
			 <c:set var="delCharge" value="${40.00}" /> 
			</c:if>
            
            <div class="col-md-3 cart-total">
               <a class="continue" href="#">Continue to basket</a>
               <div class="price-details">
                  <h3>Price Details</h3>
                  <span>Total</span> <span class="total1" id="productcost">${total}</span> <span>Discount</span>
                  <span class="total1">---</span> <span>Delivery Charges</span> <span
                     class="total1" id="delcharge">${delCharge}</span>
                  <div class="clearfix"></div>
               </div>
               <ul class="total_price">
                  <li class="last_price">
                     <h4>TOTAL</h4>
                  </li>
                  <li class="last_price"><span id="totalamount">${total + delCharge}</span></li>
                  <div class="clearfix"></div>
               </ul>
               <div class="clearfix"></div>
               <a class="order" onclick="openModal()" href="#">Place Order</a>
               <!--<div class="total-item">
                  <h3>OPTIONS</h3>
                  <h4>COUPONS</h4>
                  <a class="cpns" href="#">Apply Coupons</a>
                  <p>
                  	<a href="#">Log In</a> to use accounts - linked coupons
                  </p>
                  </div>-->
            </div>
            <div class="clearfix"></div>
         </div>
      </div>
      <!--//content-->
      <jsp:include page="footer.jsp"></jsp:include>
      <jsp:include page="modal.jsp"></jsp:include>
      <script type="text/javascript">
		$('#checkcoupon').click(function (){
			$('#alert').empty();
			var coupon = $('#coupon').val();
			var url = '${pageContext.request.contextPath}/checkcoupon/'
				+ coupon;
			$.ajax({
				type : "get",
				url : url,
				success : function(data) {
					if(data == null || data==''){
						$('#alert').append('<div class="alert alert-danger">Coupon is invalid</div>');
					}else{
						var amount = $('#totalamount').text();
						var perc = data.typeFigure;
						var percamount = (perc/100)*amount;
						var finalamount = amount-percamount;
						$('#totalamount').html(finalamount);
						$('#alert').append('<div class="alert alert-success">Coupon applied successfully</div>');
					}
				},
				error : function(xhr, ajaxOptions, throwError) {
					console.log("Error = " + throwError.message);
				},
			});
		});
	
	</script>
   </body>
</html>