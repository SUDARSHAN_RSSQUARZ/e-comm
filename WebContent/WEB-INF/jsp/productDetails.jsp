<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>

<title>Product Details</title>
<jsp:include page="websiteScript.jsp"></jsp:include>
<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->

<!-- Custom Theme files -->
<!--theme-style-->

<!--//theme-style-->
<link rel="shortcut icon" type="image/png"
	href="${pageContext.request.contextPath}/resources/images/favicon.png" />
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />


<script type="application/x-javascript">
	
	
	 addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } 


	 
</script>
<!--fonts-->

<link
	href='http://fonts.googleapis.com/css?family=Lato:100,300,400,700,900'
	rel='stylesheet' type='text/css' />

<link
	href='http://fonts.googleapis.com/css?family=Roboto:400,100,300,500,700,900'
	rel='stylesheet' type='text/css' />
<!--//fonts-->
<!-- start menu -->
<link href="${pageContext.request.contextPath}/resources/css/memenu.css"
	rel="stylesheet" type="text/css" media="all" />
<script type="text/javascript"
	src="${pageContext.request.contextPath}/resources/js/memenu.js"></script>
<script>
	$(document).ready(function() {
		$(".memenu").memenu();
	});
</script>
<script
	src="${pageContext.request.contextPath}/resources/js/simpleCart.min.js">
	
</script>
<script
	src="${pageContext.request.contextPath}/resources/js/imagezoom.js"></script>
<script src="${pageContext.request.contextPath}/resources/js/cookie.js"></script>
<!--Start of Zopim Live Chat Script-->
<script type="text/javascript">
window.$zopim||(function(d,s){var z=$zopim=function(c){z._.push(c)},$=z.s=
	d.createElement(s),e=d.getElementsByTagName(s)[0];z.set=function(o){z.set.
	_.push(o)};z._=[];z.set._=[];$.async=!0;$.setAttribute("charset","utf-8");
	$.src="//v2.zopim.com/?3a0P6Plo0iOMK5TF0Eg8CqvSBOR4WnKY";z.t=+new Date;$.
	type="text/javascript";e.parentNode.insertBefore($,e)})(document,"script");

	function getUpdatedPrice(el) {
		var selectedAttribute = $(el).val();
		var url = '${pageContext.request.contextPath}/getPrice?attributeId='
				+ selectedAttribute;
		$.ajax({
			type : "get",
			url : url,
			success : function(data) {
				console.log(data);
				if (data != 0) {
					$("#defaultPrice").val(data);
					$("#weight").val($("#drop option:selected").text().trim());
					$('#item_price').text('Rs. '+data);
				}
			},
			error : function(xhr, ajaxOptions, throwError) {
				console.log("Error = " + throwError.message);
			},
		});
	}
</script>
</head>
<body>

	<!--header-->
	<jsp:include page="header.jsp"></jsp:include>

	<!-- grow -->
	<div class="grow">
		<div class="container">
			<h2>Cake</h2>
		</div>
	</div>
	<!-- grow -->
	<div class="product">
		<div class="container">

			<c:if test="${not empty status}">
				<div class="alert alert-success">
					<strong>Success!</strong> ${status }
				</div>
			</c:if>
			<div id="alert">
				
			</div>
			<div class="product-price1">
				<div class="top-sing">
					<div class="col-md-12 register-top-grid">
						<div class="col-md-8 col-sm-10">
							<input type="text" name="pincode" placeholder="Check Pincode" id="pincode" style="height:40px;">
						</div>
						<div class="col-md-2 col-sm-2" style="padding-top:15px;">	
							<input type="submit" value="Check" id="checkpincode">
						</div>	
					</div>
					<br>
					<br>
					<form:form method="POST"
						action="${pageContext.request.contextPath}/addToBasket">

						<div class="col-md-7 single-top">
							<div class="flexslider">
								<ul class="slides">

									<c:forEach var="image" items="${product.images}"
										varStatus="loop">
										<input type="hidden" name="images[${loop.index}].productImage"
											value="${image.getProductImage()}" />

										<li
											data-thumb="${pageContext.request.contextPath}/${image.getProductImage()}">
											<div class="thumb-image">
												<img
													src="${pageContext.request.contextPath}/${image.getProductImage()}"
													data-imagezoom="true" class="img-responsive">
											</div>
										</li>
									</c:forEach>
								</ul>
							</div>

							<div class="clearfix"></div>
							<!-- slide -->


							<!-- FlexSlider -->
							<script defer
								src="${pageContext.request.contextPath}/resources/js/jquery.flexslider.js"></script>
							<link rel="stylesheet"
								href="${pageContext.request.contextPath}/resources/css/flexslider.css"
								type="text/css" media="screen" />

							<script>
								// Can also be used with $(document).ready()
								$(window).load(function() {
									$('.flexslider').flexslider({
										animation : "slide",
										controlNav : "thumbnails"
									});
								});
							</script>

						</div>
						<div class="col-md-5 single-top-in simpleCart_shelfItem">

							<input type="hidden" name="id" value="${product.id }" /> <input
								type="hidden" name="name" value="${product.name }" /> <input
								type="hidden" name="defaultPrice" id="defaultPrice"
								value="${product.defaultPrice }" />
								<input id="weight" value="${product.attributes[0].value}"
								type="hidden" name="attributes[0].value" /> 
							<div class="single-para ">
								<h4>${product.name}</h4>
								<div class="star-on">

									<div class="clearfix"></div>
								</div>

								<h5 class="item_price" id="item_price" style="color:#AA00FF;">Rs. ${product.defaultPrice}</h5>
								<div class="available">
									<ul>
										<c:forEach var="attribute" items="${product.mapOfAttributes}"
											varStatus="loop">
											<li class="size-in">${attribute.key}<select id="drop"
												name="attributes[${loop.index}].id"
												onchange="getUpdatedPrice(this)">
													<c:forEach var="values" items="${attribute.value}">
														<option value="${values.id }">${values.value}</option>
													</c:forEach>
											</select>
											</li>
											<div class="clearfix"></div>
										</c:forEach>
									</ul>
								</div>
								<!-- <input type="button" value="ADD TO CART" onclick="addToCart()"> -->
								<input type="submit" value="ADD TO CART" onclick="">
							</div>
					</form:form>

				</div>
				<div class="clearfix"></div>
			</div>
			<!---->


		<div class="clearfix"></div>
	</div>
	</div>
	<!--//content-->
	<jsp:include page="footer.jsp"></jsp:include>
	<jsp:include page="modal.jsp"></jsp:include>
	<script type="text/javascript">
		$('#checkpincode').click(function (){
			$('#alert').empty();
			var pincode = $('#pincode').val();
			var url = '${pageContext.request.contextPath}/checkpincode/'
				+ pincode;
			$.ajax({
				type : "get",
				url : url,
				success : function(data) {
					if(data === 'success'){
						$('#alert').append('<div class="alert alert-success">Delivery option is available</div>');					
					}else{
						$('#alert').append('<div class="alert alert-danger">Delivery option is not available</div>');
					}
				},
				error : function(xhr, ajaxOptions, throwError) {
					console.log("Error = " + throwError.message);
				},
			});
		});
	
	</script>
</body>
</html>
