<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Product</title>
<!-- Bootstrap core CSS -->
<jsp:include page="adminCssInclude.jsp"></jsp:include>
</head>
<body class="nav-md">
	<div class="container body">
		<div class="main_container">

			<jsp:include page="navigationbar.jsp"></jsp:include>

			<!-- page content -->
			<div class="right_col" role="main">
				<div class="">
					<div class="page-title">
						<div class="title_left">
							<h3>Form Elements</h3>
						</div>
						<div class="title_right">
							<div
								class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
								<div class="input-group">
									<input type="text" class="form-control"
										placeholder="Search for..."> <span
										class="input-group-btn">
										<button class="btn btn-default" type="button">Go!</button>
									</span>
								</div>
							</div>
						</div>
					</div>
					<div class="clearfix"></div>
					<div class="row">
						<div class="col-md-12 col-sm-12 col-xs-12">
							<div class="x_panel">
								<div class="x_title">
									<h2>
										<small>Select Attributes</small>
									</h2>
									<div class="clearfix"></div>
								</div>
								<div class="x_content">
									<br />
									<form:form method="POST" enctype="multipart/form-data"
										action="${pageContext.request.contextPath}/prepareAddProductDetails1"
										class="form-horizontal form-label-left">
										<div class="row">
											<div class="col-md-12 col-sm-12 col-xs-12">
												<div class="x_panel">

													<div class="x_panel">

														<div class="col-md-12 col-sm-12 col-xs-12">
															<div>
																<table  style=" ">
																	<c:forEach var="attr" items="${attributeList}"
																		varStatus="status">
																		<tr>
																			
																		</tr>
																		<td><input  name="selectedAttributes[${status.index}].id"
																			value="${attr.id }" placeholder="Id" /></td>
																		<td><input  name="selectedAttributes[${status.index}].attributeName"
																			value="${attr.attributeName }" placeholder="Name" /></td>
																		<td><input type="checkbox"
																			name="selectedAttributes[${status.index}].checkStatus" placeholder="Select" /></td>
																	</c:forEach>
																</table>
															</div>
														</div>
													</div>

												</div>
											</div>
										</div>
										<div class="form-group">
											<div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
												<button type="submit" class="btn btn-success">Next</button>
											</div>
										</div>
									</form:form>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>


		</div>
	</div>
	<div id="custom_notifications" class="custom-notifications dsp_none">
		<ul class="list-unstyled notifications clearfix"
			data-tabbed_notifications="notif-group">
		</ul>
		<div class="clearfix"></div>
		<div id="notif-group" class="tabbed_notifications"></div>
	</div>
	<jsp:include page="adminScriptInclude.jsp"></jsp:include>
	<script>
		autosize($('.resizable_textarea'));
	</script>
	<script type="text/javascript">
		$(function() {
			'use strict';
			var countriesArray = $.map(countries, function(value, key) {
				return {
					value : value,
					data : key
				};
			});
			$('#autocomplete-custom-append').autocomplete({
				lookup : countriesArray,
				appendTo : '#autocomplete-container'
			});
		});
	</script>
	<script src="${pageContext.request.contextPath}/resources/js/custom.js"></script>

</body>
</html>