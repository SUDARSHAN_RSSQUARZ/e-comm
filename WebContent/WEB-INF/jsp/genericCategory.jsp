<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<!-- Meta, title, CSS, favicons, etc. -->
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Category</title>
<!-- Bootstrap core CSS -->
<jsp:include page="adminCssInclude.jsp"></jsp:include>
</head>
<body class="nav-md">
	<div class="container body">
		<div class="main_container">

			<jsp:include page="navigationbar.jsp"></jsp:include>

			<!-- page content -->
			<div class="right_col" role="main">
				<div class="">
					<div class="page-title">
						<div class="title_left">
							<h3>Category Management</h3>
						</div>

					</div>
					<div class="clearfix"></div>
					<div class="row">
						<div class="col-md-6 col-sm-12 col-xs-12">
							<div class="x_panel">
								<c:if test="${not empty errorstatus}">
									<div class="alert alert-danger">
										<strong></strong> ${errorstatus }
									</div>
								</c:if>
								<c:if test="${not empty status}">
									<div class="alert alert-success">
										<strong></strong> ${status }
									</div>
								</c:if>
								<br>
									<form:form method="POST" enctype="multipart/form-data"
										action="${pageContext.request.contextPath}/addCategory"
										class="form-horizontal form-label-left">
										<div class="form-group">
											<label class="control-label col-md-3 col-sm-3 col-xs-12"
												for="first-name">Add Category <span class="required">*</span>
											</label>
											<div class="col-md-6 col-sm-6 col-xs-12">
												<input type="text" required="required"
													placeholder="Category Name" name="name"
													class="form-control col-md-7 col-xs-12">
											</div>
										</div>
										<div class="form-group">
											<div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
												<button type="submit" class="btn btn-success">Submit</button>
											</div>
										</div>
									</form:form>
								</div>
							</div>
						
						
						<div class="col-md-6 col-sm-12 col-xs-12">
                            <div class="x_panel">
                                <div class="x_title">
                                    <h2>Enter sub-category into existing category</h2>
                                   
                                    <div class="clearfix"></div>
                                </div>
                                <div class="x_content">
                                    <br />
                                    
									<form class="form-horizontal form-label-left" action="${pageContext.request.contextPath}/addSubCategory" method="POST">
										<div class="form-group">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12">Categories</label>
                                            <div class="col-md-6 col-sm-9 col-xs-12">
                                                <select class="form-control" id="categoryId">
                                                	<option value="">--Select Category--</option>
                                                    <c:forEach var="category" items="${categoryMap}">
														<option>${category.key}</option>
													</c:forEach>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                           <div class="form-group">
												<label class="control-label col-md-3 col-sm-3 col-xs-12">Nodes</label>
                                            	<div class="col-md-6 col-sm-9 col-xs-12">
                                                 <select name="parent" class="form-control" id="subcatId">
                                                    
                                                </select> 
                                            </div>                                              
                                        	</div>
                                        </div>
                                        
                                        <div class="form-group">
											<label class="control-label col-md-3 col-sm-3 col-xs-12"
												for="first-name">Add Subcategory <span class="required"></span>
											</label>
											<div class="col-md-6 col-sm-6 col-xs-12">
												<input type="text" required="required"
													placeholder="Sub-Category Name" name="name"
													class="form-control col-md-7 col-xs-12">
											</div>
										</div>
                                        
                                        <div class="ln_solid"></div>
                                        <div class="form-group">
                                            <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                                                <button type="submit" class="btn btn-success">Submit</button>
                                            </div>
                                        </div>

                                    </form>
                                </div>
                            </div>
                        </div>
						</div>
					</div>

				</div>
			</div>
		</div>
	<div id="custom_notifications" class="custom-notifications dsp_none">
		<ul class="list-unstyled notifications clearfix"
			data-tabbed_notifications="notif-group">
		</ul>
		<div class="clearfix"></div>
		<div id="notif-group" class="tabbed_notifications"></div>
	</div>
	<jsp:include page="adminScriptInclude.jsp"></jsp:include>
	<script type="text/javascript">
         $(document).ready(function () {
			 var map = ${categoryMapString};
			 $('#categoryId').change(function(){
				 //Assign value to subcategory
				 var cat = $('#categoryId').val();
				 console.log(map[cat]);
				 $('#subcatId')
					.html($("<option>Choose</option>"));
				 $.each(map[cat], function(key, value) {   
					$('#subcatId')
					.append($("<option>--Select Node--</option>")
					.attr("value",value)
					.text(value)); 
				});
				
				 
			 });
         });
     </script>    
</body>
</html>