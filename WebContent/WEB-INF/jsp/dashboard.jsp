<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html lang="en">
   <head>
      <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
      <!-- Meta, title, CSS, favicons, etc. -->
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <title>Product </title>
      <!-- Bootstrap core CSS -->
      <jsp:include page="adminCssInclude.jsp"></jsp:include>
   </head>
   <body class="nav-md">
      <div class="container body">
         <div class="main_container">
            
            <jsp:include page="navigationbar.jsp"></jsp:include>
            
            <!-- page content -->
            <div class="right_col" role="main">
               <div class="">
                  <div class="page-title">
                     <div class="title_left">
                        <h3>Product Elements</h3>
                     </div>
                     <div class="title_right">
                        <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                           <div class="input-group">
                              <input type="text" class="form-control" placeholder="Search for...">
                              <span class="input-group-btn">
                              <button class="btn btn-default" type="button">Go!</button>
                              </span>
                           </div>
                        </div>
                     </div>
                  </div>
                  <div class="clearfix"></div>
                  <div class="row">
                     <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="x_panel">
                           <div class="x_title">
                              <h2>Product</small></h2>
                              <div class="clearfix"></div>
                           </div>
                           <div class="x_content">
                              <br />
                              <form:form method="POST" enctype="multipart/form-data" action="${pageContext.request.contextPath}/product"  class="form-horizontal form-label-left">
                                 <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Product Name <span class="required">*</span>
                                    </label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                       <input type="text" id="first-name" required="required" placeholder="Product Name" name="name" class="form-control col-md-7 col-xs-12">
                                    </div>
                                 </div>
                                
                                 <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12">Category</label>
                                    <div class="col-md-6 col-sm-9 col-xs-12">
									 <select id="categoryId" name="category" class="form-control">
									<option>Choose Category</option>
                                           <c:forEach var="entry" items="${topNav}">
												<option>${entry.key}</option>
										   </c:forEach>
                                         
                                       </select>
                                    </div>
                                 </div>
                                 <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12">Subcategory</label>
                                    <div class="col-md-6 col-sm-9 col-xs-12">
										
                                       <select id="subcategoryId" name="subCategory" class="form-control">
									     
                                       </select>
                                    </div>
                                 </div>
                                 <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Product Price <span class="required">*</span>
                                    </label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                       <input name="defaultPrice" type="text"   required="required" placeholder="Product Price" class="form-control col-md-7 col-xs-12">
                                    </div>
                                 </div>
                                <div class="form-group">
                                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Type<span class="required">*</span>
                                                </label>
                                                <div class="col-md-6 col-sm-6 col-xs-12">
                                                   <div class="radio">
                                                      <label>
                                                      <input value="featured" name="type" type="radio" class="flat"> Featured &nbsp; &nbsp;
                                                      <input value="new" name="type" type="radio" class="flat"> New
                                                      </label>
                                                   </div>
                                                </div>
                                             </div>
                                 <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Product Image <span class="required">*</span>
                                    </label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                       <input type="file" name="file"   required="required" placeholder="Delivery Option" class="form-control col-md-7 col-xs-12">
                                    </div>
                                 </div>
								 <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Product Image <span class="required">*</span>
                                    </label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                       <input type="file" name="file"  placeholder="Delivery Option" class="form-control col-md-7 col-xs-12">
                                    </div>
                                 </div>
                                 <div class="ln_solid"></div>
                                 <div class="row">
                                    <div class="col-md-12 col-sm-12 col-xs-12">
                                       <div class="x_panel">
                                          <div class="x_title">
                                             <h2>Product Properties</small></h2>
                                             <div class="clearfix"></div>
                                          </div>
                                          <div class="x_content">
                                             <br />
                                             <div class="col-md-6">
                                             <div class="form-group">
                                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Available Weights <span class="required">*</span>
                                                </label>
                                                <div class="col-md-6 col-sm-6 col-xs-12">
                                                      <div class="col-md-2 col-sm-2 col-xs-2">
                                                         <input type="button" id="wtbtn" class="btn btn-success" value="+1">
                                                      </div>
                                                </div>
												<div class="col-md-6 col-sm-6 col-xs-12" id="cakeweight">
                                                   
                                                </div>
                                             </div>
                                             </div>
                                             <div class="col-md-6">
                                             <div class="form-group">
                                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Available Colors <span class="required">*</span>
                                                </label>
                                                <div class="col-md-6 col-sm-6 col-xs-12">
                                                      <div class="col-md-2 col-sm-2 col-xs-2">
                                                         <input type="button" id="clrbtn" class="btn btn-success" value="+1">
                                                      </div>
                                                </div>
												<div class="col-md-6 col-sm-6 col-xs-12" id="cakecolor">
                                                   
                                                </div>
                                             </div>
                                             </div>
                                             <!--<div class="form-group">
                                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Available Colors <span class="required">*</span>
                                                </label>
                                                <div class="col-md-6 col-sm-6 col-xs-12">
                                                   <label>
                                                      <div class="col-md-4 col-sm-4 col-xs-4" id="cakecolor">
														
                                                      </div>
                                                      <div class="col-md-2 col-sm-2 col-xs-2">
                                                         <input type="button" id="clrbtn" class="btn btn-success" value="+1"/>
                                                      </div>
                                                   </label>
                                                </div>
                                             </div>-->
                                             <div class="form-group">
                                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Available Form <span class="required">*</span>
                                                </label>
                                                <div class="col-md-6 col-sm-6 col-xs-12">
                                                   <div class="checkbox">
                                                      <label>
                                                      <input type="checkbox" class="flat"> Egg &nbsp; &nbsp;
                                                      <input type="checkbox" class="flat"> Eggless
                                                      </label>
                                                   </div>
                                                </div>
                                                <div class="col-md-3 col-sm-3 col-xs-3">
                                              		<input type="button" id="showgrid" class="btn btn-success" value="Show Grid">
                                          		</div>
                                             </div>
                                             
                                             <div class="ln_solid"></div>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                                 <div class="row">
                        			<div class="col-md-12 col-sm-12 col-xs-12">
                            			<div class="x_panel">
                                			<div class="x_title">
                                    			<div class="clearfix"></div>
                                			</div>
                                			<div class="x_content">
                                    			<table id="example" class="table table-striped responsive-utilities jambo_table">
                                        			<tbody>
                                            			<tr class="<c:choose><c:when test='${loop.index%2 == 0}'>	even</c:when><c:otherwise>odd</c:otherwise></c:choose> pointer">
                                                			<td class=" " id="weigh"></td>
                                                			<td class=" " id="colo"></td>
                                                			<td class=" " id="pri"></td>
                                                			<td class=" " id="quantity"></td>
                                            			</tr>
                                       				</tbody>
                                    			</table>
                                			</div>
                            			</div>
                        			</div>
                    			</div>
                                 <div class="form-group">
                                    <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                                       <button type="submit" class="btn btn-success">Submit</button>
                                    </div>
                                 </div>
                              </form:form>
                           </div>
                        </div>
                     </div>
                  </div>
                  
                  <script type="text/javascript">
                     $(document).ready(function () {
                         $('#birthday').daterangepicker({
                             singleDatePicker: true,
                             calender_style: "picker_4"
                         }, function (start, end, label) {
                             console.log(start.toISOString(), end.toISOString(), label);
                         });
                     });
                  </script>
               </div>
               <!-- /page content -->
            </div>
         </div>
      </div>
      <div id="custom_notifications" class="custom-notifications dsp_none">
         <ul class="list-unstyled notifications clearfix" data-tabbed_notifications="notif-group">
         </ul>
         <div class="clearfix"></div>
         <div id="notif-group" class="tabbed_notifications"></div>
      </div>
      <jsp:include page="adminScriptInclude.jsp"></jsp:include>
      
      <script>
         autosize($('.resizable_textarea'));
      </script>
      <!-- Autocomplete -->
      
      <script type="text/javascript">
         $(function () {
             'use strict';
             var countriesArray = $.map(countries, function (value, key) {
                 return {
                     value: value,
                     data: key
                 };
             });
             // Initialize autocomplete with custom appendTo:
             $('#autocomplete-custom-append').autocomplete({
                 lookup: countriesArray,
                 appendTo: '#autocomplete-container'
             });
         });
      </script>
      <script src="${pageContext.request.contextPath}/resources/js/custom.js"></script>
      <!-- select2 -->
      <script>
	  var cntAttribute = -1;
	  var weightarr = [];
	  var colorarr = [];
         $(document).ready(function () {
			 
			 loadAttributes();
             $(".select2_single").select2({
                 placeholder: "Select a state",
                 allowClear: true
             });
             $(".select2_group").select2({});
             $(".select2_multiple").select2({
                 maximumSelectionLength: 4,
                 placeholder: "With Max Selection limit 4",
                 allowClear: true
             });
         
         	$("#clrbtn").click(addAttrColor);
         	$("#wtbtn").click(addAttrWeight);
         
         	var rcount = 0;
         	$("#showgrid").click(function(){
         		$.each(weightarr, function( windex, wvalue ) {
         			$.each(colorarr, function( cindex, cvalue ) {
           			  	$('#weigh').append('<input type="text" name="productPrice['+rcount+'].weight" value="'+wvalue+'" class="form-control col-md-7 col-xs-12" readonly="readonly">');
           				$('#colo').append('<input type="text" name="productPrice['+rcount+'].color" value="'+cvalue+'" class="form-control col-md-7 col-xs-12" readonly="readonly">');
           				$('#pri').append('<input type="text" name="productPrice['+rcount+'].price" class="form-control col-md-7 col-xs-12" placeholder="Price">');
           				$('#quantity').append('<input type="text" name="productPrice['+rcount+'].quantity" class="form-control col-md-7 col-xs-12" placeholder="Available Quantity">');
           				rcount++;
           			});
         		});
         	});
         
         });
		 
         function addintoweight(weight){
        	 weightarr.push(weight);
         };
         
         function addintocolor(color){
        	 colorarr.push(color);
         };
         
		 function loadAttributes(){
			 addAttrColor();
			 addAttrWeight();			 
		 };
		 
		 function addAttrColor(){
			 cntAttribute++;
         $("#cakecolor").append("<div><input name='attributes["+cntAttribute+"].type' type='hidden' value='COLOR'/><input name='attributes["+cntAttribute+"].value' type='text' onchange='addintocolor(this.value)' required='required' placeholder='Colour' class='form-control'></div><br>");
		 };
		 
		 function addAttrWeight(){
			 cntAttribute++;
         $("#cakeweight").append("<div> <input name='attributes["+cntAttribute+"].type' type='hidden' value='WEIGHT'/><input name='attributes["+cntAttribute+"].value' type='text' onchange='addintoweight(this.value)' required='required' placeholder='Weight' class='form-control'/></div><br>");
		 };
      </script>
      <!-- /select2 -->
      <!-- input tags -->
      <script>
         function onAddTag(tag) {
             alert("Added a tag: " + tag);
         }
         
         function onRemoveTag(tag) {
             alert("Removed a tag: " + tag);
         }
         
         function onChangeTag(input, tag) {
             alert("Changed a tag: " + tag);
         }
         
         $(function () {
             $('#tags_1').tagsInput({
                 width: 'auto'
             });
         });
      </script>
      <!-- /input tags -->
      <!-- form validation -->
      <script type="text/javascript">
         /*   $(document).ready(function () {
                $.listen('parsley:field:validate', function () {
                    validateFront();
                });
                $('#demo-form .btn').on('click', function () {
                    $('#demo-form').parsley().validate();
                    validateFront();
                });
               var validateFront = function () {
                    if (true === $('#demo-form').parsley().isValid()) {
                        $('.bs-callout-info').removeClass('hidden');
                        $('.bs-callout-warning').addClass('hidden');
                    } else {
                        $('.bs-callout-info').addClass('hidden');
                        $('.bs-callout-warning').removeClass('hidden');
                    }
         };
            });
         
            $(document).ready(function () {
                $.listen('parsley:field:validate', function () {
                    validateFront();
                });
                $('#demo-form2 .btn').on('click', function () {
                    $('#demo-form2').parsley().validate();
                    validateFront();
                });
                var validateFront = function () {
                    if (true === $('#demo-form2').parsley().isValid()) {
                        $('.bs-callout-info').removeClass('hidden');
                        $('.bs-callout-warning').addClass('hidden');
                    } else {
                        $('.bs-callout-info').addClass('hidden');
                        $('.bs-callout-warning').removeClass('hidden');
                    }
                };
            });
            try {
                hljs.initHighlightingOnLoad();
         } catch (err) {} */
      </script>
      <!-- /form validation -->
      <!-- editor -->
      <script type="text/javascript">
         $(document).ready(function () {
             $('.xcxc').click(function () {
                 $('#descr').val($('#editor').html());
             });
			 var map = ${topNavString};
			  console.log(map);
			 $('#categoryId').change(function(){
				 //Assign value to subcategory
				 var cat = $('#categoryId').val();
				 console.log(map[cat]);
				 $('#subcategoryId')
					.html($("<option>Choose</option>"));
				 $.each(map[cat], function(key, value) {   
					$('#subcategoryId')
					.append($("<option></option>")
					.attr("value",value)
					.text(value)); 
				});
				
				 
			 });
         });
         
         $(function () {
             function initToolbarBootstrapBindings() {
                 var fonts = ['Serif', 'Sans', 'Arial', 'Arial Black', 'Courier',
             'Courier New', 'Comic Sans MS', 'Helvetica', 'Impact', 'Lucida Grande', 'Lucida Sans', 'Tahoma', 'Times',
             'Times New Roman', 'Verdana'],
                     fontTarget = $('[title=Font]').siblings('.dropdown-menu');
                 $.each(fonts, function (idx, fontName) {
                     fontTarget.append($('<li><a data-edit="fontName ' + fontName + '" style="font-family:\'' + fontName + '\'">' + fontName + '</a></li>'));
                 });
                 $('a[title]').tooltip({
                     container: 'body'
                 });
                 $('.dropdown-menu input').click(function () {
                         return false;
                     })
                     .change(function () {
                         $(this).parent('.dropdown-menu').siblings('.dropdown-toggle').dropdown('toggle');
                     })
                     .keydown('esc', function () {
                         this.value = '';
                         $(this).change();
                     });
         
                 $('[data-role=magic-overlay]').each(function () {
                     var overlay = $(this),
                         target = $(overlay.data('target'));
                     overlay.css('opacity', 0).css('position', 'absolute').offset(target.offset()).width(target.outerWidth()).height(target.outerHeight());
                 });
                 if ("onwebkitspeechchange" in document.createElement("input")) {
                     var editorOffset = $('#editor').offset();
                     $('#voiceBtn').css('position', 'absolute').offset({
                         top: editorOffset.top,
                         left: editorOffset.left + $('#editor').innerWidth() - 35
                     });
                 } else {
                     $('#voiceBtn').hide();
                 }
             };
         
             function showErrorAlert(reason, detail) {
                 var msg = '';
                 if (reason === 'unsupported-file-type') {
                     msg = "Unsupported format " + detail;
                 } else {
                     console.log("error uploading file", reason, detail);
                 }
                 $('<div class="alert"> <button type="button" class="close" data-dismiss="alert">&times;</button>' +
                     '<strong>File upload error</strong> ' + msg + ' </div>').prependTo('#alerts');
             };
             initToolbarBootstrapBindings();
             $('#editor').wysiwyg({
                 fileUploadError: showErrorAlert
             });
             window.prettyPrint && prettyPrint();
         });
      </script>
   </body>
</html>