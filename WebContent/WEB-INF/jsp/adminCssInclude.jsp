<link href="${pageContext.request.contextPath}/resources/css/bootstrap.min.css" rel="stylesheet">

    <link href="${pageContext.request.contextPath}/resources/fonts/css/font-awesome.min.css" rel="stylesheet">
    <link href="${pageContext.request.contextPath}/resources/css/animate.min.css" rel="stylesheet">

    <!-- Custom styling plus plugins -->
    <link href="${pageContext.request.contextPath}/resources/css/custom.css" rel="stylesheet">
    <link href="${pageContext.request.contextPath}/resources/css/icheck/flat/green.css" rel="stylesheet">
    <!-- editor -->
    <link href="http://netdna.bootstrapcdn.com/font-awesome/3.0.2/css/font-awesome.css" rel="stylesheet">
    <link href="${pageContext.request.contextPath}/resources/css/editor/external/google-code-prettify/prettify.css" rel="stylesheet">
    <link href="${pageContext.request.contextPath}/resources/css/editor/index.css" rel="stylesheet">
    <!-- select2 -->
    <link href="${pageContext.request.contextPath}/resources/css/select/select2.min.css" rel="stylesheet">
    <!-- switchery -->
    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/switchery/switchery.min.css" />
	<script src="${pageContext.request.contextPath}/resources/js/jquery.min.js"></script>