<link
         href="${pageContext.request.contextPath}/resources/css/bootstrap.css"
         rel="stylesheet" type="text/css" media="all" />
      <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
      <script
         src="${pageContext.request.contextPath}/resources/js/jquery.min.js"></script>
      <script src="${pageContext.request.contextPath}/resources/js/bootstrap.min.js"></script>
	  <script src="${pageContext.request.contextPath}/resources/js/mask.js"></script>
      <script src="${pageContext.request.contextPath}/resources/js/main.js"></script>
      <script src="${pageContext.request.contextPath}/resources/js/parallax.min.js"></script>		
      <!-- Custom Theme files -->
      <!--theme-style-->
      <link href="${pageContext.request.contextPath}/resources/css/style.css"
         rel="stylesheet" type="text/css" media="all" />
      <link href="${pageContext.request.contextPath}/resources/css/animate.css" rel="stylesheet" type="text/css" media="all" />   
      <link href="${pageContext.request.contextPath}/resources/css/memenu.css" rel="stylesheet" type="text/css" media="all" />
		<script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/memenu.js"></script>
		<script>$(document).ready(function(){$(".memenu").memenu();});</script>
		<script src="${pageContext.request.contextPath}/resources/js/simpleCart.min.js"> </script>
		<script src="${pageContext.request.contextPath}/resources/js/cookie.js"></script>   
		<link href='http://fonts.googleapis.com/css?family=Lato:100,300,400,700,900' rel='stylesheet' type='text/css'>
		<link href='http://fonts.googleapis.com/css?family=Roboto:400,100,300,500,700,900' rel='stylesheet' type='text/css'><!--//fonts-->