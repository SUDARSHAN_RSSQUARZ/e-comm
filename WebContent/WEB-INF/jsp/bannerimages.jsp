<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html lang="en">
   <head>
      <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
      <!-- Meta, title, CSS, favicons, etc. -->
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <title>Banner </title>
      <!-- Bootstrap core CSS -->
      <jsp:include page="adminCssInclude.jsp"></jsp:include>
   </head>
   <body class="nav-md">
      <div class="container body">
         <div class="main_container">
            
            <jsp:include page="navigationbar.jsp"></jsp:include>
            <!-- page content -->
            <div class="right_col" role="main">
               <div class="">
                  <div class="page-title">
                     <div class="title_left">
                        <h3>Pincode Elements</h3>
                     </div>
                     <div class="title_right">
                        <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                           <div class="input-group">
                              <input type="text" class="form-control" placeholder="Search for...">
                              <span class="input-group-btn">
                              <button class="btn btn-default" type="button">Go!</button>
                              </span>
                           </div>
                        </div>
                     </div>
                  </div>
					<div class="clearfix"></div>
                  <div class="row">
                     <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="x_panel">
                           <div class="x_title">
                              <h2></small></h2>
                              <div class="clearfix"></div>
                           </div>
                           <div class="x_content">
									<c:if test="${not empty status}">
										<div class="alert alert-success">
											<strong></strong> ${status }
										</div>
									</c:if>
									<c:if test="${not empty errorstatus}">
										<div class="alert alert-danger">
											<strong></strong> ${errorstatus }
										</div>
									</c:if>
									<br />
                              <form:form method="POST" enctype="multipart/form-data" action="${pageContext.request.contextPath}/banner"  class="form-horizontal form-label-left">
                                 <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Banner Text <span class="required">*</span>
                                    </label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                       <input type="text" id="first-name" required="required" placeholder="Banner Text" name="bannerText" class="form-control col-md-7 col-xs-12">
                                    </div>
                                 </div>
                                
                                 <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name"> Banner Image <span class="required">*</span>
                                    </label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                       <input type="file" name="file" required="required" class="form-control col-md-7 col-xs-12">
                                    </div>
                                 </div>
                                 <div class="ln_solid"></div>
                                 <div class="form-group">
                                    <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                                       <button type="submit" class="btn btn-success">Submit</button>
                                    </div>
                                 </div>
                              </form:form>
                           </div>
                        </div>
                     </div>
                  </div>
                 <div class="row">
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="x_panel">
                                <div class="x_title">
                                    <h2>Banners</h2>
                                    <div class="clearfix"></div>
                                </div>
                                <div class="x_content">
                                    <table id="example" class="table table-striped responsive-utilities jambo_table">
                                        <thead>
                                            <tr class="headings">
                                                <th>
                                                    <input type="checkbox" class="flat">
                                                </th>
												<th>Id </th>
                                                <th>Banner Text </th>
                                                <th>Image </th>
                                            </tr>
                                        </thead>

                                        <tbody>
										<c:forEach var="banner" items="${bannerList}" varStatus="loop">
										
                                            <tr class="<c:choose><c:when test='${loop.index%2 == 0}'>	even</c:when><c:otherwise>odd</c:otherwise></c:choose> pointer">
                                                <td class="a-center ">
                                                    <input type="checkbox" class="flat">
                                                </td>
                                                <td class=" ">${banner.id}</td>
                                                <td class=" ">${banner.bannerText}</td>
                                                <td class=" "><img alt="" src="${pageContext.request.contextPath}${banner.bannerImage}" 
                                                					width="60px" height="60px" class="img-responsive"></td>
                                            </tr>
											</c:forEach>
                                            </tbody>

                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
               </div>
               <!-- /page content -->
               
            </div>
         </div>
      </div>
      <div id="custom_notifications" class="custom-notifications dsp_none">
         <ul class="list-unstyled notifications clearfix" data-tabbed_notifications="notif-group">
         </ul>
         <div class="clearfix"></div>
         <div id="notif-group" class="tabbed_notifications"></div>
      </div>
      <jsp:include page="adminScriptInclude.jsp"></jsp:include>
      
      <script>
         autosize($('.resizable_textarea'));
      </script>
      <!-- Autocomplete -->
      
      <script src="${pageContext.request.contextPath}/resources/js/custom.js"></script>
      <!-- select2 -->
      
      <!-- /input tags -->
      <!-- form validation -->
      
      <!-- /form validation -->
      <!-- editor -->
      
   </body>
</html>