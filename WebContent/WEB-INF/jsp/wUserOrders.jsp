<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<!DOCTYPE html>
<html>
   <head>
      <title>Checkout</title>
      <jsp:include page="websiteScript.jsp"></jsp:include>
      <!--//theme-style-->
      <link rel="shortcut icon" type="image/png"
         href="${pageContext.request.contextPath}/resources/images/favicon.png" />
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
      <meta name="keywords" content="Honey Buzz" />
      <script type="application/x-javascript">
         addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } 
         
         
      </script>
      <!--fonts-->
      <link
         href='http://fonts.googleapis.com/css?family=Lato:100,300,400,700,900'
         rel='stylesheet' type='text/css'>
      <link
         href='http://fonts.googleapis.com/css?family=Roboto:400,100,300,500,700,900'
         rel='stylesheet' type='text/css'>
      <!--//fonts-->
      <!-- start menu -->
      <link href="${pageContext.request.contextPath}/resources/css/memenu.css"
         rel="stylesheet" type="text/css" media="all" />
      <script type="text/javascript"
         src="${pageContext.request.contextPath}/resources/js/memenu.js"></script>
      <script>
         $(document).ready(function() {
         	/*if($('#productcost').text()<240){
				$('#delcharge').text('40');
         	}*/
         	$('#placeOrder').click(placeOrder);
         	//Validations
         	$('#InputMobile').mask('9999999999');
			$('#InputMobile,#InputEmail,#InputName,#InputPin,#InputAddress').prop('required',true);
         });
         function isEmail(email) {
          var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
          return regex.test(email);
         }
      </script>
      <script
         src="${pageContext.request.contextPath}/resources/js/simpleCart.min.js"></script>
      <script
         src="${pageContext.request.contextPath}/resources/js/imagezoom.js"></script>
      <script src="${pageContext.request.contextPath}/resources/js/cookie.js"></script>
      <!--Start of Zopim Live Chat Script-->
      <script type="text/javascript">
      <!--Start of Zopim Live Chat Script-->
      window.$zopim||(function(d,s){var z=$zopim=function(c){z._.push(c)},$=z.s=
    	  d.createElement(s),e=d.getElementsByTagName(s)[0];z.set=function(o){z.set.
    	  _.push(o)};z._=[];z.set._=[];$.async=!0;$.setAttribute("charset","utf-8");
    	  $.src="//v2.zopim.com/?3a0P6Plo0iOMK5TF0Eg8CqvSBOR4WnKY";z.t=+new Date;$.
    	  type="text/javascript";e.parentNode.insertBefore($,e)})(document,"script");

         
         function placeOrder(){
         	
         	
         	var name = $("#InputName").val().trim();
         	var email = $("#InputEmail").val().trim();
         	var mobile = $("#InputMobile").val().trim();
         	var atposition=email.indexOf("@");  
         	var dotposition=email.lastIndexOf("."); 
         	
         	if (name==null || name=="" || name.length>20){  
         		alert("Name must be maximum 20 character long");
         		return false;
         	}
         	
         	if(!isEmail(email)){
         		alert("Please enter a valid e-mail address");  
         		  return false;  
         	}
         		
         		$('#Mymodel').modal('hide');
         		$.ajax({
                        url: "${pageContext.request.contextPath}/checkout",
         			headers: { 
                				'Accept': 'application/json',
                				'Content-Type': 'application/json' 
            			},
                        data: JSON.stringify({
         				email: email,
                            nameUser: name,
         				phNo: mobile,
         				address: $("#InputPin").val().trim(),
         				pin: $("#InputAddress").val().trim(),
         				price: $("#totalamount").text().trim()
                        }),
                        type: "POST",
                        dataType : "json",
                     
                    }).done(function (data) {
         			window.location.href = "${pageContext.request.contextPath}/success";
         		}).fail(function (jqXHR, textStatus) {
         alert("Some thing went wrong. Place your order again");
         		});
         	
         	
         	
         	}
         	
         	function openModal(){
         		var amount = $('#productcost').text().trim();
         		if(amount==0){
         			alert("Please add products into your basket");
         		}else{
         			$('#Mymodel').modal('show');
         		}
         			
         	}
      </script>
   </head>
   <body>
      <!--header-->
      <jsp:include page="header.jsp"></jsp:include>
      <!-- grow -->
      <div class="grow">
         <div class="container">
            <h2>Checkout</h2>
         </div>
      </div>
      <!-- grow -->
      <div class="container">
         <div class="check">
            <c:if test="${not empty status}">
               <div class="alert alert-success">
                  <strong>Success!</strong> ${status }
               </div>
            </c:if>
            <div id="alert">
				
			</div>
            <h1>My Order History</h1>
            <div class="col-md-9 cart-items">
               <c:forEach var="order" items="${orderList}"
                  varStatus="loop">
                  <div class="cart-header">
                     <div class="close1" id="${loop.index}"></div>
                     <div class="cart-sec simpleCart_shelfItem">
                        <div class="cart-item-info">
                           <div class="x_content">
                              <br />
                                 <div class="col-md-6">
                                 <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Name : </label>
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">${order.nameUser}</label>
                                 </div>
                                </div>
                                <div class="col-md-6">
                                 <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Mobile : </label>
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">${order.phNo}</label>
                                 </div>
                                </div>
                                <div class="col-md-6">
                                 <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Email : </label>
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">${order.email}</label>
                                 </div>
                                </div>
                                <div class="col-md-6">
                                 <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Address : </label>
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">${order.address}</label>
                                 </div>
                                </div>
                                <div class="col-md-6">
                                 <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">PIN : </label>
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">${order.pin}</label>
                                 </div>
                                </div>
                                <div class="col-md-6">
                                 <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Price : </label>
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">${order.price}</label>
                                 </div>
                                </div>
                           </div>
                        </div>
                        <div class="clearfix"></div>
                     </div>
                  </div>
               </c:forEach>
            </div>
         </div>
      </div>
      <!--//content-->
      <jsp:include page="footer.jsp"></jsp:include>
      <jsp:include page="modal.jsp"></jsp:include>
      
   </body>
</html>