<div class="footer">
			<div class="container">
				<div class="footer-top-at">
					<div class="col-md-3 amet-sed">
						<h4>Contact Us</h4>
						<p>Honey Buzz</p>
						<p>194, Suyog Nagar, Near MSEB,</p>
						<p>Nagpur-440015, Maharashtra, India.</p>
						<p>+91 8007744442</p>
						<p>interact@honeybuzz.com</p>
					</div>
					<div class="col-md-3 amet-sed ">
						<div class="stay">
						<div class="stay-left">
							<form>
								<input type="text" placeholder="Name" required=""><br><br>
								<input type="text" placeholder="Email " required=""><br><br>
								<input type="text" placeholder="Subject " required=""><br><br>
								<input type="text" placeholder="Message " required="">
							</form>
						</div>
						<div class="btn-1">
							<form>
								<input type="submit" value="Send">
							</form>
						</div>
							<div class="clearfix"> </div>
						</div>
					</div>	
					<div class="col-md-3 amet-sed ">
						<h4>Products</h4>
						<p><a href="${pageContext.request.contextPath}/products/Cakes">Cakes</a></p>
						<p><a href="${pageContext.request.contextPath}/products/Chocolates">Chocolates</a></p>
						<p><a href="${pageContext.request.contextPath}/products/Sweets">Sweets</a></p>
						<p><a href="${pageContext.request.contextPath}/products/Flowers">Flowers</a></p>
					</div>
					
					<div class="col-md-3 amet-sed ">
						<h4>Honey Buzz</h4>
						<p><a href="${pageContext.request.contextPath}/privacyPolicy#about">About Us</a></p>
						<p><a href="${pageContext.request.contextPath}/privacyPolicy#testi">Testimonials</a></p>
						<p><a href="${pageContext.request.contextPath}/privacyPolicy#queen">Queen Bee</a></p>
						<p><a href="${pageContext.request.contextPath}/privacyPolicy#partner">Partner With Us</a></p>
						<p><a href="${pageContext.request.contextPath}/privacyPolicy#terms">Terms and Conditions</a></p>
						<p><a href="${pageContext.request.contextPath}/privacyPolicy#privacy">Privacy Policy</a></p>
					</div>
				</div>	
			</div>
		</div>
		<div class="footer-class">
			<p>© 2015 Mattress. All Rights Reserved</p>
		</div>