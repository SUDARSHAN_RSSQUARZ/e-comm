<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<!-- Meta, title, CSS, favicons, etc. -->
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Products</title>
<jsp:include page="adminCssInclude.jsp"></jsp:include>
</head>
<body class="nav-md">
	<div class="container body">
		<div class="main_container">
			<jsp:include page="navigationbar.jsp"></jsp:include>
			<!-- page content -->
			<div class="right_col" role="main">

				<div class="page-title">
					<div class="title_left" class="col-md-4 col-sm-12 col-xs-12">
						<h3>
							Products <small> </small>
						</h3>
					</div>
					<div class="row">
						<div class="col-md-12 col-sm-12 col-xs-12">
							<div class="x_panel">
								<div class="col-md-4">
									<form:form method="POST" enctype="multipart/form-data"
										action="${pageContext.request.contextPath}/outOfStockProducts"
										class="form-horizontal form-label-left">
										<div
											class="col-md-12 col-sm-12 col-xs-12 form-group pull-right top_search">
											<label>List out Of Stock Product</label>
											<div class="input-group">
												<!-- <input type="text" class="form-control"
											placeholder="Search for..."> -->
												<span class="input-group-btn">
													<button class="btn btn-default" type="submit">List out Of Stock Product</button>
												</span>
											</div>

										</div>

									</form:form>
								</div>


								<div class="col-md-4">
									<form:form method="POST" enctype="multipart/form-data"
										action="${pageContext.request.contextPath}/productByCategory"
										class="form-horizontal form-label-left">
										<div
											class="col-md-12 col-sm-12 col-xs-12 form-group pull-right top_search">
											<label>Search By Category</label>
											<div class="input-group">
												<!-- <input type="text" class="form-control"
											placeholder="Search for..."> -->
												<select id="categoryId" name="category" class="form-control">
													<option>Choose Category</option>
													<c:forEach var="entry" items="${topNav}">
														<option value="${entry.key}">${entry.key}</option>
													</c:forEach>

												</select> <span class="input-group-btn">
													<button class="btn btn-default" type="submit">Go!</button>
												</span>
											</div>

										</div>

									</form:form>
								</div>


								<div class="col-md-4">
									<form:form method="POST" enctype="multipart/form-data"
										action="${pageContext.request.contextPath}/productByName"
										class="form-horizontal form-label-left">
										<div
											class="col-md-12 col-sm-12 col-xs-12 form-group pull-right top_search">
											<label>Search By Product Name</label>
											<div class="input-group">
												<!-- <input type="text" class="form-control"
											placeholder="Search for..."> -->
												<select id="categoryId" name="name" class="form-control">
													<option>Choose Product</option>
													<c:forEach var="p" items="${productListForSearch}">
														<option value="${p.name}">${p.name}</option>
													</c:forEach>

												</select> <span class="input-group-btn">
													<button class="btn btn-default" type="submit">Go!</button>
												</span>
											</div>

										</div>

									</form:form>
								</div>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-12 col-sm-12 col-xs-12">
							<div class="x_panel">
								<div class="x_content">
									<table id="example"
										class="table table-striped responsive-utilities jambo_table">
										<thead>
											<tr class="headings">
												<th><input type="checkbox" class="tableflat"></th>
												<th>Id</th>
												<th>Name</th>
												<th>Category</th>
												<th>Sub Category</th>
												<th>Price</th>
												<th>Type</th>
												<th><span class="nobr">Action</span></th>
												<th>Edit</th>
												<th>Delete</th>
											</tr>
										</thead>
										<tbody>
											<c:forEach var="p" items="${productList}" varStatus="loop">
												<tr
													class="<c:choose><c:when test='${loop.index%2 == 0}'>	even</c:when><c:otherwise>odd</c:otherwise></c:choose> pointer">
													<td class="a-center "><input type="checkbox"
														class="tableflat"></td>
													<td class=" ">${p.id}</td>
													<td class=" ">${p.name}</td>
													<td class=" ">${p.category}<i
														class="success fa fa-long-arrow-up"></i>
													</td>
													<td class=" ">${p.subCategory}</td>
													<td class=" ">${p.defaultPrice}</td>
													<td class="a-right a-right ">${p.type}</td>
													<td class=" last"><a
														href="${pageContext.request.contextPath}/viewProductDetails/${p.id}">View
															Product Details</a>
													<td class=" last"><a
														href="${pageContext.request.contextPath}/prepareEditProduct/${p.id}">Edit</a>
													<td class=" last"><a
														href="${pageContext.request.contextPath}/deleteProduct?id=${p.id}">Delete</a>
													</td>
												</tr>
											</c:forEach>
										</tbody>

									</table>
								</div>
							</div>
						</div>
						<br /> <br /> <br />
					</div>
				</div>

			</div>
			<!-- /page content -->
		</div>

	</div>

	<div id="custom_notifications" class="custom-notifications dsp_none">
		<ul class="list-unstyled notifications clearfix"
			data-tabbed_notifications="notif-group">
		</ul>
		<div class="clearfix"></div>
		<div id="notif-group" class="tabbed_notifications"></div>
	</div>

	<jsp:include page="adminScriptInclude.jsp"></jsp:include>
	<script>
		$(document).ready(function() {
			$('input.tableflat').iCheck({
				checkboxClass : 'icheckbox_flat-green',
				radioClass : 'iradio_flat-green'
			});
		});

		var asInitVals = new Array();
		$(document)
				.ready(
						function() {
							var oTable = $('#example')
									.dataTable(
											{
												"oLanguage" : {
													"sSearch" : "Search all columns:"
												},
												"aoColumnDefs" : [ {
													'bSortable' : false,
													'aTargets' : [ 0 ]
												} //disables sorting for column one
												],
												'iDisplayLength' : 12,
												"sPaginationType" : "full_numbers",
												"dom" : 'T<"clear">lfrtip',
												"tableTools" : {
													"sSwfPath" : "<?php echo base_url('assets2/js/Datatables/tools/swf/copy_csv_xls_pdf.swf'); ?>"
												}
											});
							$("tfoot input").keyup(
									function() {
										/* Filter on the column based on the index of this element's parent <th> */
										oTable.fnFilter(this.value, $(
												"tfoot th").index(
												$(this).parent()));
									});
							$("tfoot input").each(function(i) {
								asInitVals[i] = this.value;
							});
							$("tfoot input").focus(function() {
								if (this.className == "search_init") {
									this.className = "";
									this.value = "";
								}
							});
							$("tfoot input")
									.blur(
											function(i) {
												if (this.value == "") {
													this.className = "search_init";
													this.value = asInitVals[$(
															"tfoot input")
															.index(this)];
												}
											});
						});
	</script>
</body>

</html>