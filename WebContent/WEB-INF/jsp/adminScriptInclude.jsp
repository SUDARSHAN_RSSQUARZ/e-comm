		
		<script src="${pageContext.request.contextPath}/resources/js/bootstrap.min.js"></script>

        <!-- chart js -->
        <script src="${pageContext.request.contextPath}/resources/js/chartjs/chart.min.js"></script>
        <!-- bootstrap progress js -->
        <script src="${pageContext.request.contextPath}/resources/js/progressbar/bootstrap-progressbar.min.js"></script>
        <script src="${pageContext.request.contextPath}/resources/js/nicescroll/jquery.nicescroll.min.js"></script>
        <!-- icheck -->
        <script src="${pageContext.request.contextPath}/resources/js/icheck/icheck.min.js"></script>
        <!-- tags -->
        <script src="${pageContext.request.contextPath}/resources/js/tags/jquery.tagsinput.min.js"></script>
        <!-- switchery -->
        <script src="${pageContext.request.contextPath}/resources/js/switchery/switchery.min.js"></script>
        <!-- daterangepicker -->
        <script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/moment.min2.js"></script>
        <script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/datepicker/daterangepicker.js"></script>
        <!-- richtext editor -->
        <script src="${pageContext.request.contextPath}/resources/js/editor/bootstrap-wysiwyg.js"></script>
        <script src="${pageContext.request.contextPath}/resources/js/editor/external/jquery.hotkeys.js"></script>
        <script src="${pageContext.request.contextPath}/resources/js/editor/external/google-code-prettify/prettify.js"></script>
        <!-- select2 -->
        <script src="${pageContext.request.contextPath}/resources/js/select/select2.full.js"></script>
        <!-- form validation -->
        <script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/parsley/parsley.min.js"></script>
        <!-- textarea resize -->
        <script src="${pageContext.request.contextPath}/resources/js/textarea/autosize.min.js"></script>
        <script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/autocomplete/countries.js"></script>
        <script src="${pageContext.request.contextPath}/resources/js/autocomplete/jquery.autocomplete.js"></script>
        <script src="${pageContext.request.contextPath}/resources/js/custom.js"></script>
        <script src="${pageContext.request.contextPath}/resources/js/datatables/js/jquery.dataTables.js"></script>
        <script src="${pageContext.request.contextPath}/resources/js/datatables/tools/js/dataTables.tableTools.js"></script>
        
        