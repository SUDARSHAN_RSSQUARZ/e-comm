<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Login</title>

    <!-- Bootstrap core CSS -->

    <link href="${pageContext.request.contextPath}/resources/css/bootstrap.min.css" rel="stylesheet">

    <link href="${pageContext.request.contextPath}/resources/fonts/css/font-awesome.min.css" rel="stylesheet">
    <link href="${pageContext.request.contextPath}/resources/css/animate.min.css" rel="stylesheet">

    <!-- Custom styling plus plugins -->
    <link href="${pageContext.request.contextPath}/resources/css/custom.css" rel="stylesheet">
    <link href="${pageContext.request.contextPath}/resources/css/icheck/flat/green.css" rel="stylesheet">


    <script src="${pageContext.request.contextPath}/resources/js/jquery.min.js"></script>

    <!--[if lt IE 9]>
        <script src="../assets/js/ie8-responsive-file-warning.js"></script>
        <![endif]-->

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
          <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->

</head>

<body style="background:#F7F7F7;">
    
    <div class="">
        <a class="hiddenanchor" id="toregister"></a>
        <a class="hiddenanchor" id="tologin"></a>

        <div id="wrapper">
        
        	<c:if test="${not empty errorstatus}">
				<div class="alert alert-danger">
					<strong></strong> ${errorstatus }
				</div>
			</c:if>
			<c:if test="${not empty status}">
				<div class="alert alert-success">
					<strong></strong> ${status }
				</div>
			</c:if>
        	<br>
        	
            <div id="login" class="animate form">
                <section class="login_content">
				<form:form method="POST" action="${pageContext.request.contextPath}/login" commandName="command">
                    
                        <h1>Login Form</h1>
                        <div>
							<form:input path="userName" type="text" class="form-control" placeholder="Username" required="true"/>
                            <!--<input type="text" class="form-control" placeholder="Username" required="" />-->
                        </div>
                        <div>
							<form:input path="pwd" type="password" class="form-control" placeholder="Password" required="true"/>
                           <!-- <input type="password" class="form-control" placeholder="Password" required="" />-->
                        </div>
                        <div>
                             <input type="submit" class="btn btn-default submit" value="Submit"/>
                            <a href="#toregister" class="to_register">Lost your password?</a>
                        </div>
                        <div class="clearfix"></div>
                    </form:form>
                    <!-- form -->
                </section>
                <!-- content -->
            </div>
            <div id="register" class="animate form">
                <section class="login_content">
                    <form:form method="POST" action="${pageContext.request.contextPath}/forgotPwd" commandName="command">
                        <h1>Enter Details</h1>
                        <div>
                            <form:input path="userName" type="text" class="form-control" placeholder="Username" required="true"/>
                        </div>
                        <div>
                            <form:input path="email" type="text" class="form-control" placeholder="Email" required="true"/>
                        </div>
                        <div>
                            <input type="submit" class="btn btn-default submit" value="Submit"/>
                        </div>
                        <div class="clearfix"></div>
                        <div class="separator">

                            <p class="change_link">Already a member ?
                                <a href="#tologin" class="to_register"> Log in </a>
                            </p>
                            <div class="clearfix"></div>
                            <br />
                            <!-- <div>
                                <h1><i class="fa fa-paw" style="font-size: 26px;"></i> Gentelella Alela!</h1>

                                <p>©2015 All Rights Reserved. Gentelella Alela! is a Bootstrap 3 template. Privacy and Terms</p>
                            </div> -->
                        </div>
                    </form:form>
                    <!-- form -->
                </section>
                <!-- content -->
            </div>
        </div>
    </div>

</body>

</html>