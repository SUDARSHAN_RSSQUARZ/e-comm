<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
	<head>
		<title>Privacy Policy</title>
		<jsp:include page="websiteScript.jsp"></jsp:include>
		<!-- Custom Theme files -->
		<!--theme-style-->
		
		<!--//theme-style-->
		<link rel="shortcut icon" type="image/png" href="${pageContext.request.contextPath}/resources/images/favicon.png"/>
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<meta name="keywords" content="Honey Buzz" />
		<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
		<!--fonts-->
		
		<!-- start menu -->
		
		<!--Start of Zopim Live Chat Script-->
		<script type="text/javascript">
		window.$zopim||(function(d,s){var z=$zopim=function(c){z._.push(c)},$=z.s=
			d.createElement(s),e=d.getElementsByTagName(s)[0];z.set=function(o){z.set.
			_.push(o)};z._=[];z.set._=[];$.async=!0;$.setAttribute("charset","utf-8");
			$.src="//v2.zopim.com/?3a0P6Plo0iOMK5TF0Eg8CqvSBOR4WnKY";z.t=+new Date;$.
			type="text/javascript";e.parentNode.insertBefore($,e)})(document,"script");
		</script>
		 <script src="${pageContext.request.contextPath}/resources/js/main.js"></script>
		<style>
		
		#requestcallback{
			position: absolute;

border: medium none;
outline: medium none;
background: #FFF none repeat scroll 0% 0%;
height: 36px;
z-index: 10;
font-size: 13px;
color: #2E0854;
		
		}
	
		
		
		
		
		
		
			h3,h2{
			color:#6F7887;
			}
		
			.slide .privacy p{
			text-align:justify;
			margin : 10px;
			font: 16px/30px Georgia, serif;
			color:#6F7887;
			}
			
			#terms p, #about p, #testi p, #partner p, #queen p{
			text-align:justify;
			margin : 10px;
			font: 16px/30px Georgia, serif;
			color:#6F7887;
			}
			
			#terms ul li{
			text-align:justify;
			margin : 10px;
			font: 16px/30px Georgia, serif;
			color:#6F7887;
			}
			
			
			body { 
			height: 100%;
			overflow-y: scroll;
			overflow-x: hidden;
			/* background-image: url("images/1.jpg");
			background-attachment: fixed; */

  			/* center it */
  			background-position: center center;

  			/* Scale it nicely to the element */
  			background-size: cover;
			}
			
			
			.slide {
			position: relative;
			padding: 8vh 10%;
			min-height: 60vh;
			width: 100vw;
			box-sizing: border-box;
			box-shadow: 0 -1px 10px rgba(0, 0, 0, .7);
			transform-style: inherit;
			background-color:#fff;
			opacity:1;
			}
			
			.parallax-window {
    		min-height: 400px;
    		background: transparent;
			}
			
			
		</style>
		
	</head>
	<body>
		<!--header-->
		<jsp:include page="header.jsp"></jsp:include>
		<div class="parallax-window" data-parallax="scroll" data-image-src="${pageContext.request.contextPath}/resources/images/1.jpg"></div>
		<!--content-->
		<div class="slide" id="about">
		<h3 style="padding-top:20px;" class="animate zoomIn">About Us</h3><br><br>
		<div class="row">
			
			<div class="col-md-12">
				<p>
				We at Honey Buzz are a premium bakery in Nagpur with a variety of offerings. 
				We offer cakes, chocolates, sweets and flowers. We aim at spreading joy through food. 
				We believe food is what brings people together. That is why we are committed to the cause 
				serving people with joy!
				</p>
			</div>
		</div>
		</div>
		
		<div class="slide" id="testi">
			<h3 style="padding-top:20px;">Testimonials</h3><br>
			<div class="row">
			<div class="col-md-6">
				<div class="col-md-4">
					<img src="${pageContext.request.contextPath}/resources/images/oldman.jpg" width="148" height="184"/><br><br>
					<h5 style="padding-left:30px;"><i>Rajesh</i></h5>
				</div>
				<div class="col-md-7">
					<p> Quality of the cake is just amazing. Fresh cakes whenever and wherever you need them.</p>
				</div>
			</div>	
			<div class="col-md-6">
				<div class="col-md-4">
					<img src="${pageContext.request.contextPath}/resources/images/oldman.jpg" width="148" height="184"/><br><br>
					<h5 style="padding-left:30px;"><i>Meghana</i></h5>
				</div>
				<div class="col-md-7">
					<p> One of the best bakeries in Nagpur. Gorgeous cakes at affordable prices. </p>
				</div>
			</div>
			</div>
			<br>
			<br>
			<div class="row">
				<div class="col-md-6">
				<div class="col-md-4">
					<img src="${pageContext.request.contextPath}/resources/images/oldman.jpg" width="148" height="184"/><br><br>
					<h5 style="padding-left:30px;"><i>Deepa</i></h5>
				</div>
				<div class="col-md-7">
					<p> I ordered laddus from Honey Buzz in Diwali 2015. The delivery was omn time as 
					promised. My entire family loved the laddus. Absolutely fresh with right consistency. </p>
				</div>
				</div>
			<div class="col-md-6">
				<div class="col-md-3">
					<img src="${pageContext.request.contextPath}/resources/images/oldman.jpg" width="148" height="184"/><br><br>
					<h5 style="padding-left:30px;"><i>Amit</i></h5>
				</div>
				<div class="col-md-9">
					<p> Honey Buzz is one of the most reliable bakeries in Nagpur. I have always been 
					satisfies with their products.</p>
				</div>
			</div>
			</div>
			
		</div>
		
		<div class="slide" id="partner">
			<h3 style="padding-top:20px;">Partner With Us</h3><br><br>
			<div class="row">
			<div class="col-md-12">
				<p>
				We are open to partnerships and alliances with cake retailers, corporates and brand
				management companies. 
				If you are looking for a strategic partner to assist you in your 
				bulk orders for your occasions please feel free to drop in a line at 
				interact@honeybuzz.com
			</p>
			</div>
			</div>
				
			
			
		</div>
		
		<div class="slide" id="queen">
			<h3 style="padding-top:20px;">Queen Bee</h3>	
			
			<div class="col-md-4">
				<p style="height:12em;  overflow: hidden;">
					We are open to partnerships and alliances with cake retailers, corporates and brand
					management companies. 
					If you are looking for a strategic partner to assist you in your 
					bulk orders for your occasions please feel free to drop in a line at 
					interact@honeybuzz.com
					We are open to partnerships and alliances with cake retailers, corporates and brand
					management companies. 
					If you are looking for a strategic partner to assist you in your 
					bulk orders for your occasions please feel free to drop in a line at 
					interact@honeybuzz.com
					We are open to partnerships and alliances with cake retailers, corporates and brand
					management companies. 
					If you are looking for a strategic partner to assist you in your 
					bulk orders for your occasions please feel free to drop in a line at 
					interact@honeybuzz.com
				</p>
				<a href="#" id="blogreadmore">Read More</a>
			</div>
			<div class="col-md-4">
				<p style="height:12em;  overflow: hidden;">
					We are open to partnerships and alliances with cake retailers, corporates and brand
					management companies. 
					If you are looking for a strategic partner to assist you in your 
					bulk orders for your occasions please feel free to drop in a line at 
					interact@honeybuzz.com
					We are open to partnerships and alliances with cake retailers, corporates and brand
					management companies. 
					If you are looking for a strategic partner to assist you in your 
					bulk orders for your occasions please feel free to drop in a line at 
					interact@honeybuzz.com
					We are open to partnerships and alliances with cake retailers, corporates and brand
					management companies. 
					If you are looking for a strategic partner to assist you in your 
					bulk orders for your occasions please feel free to drop in a line at 
					interact@honeybuzz.com
				</p>
				<a href="#" id="blogreadmore">Read More</a>
			</div>
			<div class="col-md-4">
				<p style="height:12em;  overflow: hidden;">
					We are open to partnerships and alliances with cake retailers, corporates and brand
					management companies. 
					If you are looking for a strategic partner to assist you in your 
					bulk orders for your occasions please feel free to drop in a line at 
					interact@honeybuzz.com
					We are open to partnerships and alliances with cake retailers, corporates and brand
					management companies. 
					If you are looking for a strategic partner to assist you in your 
					bulk orders for your occasions please feel free to drop in a line at 
					interact@honeybuzz.com
					We are open to partnerships and alliances with cake retailers, corporates and brand
					management companies. 
					If you are looking for a strategic partner to assist you in your 
					bulk orders for your occasions please feel free to drop in a line at 
					interact@honeybuzz.com
				</p>
				<a href="#" id="blogreadmore">Read More</a>
			</div>
			
		</div>
		<div class="slide" id="terms">
			<h3 style="padding-top:20px;">Terms and Conditions</h3>
			<div class="termand" style="height:20em;  overflow: hidden;">
			<ul style="text-align: justify;text-justify: inter-word;">
				<li>
					The user should carefully read the description of the product, pricing, delivery
					charges, shipping declaration and destination information before placing the order.
				</li>
				<li>
					The images displayed on the site are indicative in nature. Actual product may vary
					in appearance, shape or design as per the availability.
				</li>
				<li>
					The user agrees to give the correct information which is true and authentic. We
					reserve the right to confirm the information provided by the user.
				</li>
				<li>
					We will not be held liable for any credit card fraud that occurs on its website. Users
					have to use only their card when they make their payments. The liability of using a 
					fraudulent credit card lies on the user and the responsibility to establish otherwise 
					shall exclusively be the users’.
				</li>
				<li>
					In case of any complaint against the quality of product we shall redeliver the
					product subject to satisfaction of recipient. But all such claim should be raised 
					within 12 Hours of delivery. Refund can also be made depending on nature of 
					complaint.
				</li>
				<li>
					Prices of items which are subject to MRP restrictions are being sold on the website
					after adding up costs of our services, delivery and online transactions cost.
				</li>
				<li>
					Flower arrangements shown here are indicative. The actual arrangement may
					varies considering the availability of flowers and other reasons.
				</li>
				<li>
					If ordered flowers/ cake are not available, then we shall communicate with you
					regarding the available stock considering our policy.
				</li>
				<li>
					The owners of the site retain the right to refuse any order unconditionally.
				</li>
				<li>
					All claims are subject to the jurisdiction of local Nagpur Court only in India.
				</li>
				<li>
					Honey Buzz may at any time modify the terms and conditions contained on the
					Site without any prior notification.
				</li>
				<li>
					The User should be eligible to place the order online.
				</li>
			</ul>	
			</div>
			<a href="#" id="readmoreterms">Read More</a>
		</div>
		<div class="slide" id="privacy">
			<h2 style="padding-top:20px;">Privacy Policy</h2>
			<div class="privacy" style="text-align: justify;text-justify: inter-word; height:20em;  overflow: hidden;">
				<p>
					Celebrating Vacations recognizes the importance of maintaining your privacy. We value 
					your privacy and appreciate your trust in us. This Policy describes how we treat user 
					information we collect on <a href="http://www.honeybuzz.com">http://www.honeybuzz.com</a> and other offline sources. This 
					Privacy Policy applies to current and former visitors to our website and to our online 
					customers. By visiting and/or using our website, you agree to this Privacy Policy.
				</p>
				<br>
				<h3>Information we collect</h3>
				<br>
				<h4>Contact information: </h4><p>We might collect your name, email, mobile number and phone
				number.</p>
				<h4>Payment and billing information: </h4>
				<p>We might collect your billing name, billing address and 
					payment method when you buy a ticket. We NEVER collect your credit card number or 
					credit card expiry date or other details pertaining to your credit card on our website. 
					Credit card information will be obtained and processed by our online payment partner.
				</p>
				<h4>Information you post: </h4>
				<p>
					We collect information you post in a public space on our website
					or on a third-party social media site belonging to www.honeybuzz.com.	
				</p>
				
				<h4>Demographic information: </h4>
				<p>
					We may collect demographic information about you, events
					you like, events you intend to participate in, tickets you buy, or any other information 
					provided by your during the use of our website. We might collect this as a part of a 
					survey also.	
				</p>
				
				<h4>Other information: </h4>
				<p>
					If you use our website, we may collect information about your IP
					address and the browser you're using. We might look at what site you came from, 
					duration of time spent on our website, pages accessed or what site you visit when you 
					leave us. We might also collect the type of mobile device you are using, or the version 
					of the operating system your computer or device is running.	
				</p><br>
				
				<h3>We collect information in different ways.</h3><br>
				<h4>We collect information directly from you: </h4>
				<p>
					We collect information directly from you when
					you register for an event or buy tickets. We also collect information if you post a 
					comment on our websites or ask us a question through phone or email.
				</p>
				<h4>We collect information from you passively: </h4>
				<p>
					We use tracking tools like Google Analytics,
					Google Webmaster, browser cookies and web beacons for collecting information about 
					your usage of our website.
				</p>
				<h4>We get information about you from third parties: </h4>
				<p>
					For example, if you use an integrated
					social media feature on our websites. The third-party social media site will give us 
					certain information about you. This could include your name and email address.
				</p><br>
				
				<h3>Use of your personal information</h3><br>
				<h4>We use information to contact you: </h4>
				<p>
					We might use the information you provide to contact
					you for confirmation of a purchase on our website or for other promotional purposes.
				</p>
				<h4>We use information to respond to your requests or questions: </h4>
				<p>
					We might use your information to confirm your registration for an event or contest.
				</p>
				<h4>We use information to improve our products and services:</h4>
				<p>
					We might use your
					information to customize your experience with us. This could include displaying content 
					based upon your preferences.
				</p>
				<h4>We use information to look at site trends and customer interests:</h4>
				<p>
					We may use your
					information to make our website and products better. We may combine information we 
					get from you with information about you we get from third parties.
				</p>
				<h4>We use information for security purposes:</h4>
				<p>
					We may use information to protect our
					company, our customers, or our websites.
				</p>
				<h4>We use information for security purposes:</h4>
				<p>
					We may use information to protect our
					company, our customers, or our websites.
				</p>
				<h4>We use information for marketing purposes:</h4>
				<p>
					We might send you information about
					special promotions or offers. We might also tell you about new features or products. 
					These might be our own offers or products, or third-party offers or products we think you 
					might find interesting.
				</p>
				<p>
					We use information to send you transactional communications. We might send you 
					emails or SMS about your account or a purchase. We use information as otherwise permitted by law.
				</p><br>
				
				<h3>Sharing of information with third-parties:</h3><br>
				<p>
					We will share information with third parties who perform services on our behalf. We 
					share information with vendors who help us manage our online registration process or 
					payment processors or transactional message processors. Some vendors may be 
					located outside of India.
				</p><br>
				<p>
					We will share information with the event organizers. We share your information with 
					event organizers and other parties responsible for fulfilling the purchase obligation. The 
					event organizers and other parties may use the information we give them as described 
					in their privacy policies.
				</p><br>
				<p>
					We will share information with our business partners. This includes a third party who 
					provide or sponsor an event, or who operates a venue where we hold events. Our 
					partners use the information we give them as described in their privacy policies.
				</p><br>
				<p>
					We may share information if we think we have to in order to comply with the law or to
					protect ourselves. We will share information to respond to a court order or subpoena. 
					We may also share it if a government agency or investigatory body requests. Or, we 
					might also share information when we are investigating potential fraud.
				</p><br>
				<p>
					We may share information with any successor to all or part of our business. For 
					example, if part of our business is sold we may give our customer list as part of that 
					transaction.
				</p><br>
				
				<h3>We may share your information for reasons not described in this policy. We will tell you
				before we do this.</h3><br>
				<h3>Email Opt-Out:</h3><br>
				<p>
					You can opt out of receiving our marketing emails. To stop receiving our promotional 
					emails, please email interact@celvac.in. It may take about ten days to process your 
					request. Even if you opt out of getting marketing messages, we will still be sending you 
					transactional messages through email and SMS about your purchases.
				</p><br>
				<h3>Third party sites:</h3><br>
				<p>
					If you click on one of the links to third party websites, you may be taken to websites we 
					do not control. This policy does not apply to the privacy practices of those websites. 
					Read the privacy policy of other websites carefully. We are not responsible for these 
					third party sites.
				</p><br>
				<h3>Grievance Officer:</h3><br>
				<p>
					In accordance with Information Technology Act 2000 and rules made there under, the 
					name and contact details of the Grievance Officer are provided below:
				</p><br>
				
				<p>Mr. Vicky Deshmukh</p>
				<p>Honey Buzz</p>
				<p>194, Suyog Nagar, Near MSEB,</p>
				<p>Nagpur-440015, Maharashtra, India.</p>
				<p>+91 8007744442</p>
				<p>interact@honeybuzz.com</p><br>
				<p>
					If you have any questions about this Policy or other privacy concerns, you can also 
					email us at interact@honeybuzz.com
				</p><br>
				<h3>Updates to this policy</h3><br>
				<p>
					This Privacy Policy was last updated on 01.12.2015. From time to time we may change 
					our privacy practices. We will notify you of any material changes to this policy as 
					required by law. We will also post an updated copy on our website. Please check our 
					site periodically for updates.
				</p><br>
				<h3>Jurisdiction</h3><br>
				<p>
					If you choose to visit the website, your visit and any dispute over privacy is subject to 
					this Policy and the website's terms of use. In addition to the foregoing, any disputes 
					arising under this Policy shall be governed by the laws of India.
				</p><br>
			</div>
			<a href="#" id="readmore">Read More</a>
		</div>
		
		
		<jsp:include page="footer.jsp"></jsp:include>
		<jsp:include page="modal.jsp"></jsp:include>
	<script>
	
		document.querySelector('#readmore').addEventListener('click', function() {
			window.location.href='${pageContext.request.contextPath}/privacyPolicy#privacy';
			document.querySelector('.privacy').style.height= 'auto';
			this.style.display= 'none';
		});
		document.querySelector('#readmoreterms').addEventListener('click', function() {
			window.location.href='${pageContext.request.contextPath}/privacyPolicy#terms';
			document.querySelector('.termand').style.height= 'auto';
			this.style.display= 'none';
		});
		
	</script>
</body>
</html>
