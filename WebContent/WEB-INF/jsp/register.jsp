
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<!DOCTYPE html>
<html>
<head>
<title>Checkout</title>
<jsp:include page="websiteScript.jsp"></jsp:include>
<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
	
<!-- Custom Theme files -->
<!--theme-style-->

<!--//theme-style-->
<link rel="shortcut icon" type="image/png"
	href="${pageContext.request.contextPath}/resources/images/favicon.png" />
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="keywords" content="Honey Buzz" />
<script type="application/x-javascript">
	
        addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){
        window.scrollTo(0,1); }

</script>
<!--fonts-->
<link
	href='http://fonts.googleapis.com/css?family=Lato:100,300,400,700,900'
	rel='stylesheet' type='text/css'>
<link
	href='http://fonts.googleapis.com/css?family=Roboto:400,100,300,500,700,900'
	rel='stylesheet' type='text/css'>
<!--//fonts-->
<!-- start menu -->
<link href="${pageContext.request.contextPath}/resources/css/memenu.css"
	rel="stylesheet" type="text/css" media="all" />
<script type="text/javascript"
	src="${pageContext.request.contextPath}/resources/js/memenu.js"></script>
<script>
	$(document).ready(function() {
		$(".memenu").memenu();
	});
</script>
<script
	src="${pageContext.request.contextPath}/resources/js/simpleCart.min.js">
	
</script>
<script
	src="${pageContext.request.contextPath}/resources/js/imagezoom.js"></script>
<script src="${pageContext.request.contextPath}/resources/js/cookie.js"></script>
<!--Start of Zopim Live Chat Script-->
<script type="text/javascript">
window.$zopim||(function(d,s){var z=$zopim=function(c){z._.push(c)},$=z.s=
	d.createElement(s),e=d.getElementsByTagName(s)[0];z.set=function(o){z.set.
	_.push(o)};z._=[];z.set._=[];$.async=!0;$.setAttribute("charset","utf-8");
	$.src="//v2.zopim.com/?3a0P6Plo0iOMK5TF0Eg8CqvSBOR4WnKY";z.t=+new Date;$.
	type="text/javascript";e.parentNode.insertBefore($,e)})(document,"script");
</script>
</head>
<body>

	<!--header-->
	<jsp:include page="header.jsp"></jsp:include>

	<div class="grow">
		<div class="container">
			<h2>Register</h2>
		</div>
	</div>
	<!-- grow -->
	<!--content-->
	<div class=" container">
		<div class=" register">

			<form>
				<div class="col-md-6 register-top-grid">
					<h3>Register</h3>
					<div>
						<span>Full Name</span> <input type="text" placeholder="Full Name" required>
					</div>
					<div>
						<span>Mobile</span> <input type="tel" placeholder="10 Digit mobile number" pattern="[0-9]{10}" required>
					</div>
					<div>
						<span>Email Address</span> <input type="email" placeholder="Email Id" required>
					</div>
					<div>
						<span>Username</span> <input type="text" placeholder="Username" required>
					</div>
					<div>
						<span>Password</span> <input type="password" placeholder="Password" required>
					</div>
					<div>
						<span>Confirm Password</span> <input type="password" placeholder="Retype Password" required>
					</div>
					<div>
						<span>City</span> <input type="text" placeholder="City">
					</div>
					<div>
						<span>State</span> <input type="text" placeholder="State">
					</div>
					<div>
						<span>Pincode</span> <input type="text" placeholder="Pincode">
					</div>
					<div>
						<span>Country</span> <input type="text" placeholder="Country">
					</div>
					<input type="submit" value="Register">
					<!-- <a class="news-letter" href="#"> <label class="checkbox"><input
							type="checkbox" name="checkbox" checked=""><i> </i>Sign
							Up for Newsletter</label>
					</a> -->
				</div>
			</form>	
			<form>
				<div class="col-md-6 register-top-grid">
					<h3>Already have an account?</h3>
					<div>
						<span>Username</span> <input type="text" placeholder="Username" required>
					</div>
					<div>
						<span>Password</span> <input type="password" placeholder="Password" required>
					</div>
					<input type="submit" value="Login">

				</div>
				<div class="clearfix"></div>
			</form>
		</div>
	</div>

	<!--//content-->
	<jsp:include page="footer.jsp"></jsp:include>
</body>
</html>
